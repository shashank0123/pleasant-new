<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Products\Product;
use App\Shop\Wishlist\Wishlist;
use App\Shop\Contact\Contact;
use StdClass,DB;

class AjaxController extends Controller
{
	public function addToCart(Request $request){
        $cart = new StdClass;

        $cartItem = 0;

        $product_id = $request->id;
        $quantity = $request->quantity ?? '1';
        $price = $request->product_price ?? '';
        $size = $request->product_size ?? '';


        $product = Product::where('id',$product_id)->first();

        if(empty($price)){
            $price = $product->price;
        }

        // echo $price; die;
        if (session()->get('cart') != null){
            $cart = session()->get('cart');
        }
        else {
            $cart = array();
        }

        if (isset($cart[$product_id]->quantity)){
            $cart[$product_id]->quantity = $cart[$product_id]->quantity + $quantity;
            $cart[$product_id]->total_price = $cart[$product_id]->quantity*$cart[$product_id]->unit_price;
        }
        else{
            $item = new StdClass;
            $item->product_id = $product_id;
            $item->quantity = $quantity;
            $item->unit_price = $price;
            $item->size = $size ?? '';
            $item->total_price = $quantity*$price;
            $cart[$product_id] = $item;
        }

        $request->session()->put('cart',$cart);

        foreach(session()->get('cart') as $getCart){
            $cartItem++;
        }

        if(!$cart){
            echo "Something Went Worng";
        }

        return response()->json(['status' => 200, 'message' => 'data retrieved','cartItem' => $cartItem]);
    }




//To show session data on Mini Cart
    public function getMinicart(){
        $cartproducts = array();
        
        $bill = 0;

        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){

                $product = Product::where('id',$cart->product_id)->first();
                $product->total_quantity = $cart->quantity;
                $product->price = $cart->unit_price;
                $cartproducts[] = $product;

                $bill+= $cart->total_price;
                
            }
            
        }



        return view('front.minicart',compact('cartproducts','bill'))->render();
        
    }


    public function addToWishlist(Request $request){
        $product_id = $request->id;
        $user_id = $request->userId;
        $response = new StdCLass;
        $response->wishItem = 0;
        $check = Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
        if($check){
            $response->message= 'Product is already in your wishlist';
        }
        else{
            $new_user = new  Wishlist;
            $new_user->product_id = $product_id;
            $new_user->user_id = $user_id;

            $new_user->save();
            $response->message= 'Product added in your wishlist';
        }

        $response->wishItem = Wishlist::where('user_id',$user_id)->count();

        return response()->json($response);

    }

    public function deleteWishlistItem(Request $request){
        $flag = 0;
        $response = new StdClass;
        $response->flag = 0;
        $response->wishItem=0;
        $wishlist = Wishlist::where('product_id',$request->id)
        ->where('user_id',$request->user_id)
        ->first();
        $wishlist->delete();
        $cartItem = 0;

        $response->wishItem = Wishlist::where('user_id',$request->user_id)->count();

        return response()->json($response);
    }

    public function deleteSessionData(Request $request)
    {
        $flag = 1 ;
        $bill = 0;
        $cart = session()->get('cart');
        $cartItem = 0;
        $cart[$request->id] = null;
        $cart = array_filter($cart);
        $request->session()->put('cart',$cart);

        foreach(session()->get('cart') as $pro){
            $cartItem++;
            $product = Product::where('id',$pro->product_id)->first();
            $price = $pro->total_price;
            $bill = $bill + $price;
        }
        if($flag == 1)
            return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully','bill' => $bill, 'cartItem' => $cartItem]);
        else
            return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
    }



    public function updateCart(Request $request)
    {
        $flag = 1 ;
        $bill = 0;
        $total = 0;
        $action = $request->action;
        $item = Product::where('id',$request->id)->first();
        $cart = session()->get('cart');

        if($action == 'plus'){
         $cart[$request->id]->quantity = $cart[$request->id]->quantity+1;
     }
     else{
         $cart[$request->id]->quantity = $cart[$request->id]->quantity-1;
     }

     $cart[$request->id]->total_price = $cart[$request->id]->quantity*$cart[$request->id]->unit_price;

     $total = $cart[$request->id]->total_price;
     $cart = array_filter($cart);
     $request->session()->put('cart',$cart);
     $cartItem = 0;
     foreach(session()->get('cart') as $pro){



      $bill = $bill + $pro->total_price;
      $cartItem++;
  }
  $bill;

  if($flag == 1)
    return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully', 'bill' => $bill, 'total' => $total, 'cartItem' => $cartItem]);
else
    return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);
}




public function submitNewsletter(Request $request){
    $check = DB::table('newsletters')->where('email',$request->email)->first();
    $message = 'Something went wrong';

    if(!empty($check)){
        $message = 'This email is already registered for newsletter';
    }
    else{


        $newData = DB::table('newsletters')->insert([
            'email' => $request->email ?? '',
            'status' => 1
        ]);

        $message = 'Email registered successfully for latest updates. Thank you.';
    }
    return redirect()->back()->with('successReview',$message);
}


public function submitContactForm(Request $request){

    $message = 'Something went wrong';

    $newData = Contact::insert([
        'name' => $request->name ?? '',
        'email' => $request->email ?? '',
        'subject' => $request->subject ?? '',
        'message' => $request->message ?? '',
    ]);

    $message = 'Message sent successfully. Thank You.';

    return redirect()->back()->with('successReview',$message);
}


}