<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Shop\Products\Product;
use App\Shop\Categories\Category;
use App\Shop\Brands\Brand;
use App\Shop\ProductSize;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Transformations\ProductTransformable;
use StdClass,DB;

class ProductController extends Controller
{
    use ProductTransformable;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepo;

    /**
     * ProductController constructor.
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepo = $productRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $categories = [];
        $allCategories = Category::where('parent_id',NULL)
        ->where('status','1')
        ->orderBy('id','DESC')
        ->get();

        $brands = Brand::all();

         $best_sellers = Product::where('status','1')->limit(5)->get();


        if(!empty($allCategories)){
            $item = array();
                $i=0;
            foreach($allCategories as $category){
             
                $item[$i][0] = $category;

                $cat2 = Category::where('parent_id',$category->id)->where('status',1)->get();
                if(!empty($cat2)){
                    $j=1;
                    foreach($cat2 as $cate2){
                        $item[$i][$j] = $cate2;
                        $j++;
                    }
                }
                $i++;
            }
            $categories = $item;
        }
        $keyword = $request->search;
        $products = Product::where('status','1')->where('name','LIKE','%'.$request->search.'%')->get();

        return view('front.productsCategory',compact('products','keyword','categories','brands','best_sellers'));
    }


    public function search1()
    {
        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->productRepo->searchProduct(request()->input('q'));
        } else {
            $list = $this->productRepo->listProducts();
        }

        $products = $list->where('status', 1)->map(function (Product $item) {
            return $this->transformProduct($item);
        });

        return view('front.products.product-search', [
            'products' => $this->productRepo->paginateArrayResults($products->all(), 10)
        ]);
    }

    /**
     * Get the product
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $slug)
    {
        $categories = array();
        $product = $this->productRepo->findProductBySlug(['slug' => $slug]);
    // var_dump($product);die;
        $images = $product->images()->orderBy('priority','ASC')->get();
        // var_dump($images); die;
        $category = Category::leftjoin('category_product','category_product.category_id','categories.id')->where('category_product.product_id',$product['id'])->select('categories.*')->get()->last();
        $id = $category->id ?? '0';
        // $productAttributes = $product->attributes ?? '';

        $allCategories = Category::where('parent_id',NULL)
        ->where('status','1')
        ->orderBy('id','DESC')
        ->get();

        if(!empty($allCategories)){
            $item = array();
                $i=0;
            foreach($allCategories as $category){
             
                $item[$i][0] = $category;

                $cat2 = Category::where('parent_id',$category->id)->where('status',1)->get();
                if(!empty($cat2)){
                    $j=1;
                    foreach($cat2 as $cate2){
                        $item[$i][$j] = $cate2;
                        $j++;
                    }
                }
                $i++;
            }
            $categories = $item;
        }
        $brand = "";
        if(!empty($product['brand_id']))
        $brand = Brand::where('id',$product['brand_id'])->first();

    $sizes = array();
    if(!empty($product['size']))
        $sizes = explode(',',$product['size']);

    $hot_deals = Product::where('status','1')->limit(5)->get();
    $best_sellers = Product::where('status','1')->limit(5)->get();

    $reviews = DB::table('product_reviews')->where('product_id',$product['id'])->orderBy('created_at','DESC')->get();
        
        $related_products = Product::join('category_product','category_product.product_id','products.id')
                ->where('products.status','1')
                ->where('category_product.category_id',$id)
                ->orderBy('created_at','DESC')
                ->select('products.*')
                ->limit(8)
                ->get();
                // var_dump(count($related_products)); die;

        // var_dump($images); die;


                $product_sizes = ProductSize::where('product_id',$product->id)->get();
                    // var_dump($product_sizes); die;

        return view('front.productDetail',compact('product','images','category','categories','related_products','product_sizes','brand','sizes','hot_deals','best_sellers','reviews'));

        // return view('front.products.product', compact(
        //     'product',
        //     'images',
        //     'productAttributes',
        //     'category',
        //     'combos'
        // ));
    }

    public function showone(string $slug)
    {
        $products = "";
        $categories = array();
        if($slug=='all'){
            $allCategories = Category::where('parent_id',NULL)
        ->where('status','1')
        ->orderBy('id','DESC')
        ->get();


        if(!empty($allCategories)){
            $item = array();
                $i=0;
            foreach($allCategories as $category){
             
                $item[$i][0] = $category;

                $cat2 = Category::where('parent_id',$category->id)->where('status',1)->get();
                if(!empty($cat2)){
                    $j=1;
                    foreach($cat2 as $cate2){
                        $item[$i][$j] = $cate2;
                        $j++;
                    }
                }
                $i++;
            }
            $categories = $item;
        }
            $products = Product::where('status','1')->orderBy('created_at','DESC')->get();
        }
        else{
            $allCategories = Category::where('slug',$slug)
        ->where('status','1')
        ->orderBy('id','DESC')
        ->get();
        $cats = array();

        if(!empty($allCategories)){
            $item = array();
                $i=0;
            foreach($allCategories as $category){
             $cats[]=$category->id;
                $item[$i][0] = $category;

                $cat2 = Category::where('parent_id',$category->id)->where('status',1)->get();
                if(!empty($cat2)){
                    $j=1;
                    foreach($cat2 as $cate2){
             $cats[]=$cate2->id;
                        $item[$i][$j] = $cate2;
                        $j++;
                    }
                }
                $i++;
            }
            $categories = $item;
        }
       $products = Product::join('category_product','category_product.product_id','products.id')
                ->where('products.status','1')->whereIn('category_product.category_id',$cats)->orderBy('created_at','DESC')->limit(8)->get();
        }

        
        $brands = Brand::all();
      
        $best_sellers = Product::where('status','1')->limit(5)->get();


        
        // $product = $this->productRepo->findProductBySlug(['slug' => $slug]);
        // $images = $product->images()->get();
        // $category = $product->categories()->first();
        // $productAttributes = $product->attributes;

        return view('front.productsCategory',compact('products','categories','brands','best_sellers'));
    }



     public function submitReview(Request $request){
        $check = DB::table('product_reviews')->where('email',$request->email)->first();
            $message = 'Something went wrong';

        if(!empty($check)){
            $message = 'A review is already posted from this email.';
        }
        else{


        $newData = DB::table('product_reviews')->insert([
            'name' => $request->name ?? '',
            'email' => $request->email ?? '',
            'product_review' => $request->product_review ?? '',
            'product_rating' => $request->product_rating ?? '',
            'product_id' => $request->product_id ?? '',
        ]);

        $message = 'Your review submitted successfully. Thank you.';
        }
        return redirect()->back()->with('successReview',$message);
            }


}
