<?php

namespace App\Http\Controllers\Admin\Blog;

use App\Shop\Blog\Exceptions\BlogInvalidArgumentException;
use App\Shop\Blog\Exceptions\BlogNotFoundException;
use App\Shop\Blog\Blog;
use App\Shop\Blog\Repositories\Interfaces\BlogRepositoryInterface;
use App\Shop\Blog\Repositories\BlogRepository;
use App\Shop\Blog\Requests\CreateBlogRequest;
use App\Shop\Blog\Requests\UpdateBlogRequest;
use App\Http\Controllers\Controller;
use App\Shop\Blog\Transformations\BlogTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    use BlogTransformable, UploadableTrait;

    /**
     * @var BlogRepositoryInterface
     */
    private $blogRepo;

    
   
    public function __construct(
        BlogRepositoryInterface $blogRepository
    ) {
        
        $this->blogRepo = $blogRepository;

        // $this->middleware(['permission:create-blog, guard:employee'], ['only' => ['create', 'store']]);
        // $this->middleware(['permission:update-blog, guard:employee'], ['only' => ['edit', 'update']]);
        // $this->middleware(['permission:delete-blog, guard:employee'], ['only' => ['destroy']]);
        // $this->middleware(['permission:view-blog, guard:employee'], ['only' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->blogRepo->listBlogs('id');

        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->blogRepo->searchBlog(request()->input('q'));
        }

        $blogs = $list->map(function (Blog $item) {
            return $this->transformBlog($item);
        })->all();

        return view('admin.blogs.list', [
            'blogs' => $this->blogRepo->paginateArrayResults($blogs, 25)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $categories = $this->categoryRepo->listCategories('name', 'asc');
        
        return view('admin.blogs.create', [
            
            'blog' => new Blog
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateBlogRequest $request)
    {
        $data = $request->except('_token', '_method');
        $data['slug'] = str_slug($request->input('title'));

        if ($request->hasFile('cover') && $request->file('cover') instanceof UploadedFile) {
            $data['cover'] = $this->blogRepo->saveCoverImage($request->file('cover'));
        }

        $blog = $this->blogRepo->createBlog($data);

        $blogRepo = new BlogRepository($blog);

        
        return redirect()->route('admin.blogs.edit', $blog->id)->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $blog = $this->blogRepo->findBlogById($id);
        return view('admin.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $blog = $this->blogRepo->findBlogById($id);

        
        // if (request()->has('delete') && request()->has('pa')) {
        //     $pa = $blogAttributes->where('id', request()->input('pa'))->first();
        //     $pa->attributesValues()->detach();
        //     $pa->delete();

        //     request()->session()->flash('message', 'Delete successful');
        //     return redirect()->route('admin.blogs.edit', [$blog->id, 'combination' => 1]);
        // }

        
        return view('admin.blogs.edit', [
            'blog' => $blog,
            
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Shop\blogs\Exceptions\ProductUpdateErrorException
     */
    public function update(UpdateBlogRequest $request, int $id)
    {
        $blog = $this->blogRepo->findBlogById($id);

        $blogRepo = new BlogRepository($blog);

        $data = $request->all();
        
        $data['slug'] = str_slug($request->input('title'));

        if ($request->hasFile('cover')) {
            $data['cover'] = $blogRepo->saveCoverImage($request->file('cover'));
        }

        $blogRepo->updateBlog($data);

        return redirect()->route('admin.blogs.edit', $id)
            ->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $blog = $this->blogRepo->findBlogById($id);
        
        $blogRepo = new BlogRepository($blog);
        $blogRepo->removeblog();

        return redirect()->route('admin.blogs.index')->with('message', 'Delete successful');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage(Request $request)
    {
        $this->blogRepo->deleteFile($request->only('blog', 'image'), 'uploads');
        return redirect()->back()->with('message', 'Image delete successful');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeThumbnail(Request $request)
    {
        $this->blogRepo->deleteThumb($request->input('src'));
        return redirect()->back()->with('message', 'Image delete successful');
    }

    /**
     * @param Request $request
     * @param Product $blog
     * @return boolean
     */
    
}
