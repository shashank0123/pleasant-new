<?php

namespace App\Http\Controllers\Admin\CompanyDetail;

use App\Http\Controllers\Controller;
use App\Shop\CompanyDetail\Repositories\CompanyDetailRepository;
use App\Shop\CompanyDetail\Repositories\CompanyDetailRepositoryInterface;
use App\Shop\CompanyDetail\Requests\CreateCompanyDetailRequest;
use App\Shop\CompanyDetail\Requests\UpdateCompanyDetailRequest;

class CompanyDetailController extends Controller
{
    /**
     * @var CmsRepositoryInterface
     */
    private $companyDetailRepo;

    /**
     * CmsController constructor.
     *
     * @param CmsRepositoryInterface $companyDetailRepository
     */
    public function __construct(CompanyDetailRepositoryInterface $companyDetailRepository)
    {
        $this->companyDetailRepo = $companyDetailRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = $this->companyDetailRepo->paginateArrayResults($this->companyDetailRepo->listCms(['*'], 'id', 'asc')->all());

        return view('admin.company.list', ['company' => $data]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * @param CreateCmsRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateCompanyDetailRequest $request)
    {
        $this->companyDetailRepo->createCompanyDetail($request->all());

        return redirect()->route('admin.company.index')->with('message', 'Create company successful!');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        return view('admin.company.edit', ['company_detail' => $this->companyDetailRepo->findCompanyDetailById($id)]);
    }

    /**
     * @param UpdateCmsRequest $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Shop\company\Exceptions\UpdateCmsErrorException
     */
    public function update(UpdateCompanyDetailRequest $request, $id)
    {
        $company = $this->companyDetailRepo->findCmsById($id);

        $companyDetailRepo = new CompanyDetailRepository($company);
        $companyDetailRepo->updateCompanyDetail($request->all());

        return redirect()->route('admin.company.edit', $id)->with('message', 'Update successful!');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $CompanyDetail = $this->companyDetailRepo->findCompanyDetailById($id);

        $companyDetailRepo = new CompanyDetailRepository($CompanyDetail);
        $companyDetailRepo->deleteCompanyDetail();

        return redirect()->route('admin.company.index')->with('message', 'Delete successful!');
    }
}
