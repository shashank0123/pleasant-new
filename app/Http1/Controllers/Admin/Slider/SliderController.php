<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Shop\Slider\Exceptions\SliderInvalidArgumentException;
use App\Shop\Slider\Exceptions\SliderNotFoundException;
use App\Shop\Slider\Slider;
use App\Shop\Slider\Repositories\Interfaces\SliderRepositoryInterface;
use App\Shop\Slider\Repositories\SliderRepository;
use App\Shop\Slider\Requests\CreateSliderRequest;
use App\Shop\Slider\Requests\UpdateSliderRequest;
use App\Http\Controllers\Controller;
use App\Shop\Slider\Transformations\SliderTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    use SliderTransformable, UploadableTrait;

    /**
     * @var sliderRepositoryInterface
     */
    private $sliderRepo;

    
   
    public function __construct(
        SliderRepositoryInterface $sliderRepository
    ) {
        
        $this->sliderRepo = $sliderRepository;

        // $this->middleware(['permission:create-slider, guard:employee'], ['only' => ['create', 'store']]);
        // $this->middleware(['permission:update-slider, guard:employee'], ['only' => ['edit', 'update']]);
        // $this->middleware(['permission:delete-slider, guard:employee'], ['only' => ['destroy']]);
        // $this->middleware(['permission:view-slider, guard:employee'], ['only' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->sliderRepo->listSliders('id');

        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->sliderRepo->searchSlider(request()->input('q'));
        }

        $sliders = $list->map(function (Slider $item) {
            return $this->transformSlider($item);
        })->all();

        return view('admin.sliders.list', [
            'sliders' => $this->sliderRepo->paginateArrayResults($sliders, 25)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $categories = $this->categoryRepo->listCategories('name', 'asc');
        
        return view('admin.sliders.create', [
            
            'slider' => new Slider
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSliderRequest $request)
    {
        $data = $request->except('_token', '_method');
        $data['slug'] = str_slug($request->input('name'));

        if ($request->hasFile('cover') && $request->file('cover') instanceof UploadedFile) {
            $data['cover'] = $this->sliderRepo->saveCoverImage($request->file('cover'));
        }

        $slider = $this->sliderRepo->createSlider($data);

        $sliderRepo = new SliderRepository($slider);

        
        return redirect()->route('admin.sliders.edit', $slider->id)->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $slider = $this->sliderRepo->findSliderById($id);
        return view('admin.sliders.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $slider = $this->sliderRepo->findSliderById($id);

        
        // if (request()->has('delete') && request()->has('pa')) {
        //     $pa = $sliderAttributes->where('id', request()->input('pa'))->first();
        //     $pa->attributesValues()->detach();
        //     $pa->delete();

        //     request()->session()->flash('message', 'Delete successful');
        //     return redirect()->route('admin.sliders.edit', [$slider->id, 'combination' => 1]);
        // }

        
        return view('admin.sliders.edit', [
            'slider' => $slider,
            
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Shop\sliders\Exceptions\ProductUpdateErrorException
     */
    public function update(UpdateSliderRequest $request, int $id)
    {
        $slider = $this->sliderRepo->findSliderById($id);

        $sliderRepo = new SliderRepository($slider);

        $data = $request->all();
        
        $data['slug'] = str_slug($request->input('title'));

        if ($request->hasFile('cover')) {
            $data['cover'] = $sliderRepo->saveCoverImage($request->file('cover'));
        }

        $sliderRepo->updateSlider($data);

        return redirect()->route('admin.sliders.edit', $id)
            ->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $slider = $this->sliderRepo->findSliderById($id);
        
        $sliderRepo = new SliderRepository($slider);
        $sliderRepo->removeSlider();

        return redirect()->route('admin.sliders.index')->with('message', 'Delete successful');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage(Request $request)
    {
        $this->sliderRepo->deleteFile($request->only('slider', 'image'), 'uploads');
        return redirect()->back()->with('message', 'Image delete successful');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeThumbnail(Request $request)
    {
        $this->sliderRepo->deleteThumb($request->input('src'));
        return redirect()->back()->with('message', 'Image delete successful');
    }

    /**
     * @param Request $request
     * @param Product $slider
     * @return boolean
     */
    
}
