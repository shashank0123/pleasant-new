<?php

namespace App\Http\Controllers\Admin\Testimonial;

use App\Shop\Testimonial\Exceptions\TestimonialInvalidArgumentException;
use App\Shop\Testimonial\Exceptions\TestimonialNotFoundException;
use App\Shop\Testimonial\Testimonial;
use App\Shop\Testimonial\Repositories\Interfaces\TestimonialRepositoryInterface;
use App\Shop\Testimonial\Repositories\TestimonialRepository;
use App\Shop\Testimonial\Requests\CreateTestimonialRequest;
use App\Shop\Testimonial\Requests\UpdateTestimonialRequest;
use App\Http\Controllers\Controller;
use App\Shop\Testimonial\Transformations\TestimonialTransformable;
use App\Shop\Tools\UploadableTrait;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TestimonialController extends Controller
{
    use TestimonialTransformable, UploadableTrait;

    /**
     * @var BlogRepositoryInterface
     */
    private $testimonialRepo;

    
   
    public function __construct(
        TestimonialRepositoryInterface $testimonialRepository
    ) {
        
        $this->testimonialRepo = $testimonialRepository;

        $this->middleware(['permission:create-testimonial, guard:employee'], ['only' => ['create', 'store']]);
        $this->middleware(['permission:update-testimonial, guard:employee'], ['only' => ['edit', 'update']]);
        $this->middleware(['permission:delete-testimonial, guard:employee'], ['only' => ['destroy']]);
        $this->middleware(['permission:view-testimonial, guard:employee'], ['only' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->testimonialRepo->listTestimonials('id');

        if (request()->has('q') && request()->input('q') != '') {
            $list = $this->testimonialRepo->searchTestimonial(request()->input('q'));
        }

        $testimonials = $list->map(function (Blog $item) {
            return $this->transformTestimonial($item);
        })->all();

        return view('admin.testimonials.list', [
            'testimonials' => $this->testimonialRepo->paginateArrayResults($testimonials, 25)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $categories = $this->categoryRepo->listCategories('name', 'asc');
        
        return view('admin.testimonials.create', [
            
            'testimonial' => new Testimonial
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTestimonialRequest $request)
    {
        $data = $request->except('_token', '_method');
        $data['slug'] = str_slug($request->input('name'));

        if ($request->hasFile('cover') && $request->file('cover') instanceof UploadedFile) {
            $data['cover'] = $this->testimonialRepo->saveCoverImage($request->file('cover'));
        }

        $testimonial = $this->testimonialRepo->createTestimonial($data);

        $testimonialRepo = new TestimonialRepository($testimonial);

        
        return redirect()->route('admin.testimonials.edit', $testimonial->id)->with('message', 'Create successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $testimonial = $this->testimonialRepo->findTestimonialById($id);
        return view('admin.testimonials.show', compact('testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $testimonial = $this->testimonialRepo->findTestimonialById($id);

        
        // if (request()->has('delete') && request()->has('pa')) {
        //     $pa = $blogAttributes->where('id', request()->input('pa'))->first();
        //     $pa->attributesValues()->detach();
        //     $pa->delete();

        //     request()->session()->flash('message', 'Delete successful');
        //     return redirect()->route('admin.blogs.edit', [$blog->id, 'combination' => 1]);
        // }

        
        return view('admin.testimonials.edit', [
            'testimonial' => $testimonial,
            
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProductRequest $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \App\Shop\blogs\Exceptions\ProductUpdateErrorException
     */
    public function update(UpdateTestimonialRequest $request, int $id)
    {
        $testimonial = $this->testimonialRepo->findTestimonialById($id);

        $testimonialRepo = new TestimonialRepository($testimonial);

        $data = $request->all();
        

        if ($request->hasFile('cover')) {
            $data['cover'] = $testimonialRepo->saveCoverImage($request->file('cover'));
        }

        $testimonialRepo->updateTestimonial($data);

        return redirect()->route('admin.testimonials.edit', $id)
            ->with('message', 'Update successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $testimonial = $this->testimonialRepo->findTestimonialById($id);
        
        $testimonialRepo = new TestimonialRepository($testimonial);
        $testimonialRepo->removeTestimonial();

        return redirect()->route('admin.testimonials.index')->with('message', 'Delete successful');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImage(Request $request)
    {
        $this->testimonialRepo->deleteFile($request->only('testimonial', 'image'), 'uploads');
        return redirect()->back()->with('message', 'Image delete successful');
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeThumbnail(Request $request)
    {
        $this->testimonialRepo->deleteThumb($request->input('src'));
        return redirect()->back()->with('message', 'Image delete successful');
    }

    /**
     * @param Request $request
     * @param Product $blog
     * @return boolean
     */
    
}
