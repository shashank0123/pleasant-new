<?php

namespace App\Shop\CompanyDetail\Requests;

use App\Shop\Base\BaseFormRequest;

class UpdateCompanyDetailRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
