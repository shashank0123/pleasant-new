<?php

namespace App\Shop\CompanyDetail\Repositories;

use Jsdecena\Baserepo\BaseRepository;
use App\Shop\CompanyDetail\CompanyDetail;
use App\Shop\CompanyDetail\Exceptions\CompanyDetailNotFoundErrorException;
use App\Shop\CompanyDetail\Exceptions\CreateCompanyDetailErrorException;
use App\Shop\CompanyDetail\Exceptions\UpdateCompanyDetailErrorException;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

class CompanyDetailRepository extends BaseRepository implements CompanyDetailRepositoryInterface
{
    /**
     * CompanyDetailRepository constructor.
     *
     * @param CompanyDetail $company_detail
     */
    public function __construct(CompanyDetail $company_detail)
    {
        parent::__construct($company_detail);
        $this->model = $company_detail;
    }

    /**
     * @param array $data
     *
     * @return CompanyDetail
     * @throws CreateCompanyDetailErrorException
     */
    public function createCompanyDetail(array $data) : CompanyDetail
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateCompanyDetailErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return CompanyDetail
     * @throws CompanyDetailNotFoundErrorException
     */
    public function findCompanyDetailById(int $id) : CompanyDetail
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new CompanyDetailNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateCompanyDetailErrorException
     */
    public function updateCompanyDetail(array $data) : bool
    {
        try {
            return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateCompanyDetailErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteCompanyDetail() : bool
    {
        return $this->delete();
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listCompanyDetail($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    
}
