<?php

namespace App\Shop\CompanyDetail\Repositories;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\CompanyDetail\CompanyDetail;
use App\Shop\Products\Product;
use Illuminate\Support\Collection;

interface CompanyDetailRepositoryInterface extends BaseRepositoryInterface
{
    public function createCompanyDetail(array $data): CompanyDetail;

    public function findCompanyDetailById(int $id) : CompanyDetail;

    public function updateCompanyDetail(array $data) : bool;

    public function deleteCompanyDetail() : bool;

    public function listCompanyDetail($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;
}
