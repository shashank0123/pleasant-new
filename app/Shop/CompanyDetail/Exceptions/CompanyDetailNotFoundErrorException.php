<?php

namespace App\Shop\CompanyDetail\Exceptions;

class CompanyDetailNotFoundErrorException extends \Exception
{
}
