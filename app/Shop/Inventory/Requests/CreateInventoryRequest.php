<?php

namespace App\Shop\Inventory\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateInventoryRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => ['required'],
            'billing_date' => ['required'],
            'amount' => ['required'],
            'stock' => ['required'],
        ];
    }
}
