<?php

namespace App\Shop\Inventory\Repositories;

use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Inventory\Inventory;
use App\Shop\Inventory\Exceptions\InventoryNotFoundErrorException;
use App\Shop\Inventory\Exceptions\CreateInventoryErrorException;
use App\Shop\Inventory\Exceptions\UpdateInventoryErrorException;
use App\Shop\Products\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

class InventoryRepository extends BaseRepository implements InventoryRepositoryInterface
{
    /**
     * InventoryRepository constructor.
     *
     * @param Inventory $inventory
     */
    public function __construct(Inventory $inventory)
    {
        parent::__construct($inventory);
        $this->model = $inventory;
    }

    /**
     * @param array $data
     *
     * @return Inventory
     * @throws CreateInventoryErrorException
     */
    public function createInventory(array $data) : Inventory
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new CreateInventoryErrorException($e);
        }
    }

    /**
     * @param int $id
     *
     * @return Inventory
     * @throws InventoryNotFoundErrorException
     */
    public function findInventoryById(int $id) : Inventory
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new InventoryNotFoundErrorException($e);
        }
    }

    /**
     * @param array $data
     * @param int $id
     *
     * @return bool
     * @throws UpdateInventoryErrorException
     */
    public function updateInventory(array $data) : bool
    {
        try {
            return $this->update($data);
        } catch (QueryException $e) {
            throw new UpdateInventoryErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deleteInventory() : bool
    {
        return $this->delete();
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @param string $sortBy
     *
     * @return Collection
     */
    public function listInventorys($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        return $this->all($columns, $orderBy, $sortBy);
    }

    
}
