<?php

namespace App\Shop\Inventory\Repositories;

use Jsdecena\Baserepo\BaseRepositoryInterface;
use App\Shop\Inventorys\Inventory;
use App\Shop\Products\Product;
use Illuminate\Support\Collection;

interface InventoryRepositoryInterface extends BaseRepositoryInterface
{
    public function createInventory(array $data): Inventory;

    public function findInventoryById(int $id) : Inventory;

    public function updateInventory(array $data) : bool;

    public function deleteInventory() : bool;

    public function listInventorys($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection;

}
