<?php

namespace App\Shop\Cms;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $fillable = [
    	'page',
    	'title',
    	'description'
    ];

    
}
