<?php

namespace App\Shop\Cms\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateCmsRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
        ];
    }
}
