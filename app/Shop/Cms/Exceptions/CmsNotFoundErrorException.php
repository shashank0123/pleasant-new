<?php

namespace App\Shop\Cms\Exceptions;

class CmsNotFoundErrorException extends \Exception
{
}
