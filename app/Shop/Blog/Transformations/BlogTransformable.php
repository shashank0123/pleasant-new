<?php

namespace App\Shop\Blog\Transformations;

use App\Shop\Blog\Blog;
use Illuminate\Support\Facades\Storage;

trait BlogTransformable
{
    /**
     * Transform the Blog
     *
     * @param Blog $Blog
     * @return Blog
     */
    protected function transformBlog(Blog $blog)
    {
        $prod = new Blog;
        $prod->id = (int) $blog->id;
        $prod->title = $blog->title;
        $prod->author = $blog->author;
        $prod->slug = $blog->slug;
        $prod->description = $blog->description;
        $prod->cover = asset("storage/$blog->cover");
        $prod->status = $blog->status;
        
        return $prod;
    }
}
