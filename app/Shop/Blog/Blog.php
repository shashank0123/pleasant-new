<?php

namespace App\Shop\Blog;

use App\Shop\Blog\Blog;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'description',
        'cover',
        'status',
        'author'
    ];

}
