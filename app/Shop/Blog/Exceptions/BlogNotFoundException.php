<?php

namespace App\Shop\Blog\Exceptions;

class BlogNotFoundException extends \Exception
{
}
