<?php

namespace App\Shop\Slider\Repositories;

use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Slider\Exceptions\SliderCreateErrorException;
use App\Shop\Slider\Exceptions\SliderUpdateErrorException;
use App\Shop\Tools\UploadableTrait;
use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Slider\Exceptions\SliderNotFoundException;
use App\Shop\Slider\Slider;
use App\Shop\Slider\Repositories\Interfaces\SliderRepositoryInterface;
use App\Shop\Slider\Transformations\SliderTransformable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class SliderRepository extends BaseRepository implements SliderRepositoryInterface
{
    use SliderTransformable, UploadableTrait;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Slider $slider)
    {
        parent::__construct($slider);
        $this->model = $slider;
    }

    /**
     * List all the products
     *
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return Collection
     */
    public function listSliders(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * Create the product
     *
     * @param array $data
     *
     * @return Product
     * @throws SliderCreateErrorException
     */
    public function createSlider(array $data) : Slider
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new SliderCreateErrorException($e);
        }
    }

    /**
     * Update the product
     *
     * @param array $data
     *
     * @return bool
     * @throws SliderUpdateErrorException
     */
    public function updateSlider(array $data) : bool
    {
        $filtered = collect($data)->except(['_token','_method'])->all();

        try {
        // var_dump($data); die;
            return $this->model->where('id', $this->model->id)->update($filtered);
        } catch (QueryException $e) {
            throw new SliderUpdateErrorException($e);
        }
    }

    /**
     * Find the product by ID
     *
     * @param int $id
     *
     * @return Slider
     * @throws SliderNotFoundException
     */
    public function findSliderById(int $id) : Slider
    {
        try {
            return $this->transformSlider($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new SliderNotFoundException($e);
        }
    }

    /**
     * Delete the Slider
     *
     * @param Slider $slider
     *
     * @return bool
     * @throws \Exception
     * @deprecated
     * @use removeSlider
     */
    public function deleteSlider(Slider $slider) : bool
    {
        // $slider->images()->delete();
        return $slider->delete();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function removeSlider() : bool
    {
        return $this->model->where('id', $this->model->id)->delete();
    }

    /**
     * Detach the categories
     */
   
    /**
     * Return the categories which the product is associated with
     *
     * @return Collection
     */
    
    /**
     * Sync the categories
     *
     * @param array $params
     */
   

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        return $this->update(['cover' => null], $file['slider']);
    }

    /**
     * @param string $src
     * @return bool
     */
    

    /**
     * Get the product via slug
     *
     * @param array $slug
     *
     * @return Product
     * @throws SliderNotFoundException
     */
    public function findSliderBySlug(array $slug) : Slider
    {
        try {
            return $this->findOneByOrFail($slug);
        } catch (ModelNotFoundException $e) {
            throw new SliderNotFoundException($e);
        }
    }

    /**
     * @param string $text
     * @return mixed
     */
    public function searchSlider(string $text) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchSlider($text);
        } else {
            return $this->listSliders();
        }
    }

    /**
     * @return mixed
     */
   

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('sliders', ['disk' => 'public']);
    }

}
