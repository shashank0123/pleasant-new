<?php

namespace App\Shop\Slider\Transformations;

use App\Shop\Slider\Slider;
use Illuminate\Support\Facades\Storage;

trait SliderTransformable
{
    /**
     * Transform the Blog
     *
     * @param Blog $Blog
     * @return Blog
     */
    protected function transformSlider(Slider $slider)
    {
        $prod = new Slider;
        $prod->id = (int) $slider->id;
        $prod->title = $slider->title;
        $prod->slug = $slider->slug;
        $prod->type = $slider->type;
        $prod->cover = asset("storage/$slider->cover");
        $prod->status = $slider->status;
        
        return $prod;
    }
}
