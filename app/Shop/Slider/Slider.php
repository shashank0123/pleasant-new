<?php

namespace App\Shop\Slider;

use App\Shop\Slider\Slider;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'type',
        'cover',
        'status'
    ];

}
