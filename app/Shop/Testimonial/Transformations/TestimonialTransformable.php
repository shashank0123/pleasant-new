<?php

namespace App\Shop\Testimonial\Transformations;

use App\Shop\Testimonial\Testimonial;
use Illuminate\Support\Facades\Storage;

trait TestimonialTransformable
{
    /**
     * Transform the Testimonial
     *
     * @param Testimonial $Testimonial
     * @return Testimonial
     */
    protected function transformTestimonial(Testimonial $test)
    {
        $prod = new Testimonial;
        $prod->id = (int) $test->id;
        $prod->name = $test->name;
        $prod->profession = $test->profession;
        $prod->description = $test->description;
        $prod->cover = asset("storage/$test->cover");
        $prod->status = $test->status;
        
        return $prod;
    }
}
