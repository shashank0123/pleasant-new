<?php

namespace App\Shop\Testimonial\Repositories;

use App\Shop\AttributeValues\AttributeValue;
use App\Shop\Testimonial\Exceptions\TestimonialCreateErrorException;
use App\Shop\Testimonial\Exceptions\TestimonialUpdateErrorException;
use App\Shop\Tools\UploadableTrait;
use Jsdecena\Baserepo\BaseRepository;
use App\Shop\Testimonial\Exceptions\TestimonialNotFoundException;
use App\Shop\Testimonial\Testimonial;
use App\Shop\Testimonial\Repositories\Interfaces\TestimonialRepositoryInterface;
use App\Shop\Testimonial\Transformations\TestimonialTransformable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class TestimonialRepository extends BaseRepository implements TestimonialRepositoryInterface
{
    use TestimonialTransformable, UploadableTrait;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Testimonial $Testimonial)
    {
        parent::__construct($Testimonial);
        $this->model = $Testimonial;
    }

    /**
     * List all the products
     *
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return Collection
     */
    public function listTestimonials(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * Create the product
     *
     * @param array $data
     *
     * @return Product
     * @throws TestimonialCreateErrorException
     */
    public function createTestimonial(array $data) : Testimonial
    {
        try {
            return $this->create($data);
        } catch (QueryException $e) {
            throw new TestimonialCreateErrorException($e);
        }
    }

    /**
     * Update the product
     *
     * @param array $data
     *
     * @return bool
     * @throws TestimonialUpdateErrorException
     */
    public function updateTestimonial(array $data) : bool
    {
        $filtered = collect($data)->except(['_token','_method'])->all();

        try {
        // var_dump($data); die;
            return $this->model->where('id', $this->model->id)->update($filtered);
        } catch (QueryException $e) {
            throw new TestimonialUpdateErrorException($e);
        }
    }

    /**
     * Find the product by ID
     *
     * @param int $id
     *
     * @return Testimonial
     * @throws TestimonialNotFoundException
     */
    public function findTestimonialById(int $id) : Testimonial
    {
        try {
            return $this->transformTestimonial($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new TestimonialNotFoundException($e);
        }
    }

    /**
     * Delete the Testimonial
     *
     * @param Testimonial $Testimonial
     *
     * @return bool
     * @throws \Exception
     * @deprecated
     * @use removeTestimonial
     */
    public function deleteTestimonial(Testimonial $Testimonial) : bool
    {
        // $Testimonial->images()->delete();
        return $Testimonial->delete();
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function removeTestimonial() : bool
    {
        return $this->model->where('id', $this->model->id)->delete();
    }

    /**
     * Detach the categories
     */
   
    /**
     * Return the categories which the product is associated with
     *
     * @return Collection
     */
    
    /**
     * Sync the categories
     *
     * @param array $params
     */
   

    /**
     * @param $file
     * @param null $disk
     * @return bool
     */
    public function deleteFile(array $file, $disk = null) : bool
    {
        return $this->update(['cover' => null], $file['testimonial']);
    }

    /**
     * @param string $src
     * @return bool
     */
    

    /**
     * Get the product via slug
     *
     * @param array $slug
     *
     * @return Product
     * @throws TestimonialNotFoundException
     */
    public function findTestimonialBySlug(array $slug) : Testimonial
    {
        try {
            return $this->findOneByOrFail($slug);
        } catch (ModelNotFoundException $e) {
            throw new TestimonialNotFoundException($e);
        }
    }

    /**
     * @param string $text
     * @return mixed
     */
    public function searchTestimonial(string $text) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchTestimonial($text);
        } else {
            return $this->listTestimonials();
        }
    }

    /**
     * @return mixed
     */
   

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveCoverImage(UploadedFile $file) : string
    {
        return $file->store('testimonials', ['disk' => 'public']);
    }

}
