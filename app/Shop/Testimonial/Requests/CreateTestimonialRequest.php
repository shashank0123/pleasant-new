<?php

namespace App\Shop\Testimonial\Requests;

use App\Shop\Base\BaseFormRequest;

class CreateTestimonialRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'profession' => ['required'],
            'description' => ['required'],
            'cover' => ['required', 'file', 'image:png,jpeg,jpg,gif']
        ];
    }
}
