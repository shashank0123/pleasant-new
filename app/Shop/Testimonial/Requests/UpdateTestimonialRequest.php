<?php

namespace App\Shop\Testimonial\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class UpdateTestimonialRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'profession' => ['required'],
            'description' => ['required']
        ];
    }
}
