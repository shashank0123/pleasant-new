<?php
   use App\Shop\Products\Product;
   use App\Shop\Categories\Category;
   $allCategories = Category::where('parent_id',NULL)
   ->where('status','1')
   ->orderBy('id','DESC')
   ->limit(5)
   ->get();
   
   $countAll = Category::where('parent_id',NULL)
   ->where('status','1')
   ->orderBy('id','DESC')
   ->count();
   
   $lastId = 0;
   $cart_total = 0;
   if(!empty(session()->get('cart')))
   $cart_total = count(session()->get('cart')) ?? '0';
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      {{-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-9325492-23"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         
         gtag('config', '{{ env('GOOGLE_ANALYTICS') }}');
      </script> --}}
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=devi
         ce-width, initial-scale=1">
      <title>{{ config('app.name') }}</title>
      <title>Pleasent Matress</title>
      <meta name="description" content="Modern open-source e-commerce framework for free">
      <meta name="tags" content="modern, opensource, open-source, e-commerce, framework, free, laravel, php, php7, symfony, shop, shopping, responsive, fast, software, blade, cart, test driven, adminlte, storefront">
      <meta name="author" content="Jeff Simons Decena">
      <!-- Social meta ================================================== -->
      <meta property="og:type" content="website">
      <meta property="og:title" content="{{ config('app.name')}}">
      <meta property="og:url" content="{{url('/')}}">
      <meta property="og:image" content="/images/t/4/assets/logoda35.png">
      <meta property="og:image:secure_url" content="/images/t/4/assets/logoda35.png">
      <meta property="og:site_name" content="{{ config('app.name')}}">
      <meta name="twitter:card" content="summary">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <!-- Helpers ================================================== -->
      <link rel="canonical" href="{{url('/')}}">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <meta name="theme-color" content="#ffffff">
      <!-- CSS ================================================== -->
      <link href="/css/frame.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/prettyPhoto.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/slick-theme.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/slick.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/slick-slider.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/lightslider.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/animate.css" rel="stylesheet" type="text/css" media="all" />
      <link href="/css/font-all.min.css?v=15722457319706631789" rel="stylesheet" type="text/css" media="all" />
      <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Nunito:300,300italic,400,600,400italic,600italic,700,700italic,800,800italic">
      <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Abel:300,300italic,400,600,400italic,600italic,700,700italic,800,800italic">
      <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Abel:300,300italic,400,600,400italic,600italic,700,700italic,800,800italic">
      <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="{{ asset('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
      <script src="{{ asset('https://oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
      <![endif]-->
      <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicons/apple-icon-57x57.png')}}">
      <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicons/apple-icon-60x60.png')}}">
      <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicons/apple-icon-72x72.png')}}">
      <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicons/apple-icon-76x76.png')}}">
      <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicons/apple-icon-114x114.png')}}">
      <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicons/apple-icon-120x120.png')}}">
      <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicons/apple-icon-144x144.png')}}">
      <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicons/apple-icon-152x152.png')}}">
      <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-icon-180x180.png')}}">
      <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicons/android-icon-192x192.png')}}">
      <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png')}}">
      <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicons/favicon-96x96.png')}}">
      <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png')}}">
      <link rel="manifest" href="{{ asset('favicons/manifest.json')}}">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png')}}">
      <meta name="theme-color" content="#ffffff">
      @yield('css')
      <meta property="og:url" content="{{ request()->url() }}"/>
      @yield('og')
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js') }}"></script>
      <script integrity="sha256-f6FFDo+K6xVN2lCe7Gwex/21wPD5ADi++haIzgy99/U=" crossorigin="anonymous" data-source-attribution="shopify.loadfeatures" defer="defer" src="/js/load_feature.js"></script>
      <script integrity="sha256-EYppj7RbseKnaugbP4EJXR4sMs7TPdTpPmQ3i163eNA=" data-source-attribution="shopify.dynamic-checkout" defer="defer" src="/js/features.js" crossorigin="anonymous"></script>
      <script id="sections-script" data-sections="slideshow,product-tab-type-9-sorting,testimonial-type-1,header-model-1" defer="defer" src="/images/t/4/compiled_assets/scripts3d48.js?6998"></script>
      <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
      <script src="//cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/respond.min.js?v=5224867783754261923" type="text/javascript"></script>
      <link href="//cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
      <link href="//softie-demo.myshopify.com/search?q=bd2754bbefaccb4421038bc0d1f0bbdf" id="respond-redirect" rel="respond-redirect" />
      <script src="//softie-demo.myshopify.com/search?q=bd2754bbefaccb4421038bc0d1f0bbdf" type="text/javascript"></script>
      <![endif]-->
      <script src="/js/header.js" type="text/javascript"></script> 
      <script>
         // Wait for window load
         $(window).load(function() {     
           var loader = $( '.se-pre-con' );
           if ( loader.length ) {
             $( window ).on( 'beforeunload', function() {
               loader.fadeIn( 500, function() {
                 loader.children().fadeIn( 100 )
               });
             });
             loader.fadeOut(1500 );
             loader.children().fadeOut();
           }
         
         });
      </script> 
      <script>
         window.use_sticky = true;
         window.ajax_cart = true;
         window.money_format = "<span class=money>RS {{10}} INR</span>";
         window.shop_currency = "INR";
         window.show_multiple_currencies = true;
         window.enable_sidebar_multiple_choice = true;
         window.loading_url = "/images/loading.gif";     
         window.dropdowncart_type = "hover";
         window.file_url = '<?php echo url('/');?>';
         window.asset_url = "";
         window.items="Items";
         window.many_in_stock="Many In Stock";
         window.out_of_stock=" Out of stock";
         window.in_stock=" In Stock";
         window.unavailable="Unavailable";
         window.product_name="Product Name";
         window.product_image="Product Image";
         window.product_desc="Product Description";
         window.available_stock="Available In stock";
         window.unavailable_stock="Unavailable In stock";
         window.compare_note="Product Added over 8 product !. Do you want to compare 8 added product ?";
         window.added_to_cmp="Added to compare";
         window.add_to_cmp="Add to compare";
         window.select_options="Select options";
         window.add_to_cart="Add to Cart";
         window.confirm_box="Yes,I want view it!";
         window.cancelButtonText="Continue";
         window.remove="Remove";
         window.use_color_swatch = true;
         window.newsletter_popup = true; 
         
         var  compare_list = []; 
      </script>  
   </head>
   <body id="softie" class="template-index " >
      <noscript>
         <p class="alert alert-danger">
            You need to turn on your javascript. Some functionality will not work if this is disabled.
            <a href="https://www.enable-javascript.com/" target="_blank">Read more</a>
         </p>
      </noscript>
      <div class="se-pre-con"></div>
      <div id="PageContainer"></div>
      <!-- <div class="quick-view"></div> -->
      <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left " id="cbp-spmenu-s1">
         <div class="gf-menu-device-wrapper">
            <div class="close-menu">x</div>
            <div class="gf-menu-device-container"></div>
         </div>
      </nav>
      <div class="wrapper-container">
         <div class="header-type-1">
            <div id="shopify-section-top-bar-type-1" class="shopify-section">
               <div class="wrapper">
                  <div class="top_bar">
                     <ul class="top_bar_left">
                        <li><span>Toll Free : (00) 000 111 222</span></li>
                        <li>Mail Info :<a href="mailto:info@pleasantmattress.com"> info@pleasantmattress.com</a></li>
                     </ul>
                     <div class="top_bar_menu">
                        <div class="customer_account">
                           <ul>
                              @if(auth()->check())
                              <li><a href="{{ route('accounts', ['tab' => 'profile']) }}"><i class="fa fa-home"></i> My Account</a></li>
                              <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                              @else
                              <li><a href="{{ route('login') }}"> <i class="fas fa-sign-in-alt"></i> Login</a></li>
                              <li><a href="{{ route('register') }}"> <i class="fas fa-user"></i> Register</a></li>
                              @endif                         
                              <li>            
                                 <a class="wishlist" href="@if(auth()->check()){{url('wishlist')}}@else{{url('login')}}@endif" title="Wishlist"><i class="far fa-heart"></i></a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               <style>
                  /* Top block */
                  .header-type-1 .top_bar { background: #e7eeef; }
                  .header-type-1 .top_bar li, .header-type-1 .top_bar span { color:#676e6d;}     
                  .header-type-1 .top_bar a,.header-type-1 .top_bar button { color:#676e6d;}    
                  .header-type-1 .top_bar a:hover,.header-type-1 .top_bar button:hover { color:#69e0ff;}    
                  .header-type-1 .top_bar_menu li a,.header-type-1 .top_bar li span { border-color: #35b8f4;}  
               </style>
            </div>
            <header class="site-header">
               <div class="header-sticky">
                  <div id="header-landing" class="sticky-animate">
                     <div class="wrapper">
                        <div class="grid--full site-header__menubar">
                           <h1 class="site-header__logo order-header" itemscope itemtype="http://schema.org/Organization">
                              <a href="{{url('/')}}">
                              <img class="normal-logo" src="/images/t/4/assets/logoda35.png" alt="Softie" itemprop="logo">
                              </a>
                           </h1>
                           <div class="grid__item menubar-section">
                              <div class="desktop-megamenu grid__item wide--four-sixths post-large--four-sixths large--four-sixths medium--one-half small-grid__item" >
                                 <div id="shopify-section-navigation" class="shopify-section">
                                    <div class="nav-bar-mobile">
                                       <nav class="nav-bar" role="navigation">
                                          <div class="site-nav-dropdown_inner">
                                             <div class="menu-tool">
                                                <ul class="site-nav">
                                                   <li class=" ">
                                                      <a  href="{{url('/')}}" class="current">
                                                      <span>         
                                                      Home     
                                                      </span>       
                                                      </a>  
                                                   </li>
                                                   @if(count($allCategories)>0)
                                                   @foreach($allCategories as $cat1)
                                                   <li class=" dropdown  mega-menu ">
                                                      <a  href="{{ url('products/'.$cat1->slug ?? '') }}" class="">
                                                      <span>         
                                                      {{ $cat1->name ?? '' }}     
                                                      </span>       
                                                      </a>  
                                                      <?php
                                                         $categories2 = Category::where('parent_id',$cat1->id)->where('status','1')->get();
                                                         ?>
                                                      @if(count($categories2)>0)
                                                      <div class="site-nav-dropdown">
                                                         <div class="container   style_1">
                                                            <div class="col-1 parent-mega-menu">
                                                               @foreach($categories2 as $cat2) 
                                                               <div class="inner col-xs-12 col-sm-4">
                                                                  <!-- Menu level 2 -->
                                                                  <a  href="{{ url('products/'.$cat2->slug ?? '') }}" class=" ">
                                                                  {{ $cat2->name ?? '' }}     
                                                                  </a>
                                                                  <?php
                                                                     $categories3 = Category::where('parent_id',$cat2->id)->where('status','1')->get();
                                                                     ?>
                                                                  @if(count($categories3)>0)
                                                                  <ul class="dropdown">
                                                                     <!-- Menu level 3 -->
                                                                     @foreach($categories3 as $cat3)
                                                                     <li>
                                                                        <a href="{{ url('products/'.$cat3->slug ?? '') }}" >
                                                                        {{ $cat3->name ?? '' }}    
                                                                        </a>
                                                                     </li>
                                                                     @endforeach
                                                                  </ul>
                                                                  @else
                                                                  <?php
                                                                     $products = Product::join('category_product','category_product.product_id','products.id')
                                                                     ->where('products.status','1')->where('category_product.category_id',$cat2->id)->orderBy('created_at','DESC')->limit(6)->get();
                                                                     ?>
                                                                  @if(count($products)>0)
                                                                  <ul class="dropdown">
                                                                     <!-- Menu level 3 -->
                                                                     @foreach($products as $product)
                                                                     <li>
                                                                        <a href="{{ url('product-detail/'.$product->slug ?? '') }}" >
                                                                        {{ $product->name ?? '' }}    
                                                                        </a>
                                                                     </li>
                                                                     @endforeach
                                                                  </ul>
                                                                  @endif
                                                                  @endif
                                                               </div>
                                                               @endforeach
                                                            </div>
                                                            <div class="col-2">
                                                               <a href="#" title="">
                                                               <img src="{{ asset('storage/'.$cat1->cover) }}" alt="{{ $cat1->name ?? '' }}" style="height:325px" />
                                                               </a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      @endif   
                                                   </li>
                                                   @php $lastId = $cat1->id @endphp
                                                   @endforeach
                                                   @endif
                                                   @if($countAll>5)
                                                   <?php
                                                      $nextCategories = Category::where('id','>',$lastId)->where('parent_id',NULL)->where('status','1')->get();
                                                      
                                                      ?>
                                                   <li class="  dropdown">
                                                      <a class="menu__moblie"  href="#" class="">
                                                      <span>         
                                                      More      
                                                      </span>       
                                                      </a>    
                                                      <ul class="site-nav-dropdown">
                                                         @foreach($nextCategories as $cat)
                                                         <li >
                                                            <a href="{{ url('products/'.$cat->slug ?? '') }}" class="">               
                                                            <span>               
                                                            {{ $cat->name ?? '' }}                
                                                            </span>
                                                            </a>
                                                            <ul class="site-nav-dropdown">
                                                            </ul>
                                                         </li>
                                                         @endforeach
                                                      </ul>
                                                   </li>
                                                   @endif
                                                   <li class="  dropdown">
                                                      <a class="menu__moblie"  href="#" class="">
                                                      <span>         
                                                      Pages      
                                                      </span>       
                                                      </a>    
                                                      <ul class="site-nav-dropdown">
                                                         <li >
                                                            <a href="{{ url('about_us') }}" class="">               
                                                            <span>               
                                                            About us                
                                                            </span>
                                                            </a>
                                                            <ul class="site-nav-dropdown">
                                                            </ul>
                                                         </li>
                                                         <li >
                                                            <a href="{{ url('contact_us') }}" class="">               
                                                            <span>               
                                                            Contact us                
                                                            </span>
                                                            </a>
                                                            <ul class="site-nav-dropdown">
                                                            </ul>
                                                         </li>
                                                         <li >
                                                            <a href="{{ url('blogs') }}" class="">               
                                                            <span>               
                                                            Blog                
                                                            </span>
                                                            </a>
                                                            <ul class="site-nav-dropdown">
                                                            </ul>
                                                         </li>
                                                         <!-- <li >
                                                            <a href="pages/faqs.html" class="">               
                                                            <span>               
                                                            Faq&#39;s                
                                                            </span>
                                                            </a>
                                                            <ul class="site-nav-dropdown">
                                                            </ul>
                                                         </li> -->
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </nav>
                                    </div>
                                 </div>
                              </div>
                              <div id="shopify-section-header-model-1" class="shopify-section">
                                 <ul class="menu_bar_right grid__item wide--one-sixth post-large--one-sixth large--one-sixth">
                                    <li class="header-mobile">
                                       <div class="menu-block visible-phone">
                                          <!-- start Navigation Mobile  -->
                                          <div id="showLeftPush">
                                             <i class="fa fa-bars" aria-hidden="true">  </i>
                                          </div>
                                       </div>
                                       <!-- end Navigation Mobile  --> 
                                    </li>
                                    <li class="header-search">
                                       <div id="sb-search" class="sb-search post-large--hide large--hide medium-down--hide" >
                                          <form action="{{ url('searchedProducts') }}" method="get" class="search-bar__table-cell search-bar__form" role="search">
                                             
                                             <input type="text" id="search" name="search" value="" placeholder="Search..." aria-label="Search..." class="search-bar__input sb-search-input">
                                             <input class="sb-search-submit" type="submit" value="">
                                             <span class="sb-icon-search fa fa-search"></span>
                                          </form>
                                       </div>
                                       <div class="wide--hide post-large--show large--show medium-down--show">
                                          <form action="{{ url('searchedProducts') }}" method="get" class="search-bar__table-cell search-bar__form" role="search">                           
                                             <input type="text" id="search" name="keyword" value="" placeholder="Search..." aria-label="Search..." class="search-bar__input sb-search-input">
                                             <button class="sb-search-submit res_btn" type="submit" value=""><i class="fa fa-search"></i></button>
                                          </form>
                                       </div>
                                    </li>
                                   <!--  <li class="header_currency">
                                       <ul class="tbl-list">
                                          <li class="currency dropdown-parent uppercase currency-block">
                                             <div class="selector-arrow">
                                                <select class="currencies_src" id="currencies">
                                                   <option data-currency="INR"  selected  value="INR">INR</option>
                                                   <option data-currency="EUR"  value="EUR">EUR</option>
                                                   <option data-currency="GBP"  value="GBP">GBP</option>
                                                   <option data-currency="AUD"  value="AUD">AUD</option>
                                                   <option data-currency="USD"  value="USD">USD</option>
                                                   <option data-currency="JPY"  value="JPY">JPY</option>
                                                   <option data-currency="CAD"  value="CAD">CAD</option>
                                                   <option data-currency="CNY"  value="CNY">CNY</option>
                                                   <option data-currency="AED"  value="AED">AED</option>
                                                   <option data-currency="RUB"  value="RUB">RUB</option>
                                                </select>
                                             </div>
                                          </li>
                                       </ul>
                                    </li> -->
                                    <li class="header-bar__module cart header_cart">
                                       <!-- Mini Cart Start -->
                                       <div class="baskettop">
                                          <div class="wrapper-top-cart">
                                             <a href="javascript:void(0)" id="ToggleDown" onmouseover="getMiniCart()" class="icon-cart-arrow">
                                                <i class="fas fa-shopping-bag"></i>
                                                <div class="detail">
                                                   <div id="cartCount"> 
                                                      {{ $cart_total ?? ''}}
                                                   </div>
                                                   <span class="cartCountspan">Item</span>
                                                   <!--  <div id="minicart_total">
                                                      <span> <span class=money>RS 0.00</span></span>
                                                      </div> -->
                                                </div>
                                             </a>
                                             <div id="slidedown-cart" style="overflow: hidden; display: none;">
                                                <!--  <h3>Shopping cart</h3>-->
                                                
                                             </div>
                                          </div>
                                       </div>
                                       <!-- End Top Header -->  
                                    </li>
                                 </ul>
                                 <style> 
                                    /* Logo block */
                                    .header-type-1 .site-header__logo { background: rgba(0,0,0,0);}    
                                    .header-type-1 .site-header__logo a,.header-type-1 .header-mobile #showLeftPush { color:#676e6d;}
                                    .header-type-1 .site-header__logo a:hover,.header-type-1 .header-mobile #showLeftPush:hover { color:#69e0ff;}    
                                    /* Menu  block */
                                    .header-type-1 .menubar-section,.mobile-nav-section,.header-type-1 .site-header__menubar {background: #ffffff;}
                                    .header-type-1 .menu-tool ul li,.header-mobile #showLeftPush {color: ;}
                                    .header-type-1 .menu-tool ul li a,.mobile-nav-section .mobile-nav-trigger,.header-mobile #showLeftPush:hover {color:#676e6d;}  
                                    .header-type-1 .menu-tool ul li a:hover,.header-type-1 .menu-tool .site-nav > li > a.current:hover {color:#69e0ff;} 
                                    .header-type-1 .menu-tool .site-nav >  li > a.current {color:#69e0ff;} 
                                    .header-type-1 .site-nav-dropdown,#MobileNav,.mobile-nav__sublist { background: #fff;}
                                    .header-type-1 .site-nav-dropdown .inner > a {color: #11423b;}    
                                    .header-type-1 .site-nav-dropdown .inner > a:hover {color: #69e0ff;}    
                                    .header-type-1 .site-nav-dropdown .inner .dropdown a,.header-type-1 .menu-tool .site-nav .site-nav-dropdown li a,.header-type-1 .site-nav .widget-featured-product .product-title,.header-type-1 .site-nav .widget-featured-product .widget-title h3,#MobileNav a,.mobile-nav__sublist a,.site-nav .widget-featured-nav .owl-prev a,.site-nav .widget-featured-nav .owl-next a  {color: #676e6d;}
                                    .header-type-1 .site-nav-dropdown .inner .dropdown a:hover,.header-type-1 .menu-tool .site-nav .site-nav-dropdown li a:hover,.header-type-1 .site-nav-dropdown .inner .dropdown a.current,.header-type-1 .menu-tool .site-nav .site-nav-dropdown li a.current,.header-type-1 .site-nav .widget-featured-product .product-title:hover,#MobileNav a.current,.mobile-nav__sublist a.current,.site-nav .widget-featured-nav .owl-prev a:hover,.site-nav .widget-featured-nav .owl-next a:hover {color: #69e0ff;}    
                                    /* Dropdown block 
                                    .header-type-1 .menubar-section #Togglemodal i {color: ;}
                                    .header-type-1 .menubar-section #Togglemodal i:hover {color: ;}
                                    .header-type-1 #slidedown-modal {background: ;}
                                    .header-type-1 #slidedown-modal ul li a {color:;} 
                                    .header-type-1 #slidedown-modal ul li a:hover {color:;} 
                                    */
                                    /* Search block */     
                                    .header-type-1 .header-search input#search {color:#676e6d;} 
                                    .header-type-1 .header-search span,.header-type-1 .search-bar__form button.res_btn  {color:#676e6d;} 
                                    .header-type-1 .header-search span:hover,.header-type-1 .search-bar__form button.res_btn:hover {color:#69e0ff;} 
                                    .header-type-1 .sb-search.sb-search-open input[type="text"] { border:1px solid #e4e4e4; background:#ffffff;}
                                    .header-type-1 .sb-search.sb-search-open .sb-icon-search { color:#11423b;background:#e7eeef;}
                                    .header-type-1 .sb-search.sb-search-open .sb-icon-search:hover, .header-type-5 .sb-icon-search:hover { color:#676e6d;background:#69e0ff;}
                                    .header-type-1 .search-bar__form button.res_btn { background:none;border:none;height:auto; }
                                    .header-type-1 .header-search input#search::-webkit-input-placeholder  { /* Chrome/Opera/Safari */
                                    color:#676e6d;
                                    }
                                    .header-type-1 .header-search input#search::-moz-placeholder { /* Firefox 19+ */
                                    color:#676e6d;
                                    }
                                    .header-type-1 .header-search input#search:-ms-input-placeholder { /* IE 10+ */
                                    color:#676e6d;
                                    }
                                    .header-type-1 .header-search input#search:-moz-placeholder { /* Firefox 18- */
                                    color:#676e6d;
                                    }
                                    /* Cart Summary block */
                                    .header-type-1 a.icon-cart-arrow i,.header-type-1 #minicart_total,.header-type-1 #cartCount,.header-type-1 .cartCountspan  {color: #676e6d;}
                                    .header-type-1 a.icon-cart-arrow:hover i {color: #69e0ff;}
                                    .header-type-1 #slidedown-cart .actions, .header-type-1  #slidedown-cart  {background: #ffffff;}
                                    .header-type-1 .header-bar__module p {color: #676e6d;}
                                    .header-type-1 .header-bar__module a {color:#676e6d;}  
                                    .header-type-1 .header-bar__module a:hover {color:#69e0ff;} 
                                    .header-type-1 .header-bar__module .btn {color:#ffffff;background: #11423b;} 
                                    .header-type-1 .header-bar__module .btn:hover {color:#ffffff;background: #69e0ff;} 
                                    .header-type-1  #slidedown-cart .total .price,.header-type-1 #slidedown-cart ul li .cart-collateral {color:#000000;} 
                                    .header-type-1 #slidedown-cart li { border-bottom:1px solid #e4e4e4; }
                                    /* Currency block */
                                    .header-type-1 .header_currency ul select,.header-type-1 .header_currency ul li.currency .selector-arrow::after {color:#676e6d;}   
                                    .header-type-1 .header_currency ul select:hover,.header-type-1 .header_currency ul li.currency .selector-arrow:hover:after {color:#69e0ff;}  
                                    .header-type-1 .header_currency ul li.currency:hover:after {border-top-color:#69e0ff;}
                                    .header-type-1 .header_currency ul li.currency:after {border-top-color:#676e6d;}
                                    .header-type-1 .header_currency option {background:#e7eeef;color:#676e6d;}
                                    /* Header borders */
                                    .header-type-1 .site-header__logo  { border-bottom:1px solid #e4e4e4; }
                                 </style>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </header>
         </div>
      </div>
      @yield('content')
      @include('layouts.front.footer')
      <script src="/images/t/4/assets/timber27ce.js?v=13748178311005876045" type="text/javascript"></script>  
      <script src="/images/t/4/assets/footer958d.js" type="text/javascript"></script>  
      <script src="/images/t/4/assets/theme10bd.js?v=18326697871268545370" type="text/javascript"></script>
      <script src="/js/option_selection.js" type="text/javascript"></script>
      <script src="/js/api.jquery.js" type="text/javascript"></script>    
      <script src="/images/t/4/assets/lightslider.minb81f.js" type="text/javascript"></script>
      <script src="/images/t/4/assets/shop44e7.js?v=723893649779524784" type="text/javascript"></script>    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

      <!-- @yield('scripts') -->
      <!-- Begin quick-view-template -->
      <div class="clearfix" id="quickview-template" style="display:none">
         <div class="overlay"></div>
         <div class="content clearfix">
            <div class="product-img images">
               <div class="quickview-featured-image product-photo-container"></div>
               <div class="more-view-wrapper">
                  <ul class="product-photo-thumbs quickview-more-views-owlslider owl-carousel owl-theme">
                  </ul>
                  <div class="quick-view-carousel"></div>
               </div>
            </div>
            <div class="product-shop summary">
               <div class="product-item product-detail-section">
                  <h2 class="product-title"><a>&nbsp;</a></h2>
                  <p class="item-text product-description"></p>
                  <div class="prices product_price">
                     <label>Effective Price :</label>
                     <div class="price h4" id="QProductPrice"></div>
                     <div class="compare-price" id="QComparePrice"></div>
                  </div>
                  <div class="product-infor">
                     <p class="product-inventory"><label>Availability :</label><span></span></p>
                  </div>
                  <div class="details clearfix">
                     <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="variants">
                        <select name='id' style="display:none"></select>
                        <div class="qty-section quantity-box">
                           <label>Quantity :</label>
                           <div class="dec button qtyminus">-</div>
                           <input type="number" name="quantity" id="Qty" value="1" class="quantity">
                           <div class="inc button qtyplus">+</div>
                        </div>
                        <div class="total-price">
                           <label>Subtotal :</label><span class="h5"></span>
                        </div>
                        <div class="actions">
                           <button type="button" class="add-to-cart-btn btn">
                           Add to Cart
                           </button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <a href="javascript:void(0)" class="close-window"></a> 
         </div>
      </div>
      <!-- End of quick-view-template -->
      @include('front.scripts.script')
      <script type="text/javascript">  
         Shopify.doNotTriggerClickOnThumb = false; 
         
         var selectCallbackQuickview = function(variant, selector) {
           var productItem = jQuery('.quick-view .product-item');
           addToCart = productItem.find('.add-to-cart-btn'),
           productPrice = productItem.find('.price'),
           comparePrice = productItem.find('.compare-price'),
           totalPrice = productItem.find('.total-price span'),
           inventory = productItem.find('.product-inventory');
           if (variant) {
             if (variant.available) {          
                 // We have a valid product variant, so enable the submit button
                 addToCart.removeClass('disabled').removeAttr('disabled').text('Add to Cart');         
         
               } else {
                 // Variant is sold out, disable the submit button
                 addToCart.val('Sold Out').addClass('disabled').attr('disabled', 'disabled');         
               }
         
               // Regardless of stock, update the product price
               productPrice.html(Shopify.formatMoney(variant.price, "<span class=money>RS {{10}}</span>"));
         
               // Also update and show the product's compare price if necessary
               if ( variant.compare_at_price > variant.price ) {
                 comparePrice
                 .html(Shopify.formatMoney(variant.compare_at_price, "<span class=money>RS {{10}}</span>"))
                 .show();
                 productPrice.addClass('on-sale');
               } else {
                 comparePrice.hide();
                 productPrice.removeClass('on-sale');
               }
         
         
         
            //update variant inventory
            
            var inventoryInfo = productItem.find('.product-inventory span');
            if (variant.available) {
             if (variant.inventory_management !=null ) {
               inventoryInfo.text(window.in_stock );
               inventoryInfo.addClass('items-count');
               inventoryInfo.removeClass('many-in-stock');
               inventoryInfo.removeClass('out-of-stock');
               inventoryInfo.removeClass('unavailable');
             } else {
               inventoryInfo.text(window.many_in_stock);
               inventoryInfo.addClass('many-in-stock')
               inventoryInfo.removeClass('items-count');
               inventoryInfo.removeClass('out-of-stock');
               inventoryInfo.removeClass('unavailable');
             }
           } else {
             inventoryInfo.addClass('out-of-stock')
             inventoryInfo.text(window.out_of_stock);
             inventoryInfo.removeClass('items-count');
             inventoryInfo.removeClass('many-in-stock');
             inventoryInfo.removeClass('unavailable');
           }
         
           
           
           /*recaculate total price*/
               //try pattern one before pattern 2
               var regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;
               var unitPriceTextMatch = jQuery('.quick-view .price').text().match(regex);
         
               if (!unitPriceTextMatch) {
                 regex = /([0-9]+[.|,][0-9]+)/g;
                 unitPriceTextMatch = jQuery('.quick-view .price').text().match(regex);     
               }
         
               if (unitPriceTextMatch) {
                 var unitPriceText = unitPriceTextMatch[0];     
                 var unitPrice = unitPriceText.replace(/[.|,]/g,'');
                 var quantity = parseInt(jQuery('.quick-view input[name=quantity]').val());
                 var totalPrice = unitPrice * quantity;
         
                 var totalPriceText = Shopify.formatMoney(totalPrice, window.money_format);
                 regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;     
                 if (!totalPriceText.match(regex)) {
                   regex = /([0-9]+[.|,][0-9]+)/g;
                 } 
                 totalPriceText = totalPriceText.match(regex)[0];
         
                 var regInput = new RegExp(unitPriceText, "g"); 
                 var totalPriceHtml = jQuery('.quick-view .price').html().replace(regInput ,totalPriceText);
                 jQuery('.quick-view .total-price span').html(totalPriceHtml);             
               }
               /*end of price calculation*/
         
         
         
               Currency.convertAll('INR', jQuery('#currencies').val(), 'span.money', 'money_format');
               
         
               /*begin variant image*/
               /*begin variant image*/
               if (variant && variant.featured_image) {
                 var originalImage = jQuery(".quick-view .quickview-featured-image img");
                 var newImage = variant.featured_image;
                 var element = originalImage[0];
                 Shopify.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
                   newImageSizedSrc = newImageSizedSrc.replace(/\?(.*)/,"");
                   jQuery('.quick-view .more-view-wrapper img').each(function() {
                     var grandSize = jQuery(this).attr('src');
                     grandSize = grandSize.replace('medium','grande');
         
                     if (grandSize == newImageSizedSrc) {
                       jQuery(this).parent().trigger('click');              
                       return false;
                     }
                   });
                 });        
               }
               /*end of variant image*/   
               /*end of variant image*/ 
             } else {
               // The variant doesn't exist. Just a safegaurd for errors, but disable the submit button anyway
               addToCart.text('Unavailable').addClass('disabled').attr('disabled', 'disabled');
         
         
               var inventoryInfo = productItem.find('.product-inventory span');
               inventoryInfo.addClass("unavailable");
               inventoryInfo.removeClass("many-in-stock");
               inventoryInfo.removeClass("items-count");
               inventoryInfo.removeClass("out-of-stock");
               inventoryInfo.text(window.unavailable);
         
             }   
           };  
         
      </script>
      <div class="loading-modal compare_modal">Loading</div>
      <div class="ajax-success-compare-modal compare_modal" id="moda-compare" tabindex="-1" role="dialog" style="display:none">
         <div class="overlay"> </div>
         <div class="modal-dialog modal-lg">
            <div class="modal-content content" id="compare-modal">
               <div class="modal-header">
                  <h4 class="modal-title">Compare Products</h4>
               </div>
               <div class="modal-body">
                  <div class="table-wrapper">
                     <table class="table table-hover">
                        <thead>
                           <tr class="th-compare">
                              <th></th>
                           </tr>
                        </thead>
                        <tbody id="table-compare">
                        </tbody>
                     </table>
                  </div>
               </div>
               <div class="modal-footer">
                  <a href="javascript:void(0)" class="close-modal"><i class="far fa-times-circle"></i></a>
               </div>
            </div>
         </div>
      </div>
      <div class="loading-modal modal">Loading</div>
      <div class="newsletter-success-modal">
         <div class="modal-overlay"></div>
         <div class="ajax-modal-content">
            <a class="close close-window btn" title="Close (Esc)">
            <i class="fa fa-times"></i>
            </a>
            <i class="fa fa-check" aria-hidden="true"></i>
            <span>Thank you for signing up!</span>
         </div>
      </div>
      <div class="ajax-error-modal modal">
         <div class="modal-inner">
            <div class="ajax-error-title">Error</div>
            <div class="ajax-error-message"></div>
         </div>
      </div>
      <div class="ajax-success-modal modal">
         <div class="overlay"></div>
         <div class="content">
            <div class="ajax-left">
               <p class="added-to-cart info">Product successfully added to your shopping cart</p>
               <p class="added-to-wishlist info">translation missing: en.products.wishlist.added_to_wishlist</p>
               <img class="ajax-product-image" alt="modal window" src="{{url('/')}}" />
               <div class="ajax-cart-desc">
                  <h3 class="ajax-product-title"></h3>
                  <span class="ajax_price"></span>
                  <p>Qty:&nbsp;<span class="ajax_qty"></span></p>
               </div>
            </div>
            <div class="ajax-right">
               <p>There are <span class="ajax_cartCount"></span>&nbsp;item(s) in your cart</p>
               <span class="ajax_cartTotal"></span>
               <button class="btn continue-shopping" onclick="javascript:void(0)">Continue shopping</button>
               <div class="success-message added-to-cart"><a href="cart.html" class="btn"><i class="fas fa-shopping-cart"></i>View Cart</a> </div>
               <!--  <div class="success-message added-to-wishlist"> <a href="/pages/wishlist" class="btn"><i class="fa fa-heart"></i>View Wishlist</a></div>                -->
            </div>
            <a href="javascript:void(0)" class="close-modal"><i class="far fa-times-circle"></i></a>
         </div>
      </div>
      <script src="/js/currencies.js" type="text/javascript"></script>
      <script src="/js/jquery.currencies.min.js" type="text/javascript"></script>
      <script>      
         // Pick your format here:  
         // Can be 'money_format' or 'money_with_currency_format'
         Currency.format = 'money_format';
         var shopCurrency = 'INR';
         
         /* Sometimes merchants change their shop currency, let's tell our JavaScript file */
         Currency.moneyFormats[shopCurrency].money_with_currency_format = "RS {{10}} INR";
         Currency.moneyFormats[shopCurrency].money_format = "RS {{10}}";
         
         var cookieCurrency = Currency.cookie.read();
         
         // Fix for customer account pages 
         jQuery('span.money span.money').each(function() {
           jQuery(this).parent('span.money').removeClass('money');
         });
         
         // Add precalculated shop currency to data attribute 
         jQuery('span.money').each(function() {
           jQuery(this).attr('data-currency-INR', jQuery(this).html());
         });
         
         // Select all your currencies buttons.
         var currencySwitcher = jQuery('#currencies');
         
         // When the page loads.
         if (cookieCurrency == null || cookieCurrency == shopCurrency) {
           Currency.currentCurrency = shopCurrency;
         }
         else {
           Currency.currentCurrency = cookieCurrency;
           currencySwitcher.val(cookieCurrency);    
           Currency.convertAll(shopCurrency, cookieCurrency);    
         }
         //currencySwitcher.selectize();
         jQuery('.selectize-input input').attr('disabled','disabled');
         
         // When customer clicks on a currency switcher.
         currencySwitcher.change(function() {
          var newCurrency =  jQuery(this).val();
          Currency.cookie.write(newCurrency);
          Currency.convertAll(Currency.currentCurrency, newCurrency);    
             //show modal
             jQuery("#currencies-modal span").text(newCurrency);
             if (jQuery("#cart-currency").length >0) {
               jQuery("#cart-currency").text(newCurrency);
             }
             jQuery("#currencies-modal").fadeIn(500).delay(3000).fadeOut(500);    
           });
         
         // For product options.
         var original_selectCallback = window.selectCallback;
         var selectCallback = function(variant, selector) {
           original_selectCallback(variant, selector);
           Currency.convertAll(shopCurrency, jQuery('#currencies').val());
         };
      </script>
      <div class="newsletterwrapper" data-newsletter id="email-modal">
         <div class="modal-overlay">
            <div class="window-content">
               <div class="window-box">
                  <div class="window-box-bg">
                     <div class="newsletter-title border-title">
                        <h4 class="title">Get to know the latest offers</h4>
                     </div>
                     <p class="message h6">Subscribe and get notified at first on the latest update and offers!</p>
                     <div id="mailchimp-email-subscibe">
                        <div id="mc_embed_signup">
                           <form method="post" action="{{ url('submit-newsletter') }}" accept-charset="UTF-8" class="contact-form">
                              @csrf
                              <input type="email" value="" placeholder="Email address" name="email" class="mail" id="mail"  aria-label="Email address" required="required">
                              <input type="submit" class="btn"  value="Send"  id="subscribe" >
                           </form>
                        </div>
                        <input id="dismiss" type="checkbox" name="dismiss">
                        <label for="dismiss">
                        <span>We do not spam</span>
                        </label>
                     </div>
                  </div>
                  <a class="close btn close-window" title="Close (Esc)">
                  Liquid error: Could not find asset snippets/icon-close.liquid
                  </a>
               </div>
            </div>
         </div>
      </div>
      <script src="/js/wow.js" type="text/javascript"></script>  
      <script src="/js/class.js"></script>
      <script src="/js/jquery.prettyPhoto.js"></script>
      <script>
         $('.qtyplus').click(function(e){
           e.preventDefault();
           var currentVal = parseInt($('input[name="quantity"]').val());
           if (!isNaN(currentVal)) {
             $('input[name="quantity"]').val(currentVal + 1);
           } else {
             $('input[name="quantity"]').val(1);
           }
         
         });
         
         $(".qtyminus").click(function(e) {
         
           e.preventDefault();
           var currentVal = parseInt($('input[name="quantity"]').val());
           if (!isNaN(currentVal) && currentVal > 0) {
             $('input[name="quantity"]').val(currentVal - 1);
           } else {
             $('input[name="quantity"]').val(1);
           }
         
         });
      </script>
      <script type="text/javascript">
         $('.quick-view .close-window').click(function() {             
           $('.quick-view').switchClass("open-in","open-out");
         });
      </script>
      <script src="/js/uisearch.js"></script>
      <script src="/js/magnific-popup.js" type="text/javascript"></script>
      <script type="text/javascript">
         if ( $( '.p-video' ).length > 0 ) {
           $( '.jas-popup-url' ).magnificPopup({
             disableOn: 0,
             tLoading: '<div class="loader"><div class="loader-inner"></div></div>',
             type: 'iframe'
           });
         }
      </script>
      <style type="text/css">
         .bottom-products-suggest {
         background-color: #fff;
         border: 0;
         border-radius: 0;
         padding: 0;
         display: block;
         position: fixed;
         text-align: left;
         width: auto;
         min-width: 250px;
         left: 35px;
         bottom: 25px;
         z-index: 1000;
         }
         .bottom-products-suggest .product-short-suggest .product-thumb img {
         max-width: 50px;
         }
         .bottom-products-suggest .product-short-suggest {
         overflow: hidden;
         padding: 10px;
         background-color: #fff;
         border: 2px solid #efefef;
         }
         .bottom-products-suggest .product-short-suggest .product-thumb {
         float: right;
         margin-left: 10px;
         }
         .bottom-products-suggest .product-short-suggest .product-title {
         display: block;
         margin-bottom: 7px;
         font-weight: 500;
         font-size: 11px;
         line-height: 1.5;
         text-transform: uppercase;
         }
         .bottom-products-suggest .product-short-suggest .price del {
         color: #D0D0D0;
         }
      </style>
      <script>
         $(document).ready(function() {
           var body = $('body');
           var doc = $(document);
         
           var showLeftPush = $('#showLeftPush');
           var nav = $('#cbp-spmenu-s1');
         
           showLeftPush.on('click', function(e) {
             e.stopPropagation();
         
             body.toggleClass('cbp-spmenu-push-toright');
             nav.toggleClass('cbp-spmenu-open');
             showLeftPush.toggleClass('active');
           });
         
           $('.gf-menu-device-wrapper .close-menu').on('click', function() {
             showLeftPush.trigger('click');
           });
         
           doc.on('click', function(e) {
             if (!$(e.target).closest('#cbp-spmenu-s1').length && showLeftPush.hasClass('active')) {
               showLeftPush.trigger('click');
             }        
           });
         });
      </script>  
      <script type="text/javascript">
         $(document).ready(function() {
         
           $("#owl-demo").owlCarousel({
         
             navigation : true, // Show next and prev buttons
         
             slideSpeed : 300,
             paginationSpeed : 400,
         
             items : 1, 
             itemsDesktop : false,
             itemsDesktopSmall : false,
             itemsTablet: false,
             itemsMobile : false
         
           });
         
         });
      </script>
      @yield('js')
   </body>
</html>