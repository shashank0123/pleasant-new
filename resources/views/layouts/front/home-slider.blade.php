<div class="grid-uniform">
   <div class="grid__item">
      <!-- BEGIN content_for_index -->
      <div id="shopify-section-1542369329707" class="shopify-section index-section index-section--flush">
         <div class="wrapper">
            <div class="home-slideshow">
               <div class="variable-width slick-initialized slick-slider slick-dotted" data-slick="{dots: true, slidesToScroll: 1,autoplay:false,fade: true,autoplaySpeed:5000" role="toolbar">
                  <button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;"></button>
                  <div aria-live="polite" class="slick-list draggable">
                     <div class="slick-track" style="opacity: 1; width: 3789px;" role="listbox">
                        <div class="slick-list slider_style_1 slider-1542369873506-list slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 1263px; position: relative; left: 0px; top: 0px; z-index: 998; opacity: 0; transition: opacity 500ms ease 0s;">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/slider_1_9bbc576f-95fd-4392-b017-c13f2b2ea7e4_2000x.jpg?v=1549351429" class="slide-img medium-down--hide" alt="">

                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/responsive-slider-3_767x.jpg?v=1551863231" class="slide-img wide--hide post-large--hide large--hide" alt="">

                           <div class="slider-content slider-content-bg" style="">
                              <div class="container-bg">
                                 <div class="slide_left" style="">
                                    <h2 class="slide-heading animated " style=" 
                                       font-size: 60px;
                                       color:#69e0ff;
                                       ">
                                       Pregnancy Pillow                   
                                    </h2>
                                    <h5 class="slide-text animated " style="display:inline-block;
                                       font-size: 18px;
                                       color:#11423b;
                                       ">
                                       Lorem is a long established fact that a reader will be distracted by the readable content of a page.
                                    </h5>
                                    <div class="grid__item">
                                       <a href="" class="slide-button animated btn " tabindex="-1">                    
                                       View Collection                 
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <style> 
                              .slider_style_1 .slide-icon img { display:inline-block; }
                              .slider-1542369873506-list .slide-offer-text span { color:;font-weight:bold; }
                              @media screen and (min-width: 320px) and (max-width: 767px) {
                              .slider-1542369873506-list:before { position:absolute;top:0;content:"";float:left;width:100%;height:100%;background:rgba(255, 255, 255, 0.9);}
                              }
                           </style>
                        </div>
                        <div class="slick-list slider_style_1 slider-1543466931051-list slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 1263px; position: relative; left: -1263px; top: 0px; z-index: 998; opacity: 0; transition: opacity 500ms ease 0s;">
                          
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/slider_3_2000x.jpg?v=1549435483" class="slide-img medium-down--hide" alt="">

                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/responsive-slider-1_767x.jpg?v=1551863238" class="slide-img wide--hide post-large--hide large--hide" alt="">

                           <div class="slider-content slider-content-bg" style="">
                              <div class="container-bg">
                                 <div class="slide_left" style="">
                                    <h2 class="slide-heading animated " style=" 
                                       font-size: 60px;
                                       color:#69e0ff;
                                       ">
                                       Soft Spring Mattress                   
                                    </h2>
                                    <h5 class="slide-text animated " style="display:inline-block;
                                       font-size: 18px;
                                       color:#11423b;
                                       ">
                                       Lorem is a long established fact that a reader will be distracted by the readable content of a page .
                                    </h5>
                                    <div class="grid__item">
                                       <a href="" class="slide-button animated btn " tabindex="-1">                    
                                       View Collection                 
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <style> 
                              .slider_style_1 .slide-icon img { display:inline-block; }
                              .slider-1543466931051-list .slide-offer-text span { color:;font-weight:bold; }
                              @media screen and (min-width: 320px) and (max-width: 767px) {
                              .slider-1543466931051-list:before { position:absolute;top:0;content:"";float:left;width:100%;height:100%;background:rgba(255, 255, 255, 0.9);}
                              }
                           </style>
                        </div>
                        <div class="slick-list slider_style_2 slider-1550208888957-list slick-slide slick-current slick-active" data-slick-index="2" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 1263px; position: relative; left: -2526px; top: 0px; z-index: 999; opacity: 1;">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/slider_2_52ec6a20-87ce-417a-a55e-0bfe20ad52ae_2000x.jpg?v=1549433077" class="slide-img medium-down--hide" alt="">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/responsive-slider-2_767x.jpg?v=1551863247" class="slide-img wide--hide post-large--hide large--hide" alt="">
                           <div class="slider-content slider-content-bg" style="">
                              <div class="container-bg">
                                 <div class="slide_left" style="">
                                    <h2 class="slide-heading animated " style=" 
                                       font-size: 60px;
                                       color:#69e0ff;
                                       ">
                                       Decoration Cushion                   
                                    </h2>
                                    <h5 class="slide-text animated " style="display:inline-block;
                                       font-size: 18px;
                                       color:#11423b;
                                       ">
                                       Lorem is a long established fact that a reader will be distracted by the readable content of a page.
                                    </h5>
                                    <div class="grid__item">
                                       <a href="" class="slide-button animated btn " tabindex="0">                    
                                       Shop Now                 
                                       </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <style> 
                              .slider_style_2 .slide-icon img { float:right; }
                              .slider_style_2 .slide_left { float:right;text-align:right; }
                              .slider-1550208888957-list .slide-offer-text span { color:;font-weight:bold; }
                              @media screen and (min-width: 320px) and (max-width: 767px) {
                              .slider-1550208888957-list:before { position:absolute;top:0;content:"";float:left;width:100%;height:100%;background:rgba(255, 255, 255, 0.9);}
                              }
                           </style>
                        </div>
                     </div>
                  </div>
                  <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"></button>
                  <ul class="slick-dots" style="display: block;" role="tablist">
                     <li class="" aria-hidden="true" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00"><button type="button" data-role="none" role="button" tabindex="0">1</button></li>
                     <li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01" class=""><button type="button" data-role="none" role="button" tabindex="0">2</button></li>
                     <li aria-hidden="false" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02" class="slick-active"><button type="button" data-role="none" role="button" tabindex="0">3</button></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="dt-sc-hr-invisible-large"></div>
         <script>  
            $(document).on('ready', function() {
              $('.variable-width').slick({
                dots: true,
                slidesToScroll: 1,
                autoplay:true,
                fade: true,
                autoplaySpeed:5000,
                afterChange: function(slick, currentSlide){
                console.log(currentSlide);
              }
                                         });   
            })
         </script>
         <style>
            @media screen and (min-width: 320px) and (max-width: 767px) {}
            .slider-1542369873506-list .btn { background:#ffffff;color:#11423b; }
            .slider-1542369873506-list .btn:after { border-right: 70px solid transparent; border-bottom: 80px solid #69e0ff; }
            .slider-1542369873506-list .btn:hover { border-color: #69e0ff;background:#69e0ff;color:#11423b; }
            .slider-1543466931051-list .btn { background:#ffffff;color:#11423b; }
            .slider-1543466931051-list .btn:after { border-right: 70px solid transparent; border-bottom: 80px solid #69e0ff; }
            .slider-1543466931051-list .btn:hover { border-color: #69e0ff;background:#69e0ff;color:#11423b; }
            .slider-1550208888957-list .btn { background:#ffffff;color:#11423b; }
            .slider-1550208888957-list .btn:after { border-right: 70px solid transparent; border-bottom: 80px solid #69e0ff; }
            .slider-1550208888957-list .btn:hover { border-color: #69e0ff;background:#69e0ff;color:#11423b; }
         </style>
         <style>
            .home-slideshow  .slick-arrow:before{ color: #69e0ff; }  
         </style>
      </div>
      <div id="shopify-section-1546669459199" class="shopify-section index-section">
         <div data-section-id="1546669459199" data-section-type="content-block-type-1" class="content-block-type-1">
            <div class="container">
               <div class="section-header section-header--small">
                  <div class="border-title">
                     <h2 style="color:#11423b;">WHAT'S NEW</h2>
                     <div class="short-desc">
                        <p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                     </div>
                  </div>
               </div>
               <div class="dt-sc-hr-invisible-small"></div>
               <div class="grid__item" style="position:relative;">
                  <ul class="content_block grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="background:rgba(0,0,0,0)">
                     <li class="grid__item wow fadeInUp animated" data-wow-delay="ms" style="visibility: visible;-webkit-animation-delay: ms; -moz-animation-delay: ms; animation-delay: ms;">
                        <div class="content_section">
                           <div class="support_icon">                      
                              <a href="#" class="support_section">
                              <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-1-1.jpg?v=1549437904" alt="Velvet Cushion">
                              </a>
                           </div>
                           <div class="support_text">
                              <h5 style="color:#11423b;">Velvet Cushion</h5>
                              <p style="color:#676e6d;" class="desc">Coordinates for abs po oordinates for abs positioning the closest positioned Coordinates for abs positioning the closest positioned parent box of the positioned ting that ments.</p>
                           </div>
                        </div>
                     </li>
                     <li class="grid__item wow fadeInUp" data-wow-delay="ms" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: ms; -moz-animation-delay: ms; animation-delay: ms;">
                        <div class="content_section">
                           <div class="support_icon">                      
                              <a href="" class="support_section">
                              <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-3-1.jpg?v=1549438518" alt="Spring Mattress">
                              </a>
                           </div>
                           <div class="support_text">
                              <h5 style="color:#11423b;">Spring Mattress</h5>
                              <p style="color:#676e6d;" class="desc">Coordinates for abs po oordinates for abs positioning the closest positioned Coordinates for abs positioning the closest positioned parent box of the positioned ting that ments.</p>
                           </div>
                        </div>
                     </li>
                  </ul>
                  <div class="v_column_img grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item">
                     <a href="" class="support_section">
                     <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-5-1.jpg?v=1549437522" alt="">
                     </a>
                  </div>
                  <ul class="content_block content-block-2 grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="background:rgba(0,0,0,0)">
                     <li class="grid__item  wow fadeInUp animated" data-wow-delay="ms" style="visibility: visible;-webkit-animation-delay: ms; -moz-animation-delay: ms; animation-delay: ms;">
                        <div class="content_section">
                           <div class="support_icon">                      
                              <a href="" class="support_section">
                              <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-2-1_e22d615d-5945-422e-b52a-75e7768101ee.jpg?v=1549438506" alt="Single Soft Mattress">
                              </a>
                           </div>
                           <div class="support_text">
                              <h5 style="color:#11423b;">Single Soft Mattress</h5>
                              <p style="color:#676e6d;" class="desc">Coordinates for abs po oordinates for abs positioning the closest positioned Coordinates for abs positioning the closest positioned parent box of the positioned ting that ments.</p>
                           </div>
                        </div>
                     </li>
                     <li class="grid__item  wow fadeInUp" data-wow-delay="ms" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;-webkit-animation-delay: ms; -moz-animation-delay: ms; animation-delay: ms;">
                        <div class="content_section">
                           <div class="support_icon">                      
                              <a href="" class="support_section">
                              <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-4-1.jpg?v=1549438635" alt="Window Blanket">
                              </a>
                           </div>
                           <div class="support_text">
                              <h5 style="color:#11423b;">Window Blanket</h5>
                              <p style="color:#676e6d;" class="desc">Coordinates for abs po oordinates for abs positioning the closest positioned Coordinates for abs positioning the closest positioned parent box of the positioned ting that ments.</p>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="dt-sc-hr-invisible-large"></div>
         </div>
      </div>
      <div id="shopify-section-1546669586143" class="shopify-section index-section">
         <div data-section-id="1546669586143" data-section-type="product-tab-type-1" class="product-tab-type-1" style="float:left;width:100%;">
            <div class="container">
               <div class="full_width_tab">
                  <div class="grid-uniform">
                     <div class="grid__item">
                        <div class="dt-sc-tabs-container-section">
                           <div class="border-title text-center wow fadeInDown animated" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
                              <h2 style="color:#11423b;">YOU MAY BE LOOKING FOR</h2>
                              <div class="short-desc">
                                 <p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                              </div>
                              <div class="dt-sc-hr-invisible-small"></div>
                           </div>
                           <div class="dt-sc-tabs-container">
                              <ul class="dt-sc-tabs wow fadeInUp animated" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
                                 <li><a href="#" class="btn tabs-1546669586143-0 current">Mattress </a></li>
                                 <li><a href="#" class="btn tabs-1546670799435">Cushions </a></li>
                                 <li><a href="#" class="btn tabs-1546670815318">Curtains </a></li>
                              </ul>
                              <div class="dt-sc-tabs-content" style="display: block;">
                                 <ul class="tab2 1546669586143-0 owl-carousel owl-theme owl-loaded owl-drag">
                                    <div class="owl-stage-outer">
                                       <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1950px;">
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204003729472">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/best-seller/products/coir-foam-mattress" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p13_large.jpg?v=1550311707" alt="Coir Foam Mattress" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p13.jpg?v=1550311707">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p13.jpg?v=1550311707" alt="Coir Foam Mattress">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90.jpg?v=1550311708">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90.jpg?v=1550311708" alt="Coir Foam Mattress">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="coir-foam-mattress" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204003729472">                                    
                                                               <input type="hidden" name="id" value="25433112674368">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/best-seller/products/coir-foam-mattress" class="grid-link__title">Coir Foam Mattress</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$549.00">$549.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204003729472" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i></span><span class="spr-badge-caption">1 review</span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204001599552">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/best-seller/products/backrest-cushion" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p26_large.jpg?v=1550292849" alt="Backrest Cushion" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p26.jpg?v=1550292849">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p26.jpg?v=1550292849" alt="Backrest Cushion">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17.jpg?v=1550292850">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17.jpg?v=1550292850" alt="Backrest Cushion">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="backrest-cushion" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001599552">                                    
                                                               <input type="hidden" name="id" value="25424764534848">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/best-seller/products/backrest-cushion" class="grid-link__title">Backrest Cushion</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$499.00">$499.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204001599552" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204001075264">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <label class="deal-lable">Hurry, Only few item(s) left!</label>
                                                      <div class="deal-clock lof-clock-2204001075264-detail">
                                                         <ul class="list-inline">
                                                            <li class="day"><b>157</b><span>Days</span></li>
                                                            <li class="hours"><b>12</b><span>Hours</span></li>
                                                            <li class="mins"><b>12</b><span>Min</span></li>
                                                            <li class="seconds"><b>40</b><span>Sec</span></li>
                                                         </ul>
                                                      </div>
                                                      <style></style>
                                                      <a href="/collections/best-seller/products/standard-microfibre-pillow" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447_large.jpg?v=1550231484" alt="Standard Microfibre Pillow" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447.jpg?v=1550231484">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447.jpg?v=1550231484" alt="Standard Microfibre Pillow">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6.jpg?v=1550231484">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6.jpg?v=1550231484" alt="Standard Microfibre Pillow">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="standard-microfibre-pillow" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001075264">                                    
                                                               <input type="hidden" name="id" value="25393193484352">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/best-seller/products/standard-microfibre-pillow" class="grid-link__title">Standard Microfibre Pillow</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204001075264" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204000419904">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/best-seller/products/door-curtain-set" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63_large.jpg?v=1550302498" alt="Door Curtain Set" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498" alt="Door Curtain Set">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18.jpg?v=1550302500">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18.jpg?v=1550302500" alt="Door Curtain Set">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="door-curtain-set" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000419904">                                    
                                                               <input type="hidden" name="id" value="25428605665344">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/best-seller/products/door-curtain-set" class="grid-link__title">Door Curtain Set</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$219.00">$219.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204000419904" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2203997175872">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/best-seller/products/h151-headphones" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94_large.jpg?v=1550297939" alt="Cotton Canvas Cushion" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94.jpg?v=1550297939">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94.jpg?v=1550297939" alt="Cotton Canvas Cushion">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27.jpg?v=1550297939">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27.jpg?v=1550297939" alt="Cotton Canvas Cushion">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="h151-headphones" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203997175872">                                    
                                                               <input type="hidden" name="id" value="25426986664000">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/best-seller/products/h151-headphones" class="grid-link__title">Cotton Canvas Cushion</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2203997175872" data-rating="4.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption">1 review</span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="owl-dots disabled"></div>
                                 </ul>
                                 <div class="tab_2_nav 1546669586143-0 carousel-arrow">
                                    <div class="owl-prev disabled"><a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>
                                    <div class="owl-next"><a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                                 </div>
                              </div>
                              <script type="text/javascript">       
                                 $(document).ready(function(){
                                   var tab2 = $('.tab2.1546669586143-0');
                                                tab2.owlCarousel({
                                                loop:false,                      
                                                nav:true,
                                                navContainer: ".tab_2_nav.1546669586143-0",
                                                navText: ['<a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>','<a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a>'],
                                                dots: false,                  
                                                autoplayTimeout: 3000,
                                                responsive:{
                                                0:{
                                                items: 2
                                                },
                                                767:{
                                                items: 2
                                                },
                                                768:{
                                                items:3
                                                },
                                                1000:{
                                                items: 3
                                                },
                                                1200:{
                                                items: 3
                                                },
                                                1600:{
                                                items: 4
                                                }
                                                }
                                                });
                                 });
                                 
                              </script>
                              <div class="dt-sc-tabs-content" style="display: none;">
                                 <ul class="tab2 1546670799435 owl-carousel owl-theme owl-loaded owl-drag owl-hidden">
                                    <div class="owl-stage-outer">
                                       <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1950px;">
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204009070656">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/featured-product/products/plain-crush-curtains" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22_large.jpg?v=1550301918" alt="Plain Crush Curtains" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22.jpg?v=1550301918">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22.jpg?v=1550301918" alt="Plain Crush Curtains">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23.jpg?v=1550301919">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23.jpg?v=1550301919" alt="Plain Crush Curtains">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="plain-crush-curtains" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204009070656">                                    
                                                               <input type="hidden" name="id" value="25428327071808">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/featured-product/products/plain-crush-curtains" class="grid-link__title">Plain Crush Curtains</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204009070656" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row        on-sale" id="product-2204006842432">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <label class="deal-lable">Hurry, Only few item(s) left!</label>
                                                      <div class="deal-clock lof-clock-2204006842432-detail">
                                                         <ul class="list-inline">
                                                            <li class="day"><b>157</b><span>Days</span></li>
                                                            <li class="hours"><b>12</b><span>Hours</span></li>
                                                            <li class="mins"><b>12</b><span>Min</span></li>
                                                            <li class="seconds"><b>40</b><span>Sec</span></li>
                                                         </ul>
                                                      </div>
                                                      <style></style>
                                                      <a href="/collections/featured-product/products/comfort-cushion" class="grid-link">
                                                         <div class="featured-tag">
                                                            <span class="badge badge--sale">          
                                                            <span class="gift-tag badge__text">Sale</span>
                                                            </span>
                                                         </div>
                                                         <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_large.jpg?v=1550291740" alt="Comfort Cushion" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24.jpg?v=1550291740">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24.jpg?v=1550291740" alt="Comfort Cushion">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p16.jpg?v=1550291741">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p16.jpg?v=1550291741" alt="Comfort Cushion">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag   offer_exist ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="comfort-cushion" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006842432">                                    
                                                               <input type="hidden" name="id" value="25424129228864">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/featured-product/products/comfort-cushion" class="grid-link__title">Comfort Cushion</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$229.00">$229.00</span></span>
                                                            </div>
                                                         </div>
                                                         <del class="grid-link__sale_price" id="ComparePrice">
                                                         <span><span class="money" data-currency-usd="$300.00">$300.00</span></span></del>
                                                         <span class="spr-badge" id="spr_badge_2204006842432" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204002222144">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/featured-product/products/umang-pillow" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5_large.jpg?v=1550291435" alt="Umang Pillow" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5.jpg?v=1550291435">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5.jpg?v=1550291435" alt="Umang Pillow">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p8.jpg?v=1550291436">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p8.jpg?v=1550291436" alt="Umang Pillow">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="umang-pillow" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204002222144">                                    
                                                               <input type="hidden" name="id" value="25423989801024">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/featured-product/products/umang-pillow" class="grid-link__title">Umang Pillow</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$225.00">$225.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204002222144" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2203999436864">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/featured-product/products/on-ear-headphones-black" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_large.jpg?v=1550308200" alt="Spring Queen Mattress" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7.jpg?v=1550308200">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7.jpg?v=1550308200" alt="Spring Queen Mattress">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12.jpg?v=1550308200">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12.jpg?v=1550308200" alt="Spring Queen Mattress">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="on-ear-headphones-black" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203999436864">                                    
                                                               <input type="hidden" name="id" value="25430662250560">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/featured-product/products/on-ear-headphones-black" class="grid-link__title">Spring Queen Mattress</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$539.00">$539.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2203999436864" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row        on-sale" id="product-2203998322752">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/featured-product/products/hand-made-cushion" class="grid-link">
                                                         <div class="featured-tag">
                                                            <span class="badge badge--sale">          
                                                            <span class="gift-tag badge__text">Sale</span>
                                                            </span>
                                                         </div>
                                                         <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8_large.jpg?v=1550298962" alt="Hand Made Cushion" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8.jpg?v=1550298962">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8.jpg?v=1550298962" alt="Hand Made Cushion">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6_04d3f889-b5a1-4910-95fd-b0d401368dae.jpg?v=1550298963">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6_04d3f889-b5a1-4910-95fd-b0d401368dae.jpg?v=1550298963" alt="Hand Made Cushion">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag   offer_exist ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="hand-made-cushion" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203998322752">                                    
                                                               <input type="hidden" name="id" value="25427285639232">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/featured-product/products/hand-made-cushion" class="grid-link__title">Hand Made Cushion</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <del class="grid-link__sale_price" id="ComparePrice">
                                                         <span><span class="money" data-currency-usd="$500.00">$500.00</span></span></del>
                                                         <span class="spr-badge" id="spr_badge_2203998322752" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="owl-dots disabled"></div>
                                 </ul>
                                 <div class="tab_2_nav 1546670799435 carousel-arrow">
                                    <div class="owl-prev disabled"><a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>
                                    <div class="owl-next"><a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                                 </div>
                              </div>
                              <script type="text/javascript">       
                                 $(document).ready(function(){
                                   var tab2 = $('.tab2.1546670799435');
                                                tab2.owlCarousel({
                                                loop:false,                      
                                                nav:true,
                                                navContainer: ".tab_2_nav.1546670799435",
                                                navText: ['<a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>','<a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a>'],
                                                dots: false,                  
                                                autoplayTimeout: 3000,
                                                responsive:{
                                                0:{
                                                items: 2
                                                },
                                                767:{
                                                items: 2
                                                },
                                                768:{
                                                items:3
                                                },
                                                1000:{
                                                items: 3
                                                },
                                                1200:{
                                                items: 3
                                                },
                                                1600:{
                                                items: 4
                                                }
                                                }
                                                });
                                 });
                                 
                              </script>
                              <div class="dt-sc-tabs-content" style="display: none;">
                                 <ul class="tab2 1546670815318 owl-carousel owl-theme owl-loaded owl-drag owl-hidden">
                                    <div class="owl-stage-outer">
                                       <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 3510px;">
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204006449216">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/plain-blanket" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p4_large.jpg?v=1550307794" alt="Plain Blanket" class="featured-image">
                                                      </a>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="plain-blanket" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006449216">                                    
                                                               <input type="hidden" name="id" value="25430470262848">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/plain-blanket" class="grid-link__title">Plain Blanket</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$386.00">$386.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204006449216" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204005466176">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/eyelet-polyster-curtain" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p19_large.jpg?v=1550303828" alt="Eyelet Polyster Curtain" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p19.jpg?v=1550303828">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p19.jpg?v=1550303828" alt="Eyelet Polyster Curtain">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21.jpg?v=1550303829">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21.jpg?v=1550303829" alt="Eyelet Polyster Curtain">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="eyelet-polyster-curtain" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204005466176">                                    
                                                               <input type="hidden" name="id" value="25428997734464">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/eyelet-polyster-curtain" class="grid-link__title">Eyelet Polyster Curtain</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$225.00">$225.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204005466176" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204005007424">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/micro-fibre-cushion" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993_large.jpg?v=1550292544" alt="Micro Fibre Cushion" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993.jpg?v=1550292544">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993.jpg?v=1550292544" alt="Micro Fibre Cushion">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_ef4f28be-6c20-4c97-b1ca-4b7387ff3ceb.jpg?v=1550292544">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_ef4f28be-6c20-4c97-b1ca-4b7387ff3ceb.jpg?v=1550292544" alt="Micro Fibre Cushion">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="micro-fibre-cushion" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204005007424">                                    
                                                               <input type="hidden" name="id" value="25424443768896">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/micro-fibre-cushion" class="grid-link__title">Micro Fibre Cushion</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204005007424" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row        on-sale" id="product-2204003237952">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/soft-cotton-mattress" class="grid-link">
                                                         <div class="featured-tag">
                                                            <span class="badge badge--sale">          
                                                            <span class="gift-tag badge__text">Sale</span>
                                                            </span>
                                                         </div>
                                                         <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p10_69ba0778-b027-4b92-8939-f8d7b16b8fb1_large.jpg?v=1550309620" alt="Soft Cotton Mattress" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p10_69ba0778-b027-4b92-8939-f8d7b16b8fb1.jpg?v=1550309620">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p10_69ba0778-b027-4b92-8939-f8d7b16b8fb1.jpg?v=1550309620" alt="Soft Cotton Mattress">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p9.jpg?v=1550309621">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p9.jpg?v=1550309621" alt="Soft Cotton Mattress">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag   offer_exist ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="soft-cotton-mattress" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204003237952">                                    
                                                               <input type="hidden" name="id" value="25431264493632">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/soft-cotton-mattress" class="grid-link__title">Soft Cotton Mattress</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <del class="grid-link__sale_price" id="ComparePrice">
                                                         <span><span class="money" data-currency-usd="$400.00">$400.00</span></span></del>
                                                         <span class="spr-badge" id="spr_badge_2204003237952" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item active" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204002222144">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/umang-pillow" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5_large.jpg?v=1550291435" alt="Umang Pillow" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5.jpg?v=1550291435">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5.jpg?v=1550291435" alt="Umang Pillow">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p8.jpg?v=1550291436">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p8.jpg?v=1550291436" alt="Umang Pillow">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="umang-pillow" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204002222144">                                    
                                                               <input type="hidden" name="id" value="25423989801024">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/umang-pillow" class="grid-link__title">Umang Pillow</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$225.00">$225.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204002222144" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204001075264">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <label class="deal-lable">Hurry, Only few item(s) left!</label>
                                                      <div class="deal-clock lof-clock-2204001075264-detail">
                                                         <ul class="list-inline">
                                                            <li class="day"><b>157</b><span>Days</span></li>
                                                            <li class="hours"><b>12</b><span>Hours</span></li>
                                                            <li class="mins"><b>12</b><span>Min</span></li>
                                                            <li class="seconds"><b>40</b><span>Sec</span></li>
                                                         </ul>
                                                      </div>
                                                      <style></style>
                                                      <a href="/collections/new-offers/products/standard-microfibre-pillow" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447_large.jpg?v=1550231484" alt="Standard Microfibre Pillow" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447.jpg?v=1550231484">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447.jpg?v=1550231484" alt="Standard Microfibre Pillow">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6.jpg?v=1550231484">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6.jpg?v=1550231484" alt="Standard Microfibre Pillow">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="standard-microfibre-pillow" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001075264">                                    
                                                               <input type="hidden" name="id" value="25393193484352">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/standard-microfibre-pillow" class="grid-link__title">Standard Microfibre Pillow</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204001075264" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204000845888">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/polyester-window-curtain" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075_large.jpg?v=1550304855" alt="Polyester Window Curtain" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075.jpg?v=1550304855">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075.jpg?v=1550304855" alt="Polyester Window Curtain">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d.jpg?v=1550304855">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d.jpg?v=1550304855" alt="Polyester Window Curtain">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="polyester-window-curtain" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000845888">                                    
                                                               <input type="hidden" name="id" value="25429378760768">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/polyester-window-curtain" class="grid-link__title">Polyester Window Curtain</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$328.00">$328.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204000845888" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row       " id="product-2204000419904">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/door-curtain-set" class="grid-link">            
                                                      <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63_large.jpg?v=1550302498" alt="Door Curtain Set" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498" alt="Door Curtain Set">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18.jpg?v=1550302500">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18.jpg?v=1550302500" alt="Door Curtain Set">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag  ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="door-curtain-set" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000419904">                                    
                                                               <input type="hidden" name="id" value="25428605665344">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/door-curtain-set" class="grid-link__title">Door Curtain Set</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$219.00">$219.00</span></span>
                                                            </div>
                                                         </div>
                                                         <span class="spr-badge" id="spr_badge_2204000419904" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                          <div class="owl-item" style="width: 390px;">
                                             <li class="grid__item swiper-slide item-row        on-sale" id="product-2203998945344">
                                                <div class="products product-hover-15">
                                                   <div class="product-container ">
                                                      <a href="/collections/new-offers/products/memory-foam-mattress-1" class="grid-link">
                                                         <div class="featured-tag">
                                                            <span class="badge badge--sale">          
                                                            <span class="gift-tag badge__text">Sale</span>
                                                            </span>
                                                         </div>
                                                         <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d_large.jpg?v=1550312267" alt="Memory Foam Mattress" class="featured-image">
                                                      </a>
                                                      <div class="lSSlideOuter  vertical">
                                                         <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                                            <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                                               <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d.jpg?v=1550312267">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d.jpg?v=1550312267" alt="Memory Foam Mattress">
                                                                  </a>
                                                               </li>
                                                               <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                                  <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e.jpg?v=1550312268">
                                                                  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e.jpg?v=1550312268" alt="Memory Foam Mattress">
                                                                  </a>
                                                               </li>
                                                            </ul>
                                                            <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                                         </div>
                                                      </div>
                                                      <div class="product_right_tag   offer_exist ">
                                                      </div>
                                                      <div class="ImageWrapper">
                                                         <div class="product-button">
                                                            <a href="javascript:void(0)" id="memory-foam-mattress-1" class="quick-view-text" title="Quick View">                      
                                                            <b class="far fa-eye"></b>
                                                            <i>   Quick View</i>
                                                            </a>       
                                                            <form action="/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203998945344">                                    
                                                               <input type="hidden" name="id" value="25433818038336">  
                                                               <a class="add-cart-btn" title="Add to Cart">
                                                               <b class="fas fa-shopping-cart"></b>
                                                               <i>  Add to Cart</i>
                                                               </a>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div class="product-detail">
                                                      <a href="/collections/new-offers/products/memory-foam-mattress-1" class="grid-link__title">Memory Foam Mattress</a> 
                                                      <div class="grid-link__meta">
                                                         <div class="product_price">
                                                            <div class="grid-link__org_price" id="ProductPrice">
                                                               <span> <span class="money" data-currency-usd="$329.00">$329.00</span></span>
                                                            </div>
                                                         </div>
                                                         <del class="grid-link__sale_price" id="ComparePrice">
                                                         <span><span class="money" data-currency-usd="$400.00">$400.00</span></span></del>
                                                         <span class="spr-badge" id="spr_badge_2203998945344" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                                         </span>
                                                      </div>
                                                      <ul class="item-swatch color_swatch_Value">  
                                                      </ul>
                                                   </div>
                                                </div>
                                                <style>
                                                   .product-container.product-hover-15 {position: relative;}
                                                   .lSSlideOuter.vertical {
                                                   float: right;
                                                   width: 20%;
                                                   position: absolute;
                                                   top: 20px;
                                                   right: -100%;
                                                   }
                                                   .thumbs_items a {display:block;}
                                                </style>
                                             </li>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="owl-dots disabled"></div>
                                 </ul>
                                 <div class="tab_2_nav 1546670815318 carousel-arrow">
                                    <div class="owl-prev disabled"><a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>
                                    <div class="owl-next"><a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                                 </div>
                              </div>
                              <script type="text/javascript">       
                                 $(document).ready(function(){
                                   var tab2 = $('.tab2.1546670815318');
                                                tab2.owlCarousel({
                                                loop:false,                      
                                                nav:true,
                                                navContainer: ".tab_2_nav.1546670815318",
                                                navText: ['<a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>','<a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a>'],
                                                dots: false,                  
                                                autoplayTimeout: 3000,
                                                responsive:{
                                                0:{
                                                items: 2
                                                },
                                                767:{
                                                items: 2
                                                },
                                                768:{
                                                items:3
                                                },
                                                1000:{
                                                items: 3
                                                },
                                                1200:{
                                                items: 3
                                                },
                                                1600:{
                                                items: 4
                                                }
                                                }
                                                });
                                 });
                                 
                              </script>
                           </div>
                           <!--End tabs container-->
                        </div>
                     </div>
                  </div>
                  <style>
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a { color:#11423b;background:none; }
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a.tabs-1546669586143-0:hover,.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current { color:#69e0ff; }
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current::after {background:#69e0ff;}
                  </style>
                  <style>
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a { color:#11423b;background:none; }
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a.tabs-1546670799435:hover,.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current { color:#69e0ff; }
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current::after {background:#69e0ff;}
                  </style>
                  <style>
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a { color:#11423b;background:none; }
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a.tabs-1546670815318:hover,.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current { color:#69e0ff; }
                     .product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current::after {background:#69e0ff;}
                  </style>
               </div>
            </div>
         </div>
         <div class="dt-sc-hr-invisible-large"></div>
      </div>
      <div id="shopify-section-1546671026627" class="shopify-section index-section">
         <div data-section-id="1546671026627" data-section-type="video-content-block-1" class="video-content-block-1">
            <div class="grid-uniform" style="background-image:url(//cdn.shopify.com/s/files/1/0167/7249/7472/files/bg_3bd2010f-5200-42e8-8cb0-eacc4804654f.jpg?v=1549431984);clear: both;background-size: cover;background-repeat: no-repeat;padding:100px 0;">
               <div class="container">
                  <div class="video-banner-type-1-block">
                     <div class="intro-video grid__item wide--two-fifths post-large--two-fifths large--two-fifths">
                        <div class="intro-video-text p-video">    
                           <a href="https://www.youtube.com/watch?v=O_IJ5-YUli0" class="jas-popup-url video-player" data-effect="mfp-move-from-top">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-6_64cfe8d9-e4f6-4df2-8262-ab881f1b7f24.jpg?v=1550578667" alt="Feel the softness...">  
                           </a>   
                        </div>
                     </div>
                     <div class="video-banner-type-1-content  grid__item wide--three-fifths post-large--three-fifths large--three-fifths">
                        <span style="color:#ffffff">Feel the softness...</span>
                        <h2 style="color:#69e0ff">what type of mattress to buy?</h2>
                        <p style="color:#ffffff">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia con sequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quis quam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                        <a class="btn" href="/collections/all">Shop Now</a>    
                     </div>
                  </div>
               </div>
               <style>
                  .video-banner-type-1-content a.btn { color:#11423b;background-color:#ffffff; }
                  .video-banner-type-1-content a.btn:hover { color:#11423b;background-color:#69e0ff; }
               </style>
            </div>
            <div class="dt-sc-hr-invisible-large"></div>
         </div>
      </div>
      <div id="shopify-section-1546669814766" class="shopify-section index-section">
         <div data-section-id="1546669814766" data-section-type="product-tab-type-9-sorting" class="product-tab-type-9-sorting">
            <div class="full_width_tab">
               <div class="grid-uniform">
                  <div class="grid__item">
                     <div class="dt-sc-tabs-container-section filter-grid-type-3">
                        <div class="container">
                           <div class="section-header section-header--small">
                              <div class="border-title">
                                 <h2 style="color:#11423b;">Product gallery</h2>
                                 <div class="short-desc">
                                    <p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                                 </div>
                              </div>
                           </div>
                           <div class="dt-sc-hr-invisible-small"></div>
                        </div>
                        <div class="dt-sc-tabs-container">
                           <ul class="dt-sc-tabs wow fadeInUp animated" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
                              <li><a href="#" class="btn tabs-1546669814766-0 current">Spring </a></li>
                              <li><a href="#" class="btn tabs-1546669824885">Foam Top </a></li>
                              <li><a href="#" class="btn tabs-1546669828675">Orthopedic </a></li>
                              <li><a href="#" class="btn tabs-1546669831118">Rollable </a></li>
                           </ul>
                           <div class="dt-sc-tabs-content portfolio-container" style="padding: 0px; background: rgb(255, 255, 255); display: block;">
                              <div class="grid-uniform list-collection-products portfolio-container">
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_large.jpg?v=1549353303" alt="Gallery Images">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/gallery-images" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_1024x1024.jpg?v=1549353303" class="portfolio_zoom" rel="prettyPhoto[gallery1]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/gallery-images">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_large.jpg?v=1550580759" alt="Sale Off">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sale-off" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_1024x1024.jpg?v=1550580759" class="portfolio_zoom" rel="prettyPhoto[gallery2]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sale-off">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-5_large.jpg?v=1549353828" alt="Featured Product">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/featured-product" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-5_1024x1024.jpg?v=1549353828" class="portfolio_zoom" rel="prettyPhoto[gallery3]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/featured-product">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-10_large.jpg?v=1549353861" alt="Sold Product">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sold-product" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-10_1024x1024.jpg?v=1549353861" class="portfolio_zoom" rel="prettyPhoto[gallery4]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sold-product">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="dt-sc-tabs-content portfolio-container" style="padding: 0px; background: rgb(255, 255, 255); display: none;">
                              <div class="grid-uniform list-collection-products portfolio-container">
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/gal-1_960x_df184099-df49-4d99-b1e5-b42ed565e2b9_large.jpg?v=1550575513" alt="Best Seller">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/best-seller" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/gal-1_960x_df184099-df49-4d99-b1e5-b42ed565e2b9_1024x1024.jpg?v=1550575513" class="portfolio_zoom" rel="prettyPhoto[gallery1]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/best-seller">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-10_large.jpg?v=1549353861" alt="Sold Product">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sold-product" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-10_1024x1024.jpg?v=1549353861" class="portfolio_zoom" rel="prettyPhoto[gallery2]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sold-product">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_large.jpg?v=1549353303" alt="Gallery Images">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/gallery-images" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_1024x1024.jpg?v=1549353303" class="portfolio_zoom" rel="prettyPhoto[gallery3]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/gallery-images">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_large.jpg?v=1550580759" alt="Sale Off">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sale-off" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_1024x1024.jpg?v=1550580759" class="portfolio_zoom" rel="prettyPhoto[gallery4]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sale-off">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="dt-sc-tabs-content portfolio-container" style="padding: 0px; background: rgb(255, 255, 255); display: none;">
                              <div class="grid-uniform list-collection-products portfolio-container">
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-10_large.jpg?v=1549353861" alt="Sold Product">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sold-product" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-10_1024x1024.jpg?v=1549353861" class="portfolio_zoom" rel="prettyPhoto[gallery1]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sold-product">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-5_large.jpg?v=1549353828" alt="Featured Product">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/featured-product" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-5_1024x1024.jpg?v=1549353828" class="portfolio_zoom" rel="prettyPhoto[gallery2]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/featured-product">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_large.jpg?v=1549353303" alt="Gallery Images">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/gallery-images" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_1024x1024.jpg?v=1549353303" class="portfolio_zoom" rel="prettyPhoto[gallery3]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/gallery-images">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_large.jpg?v=1550580759" alt="Sale Off">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sale-off" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_1024x1024.jpg?v=1550580759" class="portfolio_zoom" rel="prettyPhoto[gallery4]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sale-off">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="dt-sc-tabs-content portfolio-container" style="padding: 0px; background: rgb(255, 255, 255); display: none;">
                              <div class="grid-uniform list-collection-products portfolio-container">
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/bannerbtm4_large.png?v=1550575495" alt="New Arrival">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/new-arrival" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/bannerbtm4_1024x1024.png?v=1550575495" class="portfolio_zoom" rel="prettyPhoto[gallery1]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/new-arrival">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/gal-1_960x_df184099-df49-4d99-b1e5-b42ed565e2b9_large.jpg?v=1550575513" alt="Best Seller">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/best-seller" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/gal-1_960x_df184099-df49-4d99-b1e5-b42ed565e2b9_1024x1024.jpg?v=1550575513" class="portfolio_zoom" rel="prettyPhoto[gallery2]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/best-seller">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_large.jpg?v=1550580759" alt="Sale Off">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/sale-off" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/blog-6_1024x1024.jpg?v=1550580759" class="portfolio_zoom" rel="prettyPhoto[gallery3]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/sale-off">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                                 <div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_large.jpg?v=1549353303" alt="Gallery Images">        
                                    <div class="image-overlay">
                                       <div class="links">
                                          <a href="/collections/gallery-images" class="link"><i class="fas fa-link icons"></i></a>
                                          <a href="//cdn.shopify.com/s/files/1/0167/7249/7472/collections/img-7_1024x1024.jpg?v=1549353303" class="portfolio_zoom" rel="prettyPhoto[gallery4]"><i class="fas fa-search icons"></i></a>
                                       </div>
                                    </div>
                                    <div class="image-overlay-details">
                                       <h4><a href="/collections/gallery-images">SUSPENDISSE TEMPOR IACULIS LEO</a></h4>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--End tabs container-->
                     </div>
                  </div>
               </div>
               <style>
                  .product-tab-type-9-sorting .dt-sc-tabs-frame-content, .dt-sc-tabs-content {border:none; }
                  .filter-grid-type-3 .portfolio-container .grid__item.gallery:hover .image-overlay {    opacity: 1;}
                  .filter-grid-type-3 .grid__item .image-overlay .links a { border:1px solid #ffffff;color:#ffffff; }
                  .filter-grid-type-3 .grid__item .image-overlay .links a:hover { background:#ffffff;color:#11423b; }
                  .filter-grid-type-3 .portfolio-container .image-overlay-details h4 a { color:#11423b; }
                  .filter-grid-type-3 .portfolio-container .image-overlay-details h4 a:hover { color:#000; }
                  .filter-grid-type-3 .portfolio-container .image-overlay-details p { color:#ffffff; }
                  .filter-grid-type-3 .grid__item .image-overlay {  background: rgba(105, 224, 255, 0.8);  }
                  .filter-grid-type-3 .portfolio-container .gallery:hover .image-overlay-details { }
               </style>
            </div>
         </div>
         <div class="dt-sc-hr-invisible-large"></div>
      </div>
      <div id="shopify-section-1546671143172" class="shopify-section index-section">
         <div data-section-id="1546671143172" data-section-type="grid-banner-type-8" class="grid-banner-type-8">
            <div class="container">
               <div class="section-header section-header--small">
                  <div class="border-title">
                     <h2 style="color:#11423b;">Latest Collection</h2>
                     <div class="short-desc">
                        <p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                     </div>
                  </div>
               </div>
               <div class="dt-sc-hr-invisible-small"></div>
               <div class="grid-uniform">
                  <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item loop-1">
                     <div class="img-hover-effect">
                        <div class="overlay">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-12.jpg?v=1549361990" alt="Stylish">
                           <a class="overlay-link" href="/"></a>
                        </div>
                        <div class="block-content">
                           <h6 style="color:#11423b">Stylish</h6>
                           <h4 style="color:#11423b">Bedspread</h4>
                           <p style="color:#11423b;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                           <a href="/" class="btn">View all collection <i class="fas fa-arrow-right"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item loop-2">
                     <div class="img-hover-effect">
                        <div class="overlay">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-11.jpg?v=1549362002" alt="Sofa">
                           <a class="overlay-link" href="/"></a>
                        </div>
                        <div class="block-content">
                           <h6 style="color:#11423b">Sofa</h6>
                           <h4 style="color:#11423b">Cushion</h4>
                           <p style="color:#11423b;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                           <a href="/" class="btn">View all collection <i class="fas fa-arrow-right"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item loop-3">
                     <div class="img-hover-effect">
                        <div class="overlay">
                           <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-13.jpg?v=1549362021" alt="Bed">
                           <a class="overlay-link" href="/"></a>
                        </div>
                        <div class="block-content">
                           <h6 style="color:#11423b">Bed</h6>
                           <h4 style="color:#11423b">Pillows</h4>
                           <p style="color:#11423b;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                           <a href="/" class="btn">View all collection <i class="fas fa-arrow-right"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
               <style>
                  .grid-banner-type-8 .block-content { background:#f4f9f9; }
                  .grid-banner-type-8 .loop-2 .block-content { background:#69e0ff; }
                  .grid-banner-type-8 .loop-2 .block-content .btn { background:#69e0ff;color:#11423b;  }
                  .grid-banner-type-8 .loop-2 .block-content .btn:hover { background:#ffffff; color:#11423b; }
                  .grid-banner-type-8 .block-content .btn { background:#e7eeef;color:#11423b; }
                  .grid-banner-type-8 .block-content .btn:hover { background:#69e0ff;color:#11423b; }
                  Liquid error (sections/grid-banner-type-8.liquid line 68): Could not find asset snippets/hex2rgb.liquid
                  .grid-banner-type-8 .img-hover-effect .overlay .overlay-link {  background: rgba(17, 66, 59, 0.8); }
               </style>
               <div class="dt-sc-hr-invisible-large"></div>
            </div>
         </div>
      </div>
      <div id="shopify-section-1546671336518" class="shopify-section index-section">
         <div data-section-id="1546671336518" data-section-type="vertical-product-grid-type-2" class="vertical-product-grid-type-2">
            <div class="vertical-product-grid" style="float:left;width:100%;background-color:#e7eeef;">
               <div class="dt-sc-hr-invisible-large"></div>
               <div class="container-bg">
                  <div class="section-header section-header--small">
                     <div class="border-title">
                        <h2 class="section-header__title" style="color:#11423b;">    
                           New arrivals
                        </h2>
                        <div class="short-desc">
                           <p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                     </div>
                  </div>
                  <div class="dt-sc-hr-invisible-small"></div>
                  <div class="grid-uniform">
                     <ul class="v_grid-type-1__items">
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204000845888">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/polyester-window-curtain" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075_large.jpg?v=1550304855" class="featured-image" alt="Polyester Window Curtain">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d_large.jpg?v=1550304855" class="hidden-feature_img" alt="Polyester Window Curtain">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/polyester-window-curtain" class="grid-link__title">Polyester Window Curtain</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Sennheiser</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$328.00">$328.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204000845888">                                    
                                       <input type="hidden" name="id" value="25429378760768">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item on-sale" id="product-2203998945344">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/memory-foam-mattress-1" class="grid-link">
                                    <div class="featured-tag">
                                       <span class="badge badge--sale">
                                       <span class="gift-tag badge__text">Sale</span>
                                       </span>
                                    </div>
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d_large.jpg?v=1550312267" class="featured-image" alt="Memory Foam Mattress">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e_large.jpg?v=1550312268" class="hidden-feature_img" alt="Memory Foam Mattress">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/memory-foam-mattress-1" class="grid-link__title">Memory Foam Mattress</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Skullcandy</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$329.00">$329.00</span>
                                          </div>
                                          <del class="grid-link__sale_price"><span class="money" data-currency-usd="$400.00">$400.00</span></del>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2203998945344">                                    
                                       <input type="hidden" name="id" value="25433818038336">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204003729472">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/coir-foam-mattress" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p13_large.jpg?v=1550311707" class="featured-image" alt="Coir Foam Mattress">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90_large.jpg?v=1550311708" class="hidden-feature_img" alt="Coir Foam Mattress">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/coir-foam-mattress" class="grid-link__title">Coir Foam Mattress</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Bluedio</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$549.00">$549.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204003729472">                                    
                                       <input type="hidden" name="id" value="25433112674368">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item on-sale" id="product-2204004417600">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/single-foam-mattress" class="grid-link">
                                    <div class="featured-tag">
                                       <span class="badge badge--sale">
                                       <span class="gift-tag badge__text">Sale</span>
                                       </span>
                                    </div>
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758_large.jpg?v=1550310807" class="featured-image" alt="Single Foam Mattress">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_large.jpg?v=1550310807" class="hidden-feature_img" alt="Single Foam Mattress">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/single-foam-mattress" class="grid-link__title">Single Foam Mattress</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Bluedio</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$139.00">$139.00</span>
                                          </div>
                                          <del class="grid-link__sale_price"><span class="money" data-currency-usd="$500.00">$500.00</span></del>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204004417600">                                    
                                       <input type="hidden" name="id" value="25432331812928">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204002680896">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/cooling-foam-mattress" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37f_large.jpg?v=1550309434" class="featured-image" alt="Cooling Foam Mattress">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p10_large.jpg?v=1550309434" class="hidden-feature_img" alt="Cooling Foam Mattress">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/cooling-foam-mattress" class="grid-link__title">Cooling Foam Mattress</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Captcha</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$738.00">$738.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204002680896">                                    
                                       <input type="hidden" name="id" value="25431018897472">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2203999436864">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/on-ear-headphones-black" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_large.jpg?v=1550308200" class="featured-image" alt="Spring Queen Mattress">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12_large.jpg?v=1550308200" class="hidden-feature_img" alt="Spring Queen Mattress">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/on-ear-headphones-black" class="grid-link__title">Spring Queen Mattress</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Captcha</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$539.00">$539.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2203999436864">                                    
                                       <input type="hidden" name="id" value="25430662250560">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204006449216">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/plain-blanket" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p4_large.jpg?v=1550307794" class="featured-image" alt="Plain Blanket">
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/plain-blanket" class="grid-link__title">Plain Blanket</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Bluedio</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$386.00">$386.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204006449216">                                    
                                       <input type="hidden" name="id" value="25430470262848">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204005466176">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/eyelet-polyster-curtain" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p19_large.jpg?v=1550303828" class="featured-image" alt="Eyelet Polyster Curtain">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_large.jpg?v=1550303829" class="hidden-feature_img" alt="Eyelet Polyster Curtain">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/eyelet-polyster-curtain" class="grid-link__title">Eyelet Polyster Curtain</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Sennheiser</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$225.00">$225.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204005466176">                                    
                                       <input type="hidden" name="id" value="25428997734464">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                        <li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204000419904">
                           <div class="products">
                              <div class="product-container">
                                 <a href="/collections/new-arrival/products/door-curtain-set" class="grid-link">
                                    <div class="ImageOverlayCa"></div>
                                    <div class="reveal"> 
                                       <span class="product-additional">      
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63_large.jpg?v=1550302498" class="featured-image" alt="Door Curtain Set">
                                       </span>
                                       <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18_large.jpg?v=1550302500" class="hidden-feature_img" alt="Door Curtain Set">
                                    </div>
                                 </a>
                              </div>
                              <div class="product-detail">
                                 <div class="product_left">
                                    <a href="/collections/new-arrival/products/door-curtain-set" class="grid-link__title">Door Curtain Set</a>     
                                    <p class="product-vendor">
                                       <label>Brand :</label>
                                       <span>Bluedio</span>
                                    </p>
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="$219.00">$219.00</span>
                                          </div>
                                       </div>
                                    </div>
                                    <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204000419904">                                    
                                       <input type="hidden" name="id" value="25428605665344">  
                                       <a title="Add to Cart" class="add-cart-btn">
                                       <i class="fas fa-shopping-cart"></i>
                                       </a>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </li>
                     </ul>
                     <div class="nav_v_grid-type-1__items">
                        <a class="prev active"></a>
                        <a class="next"></a>  
                     </div>
                  </div>
               </div>
               <style>
                  .vertical-product-grid-type-2 ul li .products { background:#ffffff; }
               </style>
               <div class="dt-sc-hr-invisible-medium"></div>
            </div>
            <div class="dt-sc-hr-invisible-large"></div>
         </div>
      </div>
      <div id="shopify-section-1546671399231" class="shopify-section">
         <div data-section-id="1546671399231" data-section-type="blog-post-type-3" class="blog-post-type-3">
            <div class="grid">
               <div class="blog-post">
                  <div class="container">
                     <div class="section-header section-header--small">
                        <div class="border-title">
                           <h2>From our blog</h2>
                           <div class="short-desc">
                              <p style="color:#676e6d;">    
                                 Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="dt-sc-hr-invisible-small"></div>
                  </div>
                  <div class="home-blog-type-3 blog-section">
                     <div class="article-item article-item-1 grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="position:relative;background:url('//cdn.shopify.com/s/files/1/0167/7249/7472/articles/blog-12.jpg?v=1550228772');min-height:550px;background-size:cover;background-position:center;">
                        <div class="blog-overlay"></div>
                        <div class="article">
                           <div class="blog-description grid__item">
                              <div class="blogs-sub-title">
                                 <p class="blog-date" style="color:#ffffff;">
                                    <time datetime="2019-02-14"><span class="date"><i style="color:#ffffff">14 </i> / Feb </span></time>         
                                 </p>
                                 <p class="author" style="color:#ffffff">
                                    <i class="zmdi zmdi-account"></i>
                                    <span> Ram M</span>
                                 </p>
                              </div>
                              <div class="home-blog-content blog-detail">
                                 <h4><a href="/blogs/news/pastel-colored-mattress" style="">Pastel colored mattress</a></h4>
                                 <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ...</p>
                                 <div class="blog-btn">
                                    <a href="/blogs/news/pastel-colored-mattress">Read more <i class="fas fa-arrow-right"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="article-item article-item-2 grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="position:relative;background:url('//cdn.shopify.com/s/files/1/0167/7249/7472/articles/blog-11.jpg?v=1550228640');min-height:550px;background-size:cover;background-position:center;">
                        <div class="blog-overlay"></div>
                        <div class="article">
                           <div class="blog-description grid__item">
                              <div class="blogs-sub-title">
                                 <p class="blog-date" style="color:#ffffff;">
                                    <time datetime="2019-02-14"><span class="date"><i style="color:#ffffff">14 </i> / Feb </span></time>         
                                 </p>
                                 <p class="author" style="color:#ffffff">
                                    <i class="zmdi zmdi-account"></i>
                                    <span> Ram M</span>
                                 </p>
                              </div>
                              <div class="home-blog-content blog-detail">
                                 <h4><a href="/blogs/news/modern-interior-mattress" style="">Modern interior mattress</a></h4>
                                 <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ...</p>
                                 <div class="blog-btn">
                                    <a href="/blogs/news/modern-interior-mattress">Read more <i class="fas fa-arrow-right"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="article-item article-item-3 grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="position:relative;background:url('//cdn.shopify.com/s/files/1/0167/7249/7472/articles/blog-10.jpg?v=1550228452');min-height:550px;background-size:cover;background-position:center;">
                        <div class="blog-overlay"></div>
                        <div class="article">
                           <div class="blog-description grid__item">
                              <div class="blogs-sub-title">
                                 <p class="blog-date" style="color:#ffffff;">
                                    <time datetime="2019-02-14"><span class="date"><i style="color:#ffffff">14 </i> / Feb </span></time>         
                                 </p>
                                 <p class="author" style="color:#ffffff">
                                    <i class="zmdi zmdi-account"></i>
                                    <span> Ram M</span>
                                 </p>
                              </div>
                              <div class="home-blog-content blog-detail">
                                 <h4><a href="/blogs/news/sleeping-on-maternity-pillow" style="">Sleeping on maternity pillow</a></h4>
                                 <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ...</p>
                                 <div class="blog-btn">
                                    <a href="/blogs/news/sleeping-on-maternity-pillow">Read more <i class="fas fa-arrow-right"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <style>
                  .blog-post-type-3 .border-title h2 {  color:#11423b; }
                  .blog-post-type-3 .home-blog-content.blog-detail h4 a { color:#69e0ff; }
                  .blog-post-type-3 .home-blog-content.blog-detail h4 a:hover { color:#ffffff; }
                  .blog-post-type-3 .article-item .blog-overlay { background:#000; }
                  .blog-post-type-3 .article-item.article-item-2 .blog-overlay { background:#000; }
                  .blog-post-type-3 .article-item.article-item-2:hover .blog-overlay { background:#000; }
                  .blog-post-type-3 .article-item:hover .blog-overlay { background:#000; }
                  .blog-post-type-3 .article-item .blog-btn a { color:#ffffff; } 
                  .blog-post-type-3 .article-item .blog-btn a:hover { color:#69e0ff; }
               </style>
            </div>
            <div class="dt-sc-hr-invisible-large"></div>
         </div>
      </div>
      <div id="shopify-section-1546672563385" class="shopify-section">
         <div data-section-id="1546672563385" data-section-type="testimonial-type-1" class="testimonial-type-1">
            <div class="grid-uniform">
               <div class="container-bg">
                  <div class="section-header section-header--small">
                     <div class="border-title">
                        <h2 class="section-header__title" style="color:#11423b">What our customers say</h2>
                        <div class="short-desc">
                           <p style="color:#676e6d">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                        </div>
                     </div>
                  </div>
                  <div class="dt-sc-hr-invisible-small"></div>
                  <div class="grid__item">
                     <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                        <div class="testimonial-block" style="background:#e7eeef">
                           <div class="grid__item wide--one-fifth post-large--one-fifth large--one-fifth">
                              <div class="authorimg">
                                 <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/client-1.jpg?v=1549362112" alt="John Bowski">
                              </div>
                           </div>
                           <div class="grid__item wide--four-fifths post-large--four-fifths large--four-fifths">
                              <div class="testimonial-detail">
                                 <blockquote>
                                    <q style="color:#676e6d">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cumsociis natoque penatibus et magnis dis parturient montes.</q>
                                 </blockquote>
                                 <h6 style="color:#11423b">John Bowski <span style="color:#69e0ff">Director</span></h6>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                        <div class="testimonial-block" style="background:#e7eeef">
                           <div class="grid__item wide--one-fifth post-large--one-fifth large--one-fifth">
                              <div class="authorimg">
                                 <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/client-2.jpg?v=1549362129" alt="Candis Tant">
                              </div>
                           </div>
                           <div class="grid__item wide--four-fifths post-large--four-fifths large--four-fifths">
                              <div class="testimonial-detail">
                                 <blockquote>
                                    <q style="color:#676e6d">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cumsociis natoque penatibus et magnis dis parturient montes.</q>
                                 </blockquote>
                                 <h6 style="color:#11423b">Candis Tant <span style="color:#69e0ff">Marketing ANalyst</span></h6>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                        <div class="testimonial-block" style="background:rgba(0,0,0,0)">
                           <div class="grid__item wide--one-fifth post-large--one-fifth large--one-fifth">
                              <div class="authorimg">
                                 <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/client-3.jpg?v=1549362160" alt="Aldo Dhillon">
                              </div>
                           </div>
                           <div class="grid__item wide--four-fifths post-large--four-fifths large--four-fifths">
                              <div class="testimonial-detail">
                                 <blockquote>
                                    <q style="color:#676e6d">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cumsociis natoque penatibus et magnis dis parturient montes.</q>
                                 </blockquote>
                                 <h6 style="color:#11423b">Aldo Dhillon <span style="color:#69e0ff">Manager</span></h6>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                        <div class="testimonial-block" style="background:rgba(0,0,0,0)">
                           <div class="grid__item wide--one-fifth post-large--one-fifth large--one-fifth">
                              <div class="authorimg">
                                 <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/client-4.jpg?v=1549362185" alt="Fae Mullin">
                              </div>
                           </div>
                           <div class="grid__item wide--four-fifths post-large--four-fifths large--four-fifths">
                              <div class="testimonial-detail">
                                 <blockquote>
                                    <q style="color:#676e6d">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cumsociis natoque penatibus et magnis dis parturient montes.</q>
                                 </blockquote>
                                 <h6 style="color:#11423b">Fae Mullin <span style="color:#69e0ff">PSD Designer</span></h6>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <style>
            .testimonial-type-1 {background:#e7eeef;padding: 30px 0;}
         </style>
      </div>
      <!-- END content_for_index -->
   </div>
</div>