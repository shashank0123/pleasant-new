 <div id="shopify-section-footer-model-4" class="shopify-section"><div data-section-id="footer-model-4"  data-section-type="Footer-model-4" class="footer-model-4">
 <footer class="site-footer" style="background:#ffffff;">   
    <div class="wrapper">
      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half">
        <div class="footer-logo">
          <a href="{{url('/')}}">
            <img class="normal-footer-logo" src="/images/softie-logo_200x2544.png?v=1550576766" alt="Softie" />
          </a>


          <p style="color:#676e6d">Pellentesque posuere orci lobortis scelerisque blandit. Donec id tellus lacinia an, tincidunt risus ac, consequat velit. <br><br>Quisquemos sodales suscipit tortor ditaemcos condimentum lacus meleifend menean viverra auctor blanditos.</p> 
          
          <div class="footer_social_icons">

            <ul class="inline-list social-icons">

              <li>
                <a class="icon-fallback-text twitt hexagon" target="blank" href="https://twitter.com/shopify" title="Twitter">


                  <span class="fab fa-twitter" aria-hidden="true"></span>

                </a>
              </li>


              <li>
                <a class="icon-fallback-text fb hexagon" target="blank" href="https://www.facebook.com/shopify" title="Facebook">
                  <span class="fab fa-facebook" aria-hidden="true"></span>
                  <span class="fallback-text">Facebook</span>
                </a>
              </li>



              <li>
                <a class="icon-fallback-text google hexagon" target="blank" href="#" title="Google+" rel="publisher">
                  <span class="fab fa-google-plus-g" aria-hidden="true"></span>
                  <span class="fallback-text">Google</span>
                </a>
              </li>



              <li>
                <a class="icon-fallback-text tumblr" target="blank" href="#" title="Tumblr">
                  <span class="fab fa-tumblr" aria-hidden="true"></span>
                  <span class="fallback-text">Tumblr</span>
                </a>
              </li>





            </ul>

            
          </div>
        </div>
        
      </div>
      
      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half">

        <h4 style="color:#11423b">Best sellers</h4>
        <?php
        $best_sell = DB::table('products')->orderBy('created_at','DESC')->limit(4)->get();
        ?>
        <div class="footer_top_rated_products">
          <ul class="no-bullets top-products">
            @if(!empty($best_sell))
            @foreach($best_sell as $product)
            <li> 

              <a class="thumb" href="{{ url('product-detail/'.$product->slug ?? '') }}">                                        
                <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="Plain Crush Curtains">                                              
              </a>

              <div class="top-products-detail">
                <a style="color:#676e6d" href="{{ url('product-detail/'.$product->slug ?? '') }}"> {{ $product->name ?? '' }} </a>
                
                <div class="top-product-prices">
                  <span style="color:#676e6d" class="price"><span class=money>RS. {{ $product->price ?? '' }}</span></span>

                </div>

                <span class="shopify-product-reviews-badge" data-id="2204009070656"></span> 
              </div>
            </li>
            @endforeach
            @endif
            
           
            
          </ul>
        </div>
      </div>

      
      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half">
<?php 
  $tags = DB::table('categories')->where('parent_id',NULL)->orderBy('created_at','DESC')->limit(5)->get();
?>

        <h4 style="color:#11423b">Tags</h4>
        
        <div class="footer-tags">

          <ul>

            @if(count($tags)>0)
            @foreach($tags as $tag)
            <li><a style="border:1px solid #e7eeef;color:#676e6d" href="{{ url('products/'.$tag->slug) }}">{{$tag->slug}}</a></li>
            @endforeach
            
@endif
           
            
          </ul>
        </div>

        
        
        <h4 style="color:#11423b">Information</h4>
        
        <ul class="site-footer__links">

          <li><a style="color:#676e6d" href="{{ url('blogs') }}">Blogs</a></li>
          
          <li><a style="color:#676e6d" href="{{ url('about_us') }}">About Us</a></li>
          
          <li><a style="color:#676e6d" href="{{ url('contact_us') }}">Contact Us</a></li>
          
          <li><a style="color:#676e6d" href="{{ url('terms_n_conditions') }}">Terms & conditions</a></li>

          <li><a style="color:#676e6d" href="{{ url('privacy_policy') }}">Privacy Policy</a></li>
          
          <li><a style="color:#676e6d" href="{{ url('return_policy') }}">Return Policy</a></li>
          
        </ul>
        
      </div>
      
      
      <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half">

        <div class="footer_contact_info">

          <h4 style="color:#11423b">We are @</h4>
          
          <ul>
           <li class="address"> <h6 style="color:#676e6d"><span class="fa fa-home"></span>Address</h6>  <p style="color:#676e6d"> No: 58 A, East Madison Street,<br>
           Baltimore, MD, USA 4508</p><mark></mark> </li>
           <li><h6 style="color:#676e6d"><span class="fa fa-envelope"></span>Email</h6>
            <p> <a style="color:#676e6d" title="" href="#">buddhathemes@pleasantmattress.com</a> </p> 
            <p> <a style="color:#676e6d" title="" href="#">support@pleasantmattress.com</a> </p> 
          </li>
          <li> <h6 style="color:#676e6d"><span class="fa fa-phone"></span>Phone</h6> 
            <p style="color:#676e6d;">  +91 1234567890</p>
            <p style="color:#676e6d">  0803 - 080 - 3082</p>
          </li>
          <li><h6 style="color:#676e6d"><span class="fa fa-fax"></span>Fax</h6>  <p style="color:#676e6d">2542 2445 2566</p><mark></mark></li>
        </ul>
      </div>
    </div>

  </div>
</footer>
<div class="footer-bottom" style="background:#e7eeef;">
  <div class="wrapper">

    <p style="color:#676e6d;" class="copyright">Copyright &copy; 2020, Softie

      <a target="_blank" rel="nofollow" href="https://www.shopify.com/?utm_campaign=poweredby&amp;utm_medium=shopify&amp;utm_source=onlinestore">Powered by Shopify</a>

    </p>


    <div class="footer-icons">        
      <ul class="inline-list payment-icons">
        <li><a href="cart.html"><img src="/images/card-icons_0000_23_small4368.png?v=1546860384" alt="payment_icon_1" /></a></li>
        <li><a href="cart.html"><img src="/images/card-icons_0015_8_smalld065.png?v=1546860397" alt="payment_icon_2" /></a></li>
        <li><a href="cart.html"><img src="/images/card-icons_0023_1_smallc707.png?v=1546860422" alt="payment_icon_3" /></a></li>
        <li><a href="cart.html"><img src="/images/card-icons_0018_5_small5d92.png?v=1546860443" alt="payment_icon_4" /></a></li>






      </ul>                 
    </div> 

  </div>
</div>

<style>
  .footer-model-4 .top-products li a:hover, .footer-model-4 .site-footer__links li a:hover,.footer-model-4 .site-footer a:hover { color:#69e0ff !important; }
  .footer-model-4 .site-footer .social-icons li a { color:#11423b;  }
  .footer-model-4 .site-footer .social-icons li a:hover { color:#11423b;  }
  .footer-model-4 .site-footer .social-icons li a:hover { border-color:#11423b !important;background:#69e0ff !important;color:#11423b !important; }
  .footer-model-4 .footer-tags li a:hover { border-color:#69e0ff !important;background:#69e0ff !important;color:#11423b !important; }
  .footer-model-4 .site-footer a:hover { color: !important; }
  .footer-model-4 .copyright a { color:#676e6d; }
  .footer-model-4 .copyright a:hover { color:#11423b; }
  .footer-model-4 .top-products-detail span i { color:#676e6d; }

</style>
</div>


</div>
