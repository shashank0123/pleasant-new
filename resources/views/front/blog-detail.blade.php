@extends('layouts.front.app')
@section('content')
<main class="main-content">
   <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>{{$blog->title ?? ''}}</h1>
      <a href="/" title="Back to the frontpage">Home</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <a href="{{ url('/blogs') }}" title="">Blogs</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>{{$blog->title ?? ''}}</span>
   </nav>
   <div class="dt-sc-hr-invisible-large"></div>
   <div class="wrapper">
      <div class="grid-uniform">
         <div class="grid__item">
            <div class="container-bg">
               <div id="shopify-section-article" class="shopify-section">
                  <div class="grid blog-design-4 blog-detail-section">
                     <div class="container">
                        <div class="blog-section">
                           <article class="grid__item" itemscope="" itemtype="">
                              <a href="{{ url('blog-detail/'.$blog->slug ?? '') }}" title=""><img src="{{ asset('storage/'.$blog->cover ?? '') }}" alt="{{ $blog->title ?? '' }}" class="article__image"></a>
                              <div class="blog-description">
                                 <div class="blogs-sub-title">
                                    <p class="blog-date">
                                       <i class="far fa-calendar"></i>   <span data-datetime="2019-02-14"><span class="date">{{ date('M d,Y', strtotime(explode(' ',$blog->created_at)[0])) }}</span></span>             
                                    </p>
                                    <p class="comments-count">{{ $comments }} Comments</p>
                                    <p class="author">
                                       <i class="far fa-user"></i>
                                       <span>{{ $blog->author ?? '' }}</span>
                                    </p>
                                 </div>
                                 <div class="blog_section_detail">
                                    <h3><a href="{{ url('blog-detail/'.$blog->slug ?? '') }}">{{ $blog->title ?? '' }}</a></h3>
                                    <p class="desc">{{ $blog->description ?? '' }}</p>
                                    @if(count($tags)>0)
                                    <div class="blog-tag">
                                       <i class="fas fa-tag"></i>
                                       @foreach($tags as $tag)
                                       <a class="blog-tags" href="/blogs/{{$tag}}">{{$tag}}</a>/
                                       @endforeach
                                       
                                    </div>
                                    @endif
                                    <div class="social-sharing normal" data-permalink="https://softie-demo.myshopify.com/blogs/news/modern-interior-mattress">
                                       <label>Share this on:  </label>      
                                       <a target="_blank" href="#" class="share-facebook">
                                          <span class="fab fa-facebook"></span>
                                          <!--  <span class="share-title">Share</span>
                                             <span class="share-count">0</span>
                                             -->
                                       </a>
                                       <a target="_blank" href="#" class="share-twitter">
                                          <span class="fab fa-twitter"></span>
                                          <!-- <span class="share-title">Tweet</span>
                                             <span class="share-count">0</span>
                                             -->
                                       </a>
                                       <a target="_blank" href="#" class="share-pinterest">
                                          <span class="fab fa-pinterest"></span>
                                          <!-- <span class="share-title">Pin it</span>
                                             <span class="share-count">0</span>
                                             -->
                                       </a>
                                       <a target="_blank" href="#" class="share-google">
                                          <!-- Cannot get Google+ share count with JS yet -->
                                          <span class="fab fa-google"></span>
                                          <!-- 
                                             <span class="share-count">+1</span>
                                             -->
                                       </a>
                                    </div>
                                    <hr class="hr--clear hr--small">
                                    <div id="comments">
                                       <form method="post" action="{{ url('blog-review/submitreview') }}" id="comment_form" accept-charset="UTF-8" class="comment-form">
                                          <input type="hidden" name="blog_id" value="{{ $blog->id }}">
                                          @csrf
                                          <h4>Leave a comment</h4>
                                          <div class="grid">
                                             <p class="grid__item wide--one-half post-large--one-half large--one-half">
                                                <label for="CommentAuthor" class="label--hidden">Name</label>
                                                <input type="text" name="name" placeholder="Name" id="CommentAuthor" value="" autocapitalize="words" required="required">
                                             </p>
                                             <p class="grid__item wide--one-half post-large--one-half large--one-half">
                                                <label for="CommentEmail" class="label--hidden">Email</label>
                                                <input type="email" name="email" placeholder="Email" id="CommentEmail" value="" autocorrect="off" autocapitalize="off" required="required">
                                             </p>
                                             <label for="CommentBody" class="label--hidden">Message</label>
                                             <textarea name="blog_review" id="CommentBody" placeholder="Message" required="required"></textarea>
                                          </div>
                                          <p><small>Please note, comments must be approved before they are published</small></p>
                                          <button type="submit" class="btn"><span>Post comment</span></button>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                           </article>
                        </div>
                     </div>
                  </div>
                  <style>
                     .blog-design-4.blog-detail-section .blog_section_detail h4 a,.blog-design-4.blog-detail-section .article-detail h5 a { color:#11423b; }
                     .blog-design-4.blog-detail-section .blog_section_detail h4 a:hover,.blog-design-4.blog-detail-section .article-detail h5 a:hover { color:#69e0ff; }
                     .blog-design-4.blog-detail-section .blog-detail .blog-date,.blog-design-4.blog-detail-section .blog-date { color:#333333; }
                     .blog-design-4.blog-detail-section .blog-detail .author,.blog-design-4 .author { color:#333333; }
                     .blog-design-4.blog-detail-section .blog-detail .author { color:#333333; }
                     .blog-design-4.blog-detail-section  .article .blog-detail i,.blog-design-4 .article .blogs-sub-title i   {  margin-right: 5px;color:#69e0ff; }
                     .blog-design-4.blog-detail-section  .comments-count { color:#333333; }
                     .blog-design-4.blog-detail-section .blog-tag a { color:#69e0ff; }
                     .blog-design-4.blog-detail-section  .blog-tag a:hover {color:#11423b; }
                  </style>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-hr-invisible-large"></div>
</main>
@endsection