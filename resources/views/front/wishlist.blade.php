@extends('layouts.front.app')
@section('content')
<main class="main-content">
   <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>Wishlist</h1>
      <a href="/" title="Back to the frontpage">Home</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>Wishlist</span>
   </nav>
   <div class="dt-sc-hr-invisible-large"></div>
   <div class="wrapper">
      <div class="grid-uniform">
         <div class="grid__item">
            <div class="container-bg">
               <form action="/cart" method="post" novalidate="" class="cart">


                <table class='table table-stripped'>
                  <thead class="dark-colored">
                    <tr>
                  <th colspan="2">Product</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Stock</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody class="light-colored">
                  @if(count($products)>0)
                  @foreach($products as $product)
                  <tr>
                    <td><a href="{{ url('product-detail/'.$product->slug ?? '') }}"><img src="{{ asset('storage/'.$product->cover ?? '') }}" style="height: 150px; width: 150px"> </a></td>
                    <td><span><br><br><a href="{{ url('product-detail/'.$product->slug ?? '') }}" class="productName">{{ $product->name ?? '' }}</a></span></td>
                    <td><br><br><b><span>Rs. {{ $product->price ?? '' }}</span></b></td>
                    <td><br><br>
                      <div class="cartQuantity">
                        <i class="fa fa-minus qtyMinus" onclick="minusProductQty({{ $product->id }})"></i>&nbsp;
                        <input type="text" name="quantity" id="quantityId{{$product->id ?? ''}}" value="1" class="productQuantity">&nbsp;
                        <i class="fa fa-plus qtyPlus" onclick="plusProductQty({{ $product->id }})" ></i>
                      </div>
                    </td>
                    <td><br><br>
                      @if($product->quantity>0)
                      <span class="green">{{'In stock'}}
                        @else
                      <span class="red">{{'In stock'}}
                        @endif
                    </td>
                    <td class="cartIcons"><br><br>
                      <i class="fa fa-cart-plus" onclick="checkStock({{$product->id}},{{$product->quantity}})"></i>
                      <i class="fa fa-times" onclick="confirmDelete({{$product->id}})"></i>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
                </table>



                 

                  <div class="cart__row">
                     <div class="grid shipping-section">
                        <div class="grid__item wide--one-half post-large--one-half large--one-half">
                           
                        </div>
                        <div class="grid__item text-right wide--one-half post-large--one-half large--one-half">
                           
                           <div class="cart_btn">
                              <a class="btn" href="{{ url('/') }}" id="update-cart">Continue shopping</a>
                              <a class="btn" href="{{ url('/') }}" id="update-cart">Empty Wishlist</a>
                           </div>
                        </div>
                     </div>
                  </div>

               </form>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-hr-invisible-large"></div>
</main>
@endsection