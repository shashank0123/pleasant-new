 <div class="has-items">
   @if(count($cartproducts)>0)
   <ul class="mini-products-list">
      @foreach($cartproducts as $product)
      <li class="item" id="div-list{{$product->id}}">
         <a href="{{ url('product-detail/'.$product->slug ?? '') }}" title="{{ $product->name ?? '' }}" class="product-image">
            <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="Backrest Cushion - 8 / Silk"></a>
            <div class="product-details">
               <p class="product-name">
                  <a href="{{ url('product-detail/'.$product->slug ?? '') }}">
                     {{ $product->name ?? '' }}
               </a>
            </p>
            <div class="cart-collateral"><span class="price"><span class="money" data-currency-usd="RS. {{ $product->price ?? '' }}">RS. {{ $product->price ?? '' }}</span></span> x {{ $product->total_quantity ?? '1' }}</div>
         </div>
         <a href="javascript:void(0)" title="Remove This Item" class="btn-remove" onclick="removeProduct({{ $product->id ?? '' }})"><span class="fas fa-times"></span></a>
      </li>
      @endforeach
   </ul>
   <div class="summary">
      <p class="total">
         <span class="label" style="color: #000">Cart total :</span>
         <span class="price"><span class="money" data-currency-usd="RS. {{ $bill ?? '0' }}" id="totalCart"> RS. {{ $bill ?? '0' }}</span></span> 
      </p>
   </div>
   <div class="actions">
      <button class="btn" onclick="window.location='/checkout'"><i class="fas fa-check"></i>Check Out</button>
      <button class="btn text-cart" onclick="window.location='/cart'"><i class="fas fa-shopping-basket"></i>View Cart</button>
   </div>
   @else

   <div class="no-items" style="display: block;">
      <p>Your cart is currently empty!</p>
      <p class="text-continue"><a class="btn" href="javascript:void(0)">Continue shopping</a></p>
   </div>


   @endif
</div>