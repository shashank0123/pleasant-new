<?php
  // var_dump(session()->get('cart')); die;
// var_dump($cartProducts); die;
?>
@extends('layouts.front.app')
@section('content')
<main class="main-content">
   <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>Your Shopping Cart</h1>
      <a href="/" title="Back to the frontpage">Home</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>Your Shopping Cart</span>
   </nav>
   <div class="dt-sc-hr-invisible-large"></div>
   <div class="wrapper">
      <div class="grid-uniform">
         <div class="grid__item">
            <div class="container-bg">
               <form action="/cart" method="post" novalidate="" class="cart">


                <table class='table table-stripped'>
                  <thead class="dark-colored">
                    <tr>
                  <th colspan="2">Product</th>
                  <th>Price</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>Total</th>
                  <th>Remove</th>
                </tr>
                </thead>
                <tbody class="light-colored">
                  @if(count($cartProducts)>0)
                  @foreach($cartProducts as $product)
                  <tr id="cartTr{{$product->id ?? ''}}">
                    <td><a href="{{ url('product-detail/'.$product->slug ?? '') }}"><img src="{{ asset('storage/'.$product->cover ?? '') }}" style="height: 150px; width: 150px"> </a></td>
                    <td><span><br><br><a href="{{ url('product-detail/'.$product->slug ?? '') }}" class="productName">{{ $product->name ?? '' }}</a></span></td>   
                    <td><br><br><b><span>Rs. {{ $product->price ?? '' }}</span></b></td>
                    <td><br><br><b><span>{{ $product->size ?? '' }}</span></b></td>
                    <td><br><br>
                      <div class="cartQuantity">
                        <i class="fa fa-minus qtyMinus" onclick="minus({{ $product->id }})"></i>&nbsp;
                        <input type="text" name="quantity" id="productQuantity{{$product->id ?? ''}}" value="{{ $product->product_quantity ?? '1' }}" class="productQuantity">&nbsp;
                        <i class="fa fa-plus qtyPlus" onclick="plus({{ $product->id }})" ></i>
                      </div>
                    </td>
                    <td><br><br><b><span id="total{{$product->id}}">{{ $product->total_price ?? '' }}.00</span></b></td>
                    <td class="cartIcons"><br><br><i class="fa fa-times" onclick="deleteCartProduct({{ $product->id ?? '' }})"></i></td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
                </table>


                 

                  <div class="cart__row">
                     <div class="grid shipping-section">
                        <div class="grid__item wide--one-half post-large--one-half large--one-half">
                           <button type="button" class="text-link cart__note-add">
                           Add a note to your order
                           </button>
                           <div class="cart__note">
                              <label for="CartSpecialInstructions">Special instructions for seller</label>
                              <textarea name="note" class="input-full" id="CartSpecialInstructions"></textarea>
                           </div>
                        </div>
                        <div class="grid__item text-right wide--one-half post-large--one-half large--one-half">
                           <p class="cart_total_price" style="margin:0;">
                              <span class="cart__subtotal-title h5">Subtotal :</span>
                              <span class="h5 cart__subtotal"><span class="money" data-currency-usd="$4,371.00" id="totalBill">RS. {{ $grandTotal }}.00 </span></span>
                           </p>
                           <p class="shopping-checkout"><em>Shipping, taxes, and discounts will be calculated at checkout.</em></p>
                           <div class="cart_btn">
                              <a class="btn" href="{{ url('/') }}" id="update-cart">Continue shopping</a>
                             
                              <input type="button" onclick="window.location='/checkout'" name="checkout" class="btn" value="Check Out">
                           </div>
                        </div>
                     </div>
                  </div>

               </form>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-hr-invisible-large"></div>
</main>
@endsection