@if(count($products)>0)
    <table class="table table-striped">
        <thead>
        <th class="col-md-2 col-lg-2">Cover</th>
        <th class="col-md-5 col-lg-5">Name</th>
        <th class="col-md-2 col-lg-1">Quantity</th>
        <th class="col-md-2 col-lg-2">Price</th>
        <th class="col-md-3 col-lg-2">Total Price</th>
        </thead>
        <tfoot>
        <tr>
            <td class="bg-warning">Subtotal</td>
            <td class="bg-warning"></td>
            <td class="bg-warning"></td>
            <td class="bg-warning"></td>
            <td class="bg-warning">{{config('cart.currency')}} {{ number_format($totalBill, 2, '.', ',') }}</td>
        </tr>
        <tr>
            <td class="bg-warning">Shipping</td>
            <td class="bg-warning"></td>
            <td class="bg-warning"></td>
            <td class="bg-warning"></td>
            <td class="bg-warning">{{config('cart.currency')}} <span id="shippingFee">{{ number_format(0, 2) }}</span></td>
        </tr>
        <tr>
            <td class="bg-warning">Tax</td>
            <td class="bg-warning"></td>
            <td class="bg-warning"></td>
            <td class="bg-warning"></td>
            <td class="bg-warning">{{config('cart.currency')}} {{ number_format(0, 2) }}</td>
        </tr>
        <tr>
            <td class="bg-success">Total</td>
            <td class="bg-success"></td>
            <td class="bg-success"></td>
            <td class="bg-success"></td>
            <td class="bg-success">{{config('cart.currency')}} <span id="grandTotal" data-total="{{ $totalBill }}">{{ number_format($totalBill, 2, '.', ',') }}</span></td>
        </tr>
        </tfoot>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>
                    <a href="{{ url('product-detail/'.$product->slug) }}" class="hover-border">
                        @if(isset($product->cover))
                            <img src="{{ asset('storage/'.$product->cover) }}" alt="{{ $product->name }}" class="img-responsive img-thumbnail">
                        @else
                            <img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                        @endif
                    </a>
                </td>
                <td>
                    <p>
                        <strong>{{ $product->name }}</strong> <br />
                       
                    </p>
                    <hr>
                    <div class="product-description">
                        {!! substr($product->description,0,100) !!}...
                    </div>
                </td>
                <td>
                    <form action="" class="form-inline" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="input-group">
                            <input type="text" name="quantity" value="{{ $product->cart_quantity }}" class="form-control" readonly="readonly" />
                            
                        </div>
                    </form>
                </td>

               <!--  <td>
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">
                        <button onclick="return confirm('Are you sure?')" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    </form>
                </td> -->
                <td>{{config('cart.currency')}} {{ number_format($product->price, 2) }}</td>
                <td>{{config('cart.currency')}} {{ number_format($product->total_price, 2) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        let courierRadioBtn = $('input[name="rate"]');
        courierRadioBtn.click(function () {
            $('#shippingFee').text($(this).data('fee'));
            let totalElement = $('span#grandTotal');
            let shippingFee = $(this).data('fee');
            let total = totalElement.data('total');
            let grandTotal = parseFloat(shippingFee) + parseFloat(total);
            totalElement.html(grandTotal.toFixed(2));
        });
    });
</script>