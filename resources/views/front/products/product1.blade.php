@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="product"/>
    {{-- <meta property="og:title" content="{{ $product->name }}"/> --}}
    {{-- <meta property="og:description" content="{{ strip_tags($product->description) }}"/> --}}
    {{-- @if(!is_null($product->cover)) --}}
        {{-- <meta property="og:image" content="{{ asset("storage/$product->cover") }}"/> --}}
    {{-- @endif --}}
@endsection

@section('content')
<main class="main-content">  
    <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>Cotton Striped Pillows</h1>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>Cotton Striped Pillows</span>
  </nav>
  <div class="dt-sc-hr-invisible-large"></div> 
  <div class="wrapper">
      <div class="grid-uniform">
        <div class="grid__item">  
          <div class="container-bg"> 
            <div class="position-change">
                <div class="grid__item wide--one-fifth post-large--one-quarter">
                    <div class="product_sidebar">  
                      <div id="shopify-section-sidebar-category" class="shopify-section">  
                        <div class="widget widget_product_categories">
                          <h4>
                            Category
                        </h4>  
                        <ul class="product-categories dt-sc-toggle-frame-set">



                            <li class="cat-item cat-item-39 cat-parent first">
                              <i></i>
                              <a class="" href="standard-microfibre-pillow.html">Curtains</a> <span class="dt-sc-toggle"></span>


                              <ul class="children dt-sc-toggle-content ">


                                <li class="second">
                                  <i></i>             
                                  <a class="" href="umang-pillow.html">Pindia</a>




                              </li>


                              <li class="second">
                                  <i></i>             
                                  <a class="" href="comfort-cushion.html">Linenwalas</a>




                              </li>


                              <li class="second">
                                  <i></i>             
                                  <a class="" href="umang-pillow.html">Sizzler</a>




                              </li>

                          </ul>

                      </li>




                      <li class="cat-item cat-item-39 cat-parent first">
                          <i></i>
                          <a class="" href="umang-pillow.html">Throws</a> <span class="dt-sc-toggle"></span>


                          <ul class="children dt-sc-toggle-content ">


                            <li class="second">
                              <i></i>             
                              <a class="" href="comfort-cushion.html">Pluchi</a>




                          </li>


                          <li class="second">
                              <i></i>             
                              <a class="" href="standard-microfibre-pillow.html">Napa</a>




                          </li>


                          <li class="second">
                              <i></i>             
                              <a class="" href="umang-pillow.html">Trade Star</a>




                          </li>

                      </ul>

                  </li>




                  <li class="cat-item cat-item-39 cat-parent">
                      <i></i><a class=" " href="standard-microfibre-pillow.html">Carpets</a> 
                  </li>




                  <li class="cat-item cat-item-39 cat-parent">
                      <i></i><a class=" " href="comfort-cushion.html">Wallpaper</a> 
                  </li>




                  <li class="cat-item cat-item-39 cat-parent first">
                      <i></i>
                      <a class="" href="umang-pillow.html">Floor Mats</a> <span class="dt-sc-toggle"></span>


                      <ul class="children dt-sc-toggle-content ">


                        <li class="second">
                          <i></i>             
                          <a class="" href="comfort-cushion.html">Saral Home</a>




                      </li>


                      <li class="second">
                          <i></i>             
                          <a class="" href="umang-pillow.html">Status</a>




                      </li>


                      <li class="second">
                          <i></i>             
                          <a class="" href="comfort-cushion.html">Younique</a>




                      </li>

                  </ul>

              </li>


          </ul>

      </div>







  </div>   
  <div id="shopify-section-product-sidebar-deals" class="shopify-section">
    <div data-section-id="product-sidebar-deals" data-section-type="product-sidebar-deals" class="product-sidebar-deals">  
        <div class="widget widget_top_rated_products">


          <h4><span>Hot Deals</span></h4>  


          <div class="widget_top_rated_products-section">
              <ul class="no-bullets sidebar-deal-products owl-carousel owl-theme owl-loaded owl-drag">













                  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1020px;"><div class="owl-item active" style="width: 245px; margin-right: 10px;"><li> 


                      <a class="thumb grid__item" href="plain-blanket.html">                                          
                        <img alt="featured product" src="/images/products/p428ef.jpg?v=1550307794">                                              























                    </a>

                    <div class="products">
                      <div class="top-products-detail product-detail grid__item"> 
                          <div class="product_left">
                            <a class="grid-link__title" href="plain-blanket.html"> Plain Blanket </a>
                            <span class="shopify-product-reviews-badge" data-id="2204006449216"></span>

                            <ul class="item-swatch color_swatch_Value">  





                            </ul>



                        </div>
                        <div class="top-product-prices grid-link__meta">

                          <div class="product_price">
                            <div class="grid-link__org_price">
                                <span class="money" data-currency-usd="$386.00" data-currency="INR">₹ 386.00</span>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </li></div><div class="owl-item" style="width: 245px; margin-right: 10px;"><li> 


          <a class="thumb grid__item" href="eyelet-polyster-curtain.html">                                          
            <img alt="featured product" src="/images/products/p196ac6.jpg?v=1550303828">                                              


            
            


















            
        </a>

        <div class="products">
          <div class="top-products-detail product-detail grid__item"> 
              <div class="product_left">
                <a class="grid-link__title" href="eyelet-polyster-curtain.html"> Eyelet Polyster Curtain </a>
                <span class="shopify-product-reviews-badge" data-id="2204005466176"></span>

                <ul class="item-swatch color_swatch_Value">  





                </ul>



            </div>
            <div class="top-product-prices grid-link__meta">

              <div class="product_price">
                <div class="grid-link__org_price">
                    <span class="money" data-currency-usd="$225.00" data-currency="INR">₹ 225.00</span>
                </div>

            </div>
        </div>

    </div>
</div>
</li></div><div class="owl-item" style="width: 245px; margin-right: 10px;"><li> 


  <a class="thumb grid__item" href="micro-fibre-cushion.html">                                          
    <img alt="featured product" src="/images/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993561a.jpg?v=1550292544">                                              























</a>

<div class="products">
  <div class="top-products-detail product-detail grid__item"> 
      <div class="product_left">
        <a class="grid-link__title" href="micro-fibre-cushion.html"> Micro Fibre Cushion </a>
        <span class="shopify-product-reviews-badge" data-id="2204005007424"></span>

        <ul class="item-swatch color_swatch_Value">  





        </ul>



    </div>
    <div class="top-product-prices grid-link__meta">

      <div class="product_price">
        <div class="grid-link__org_price">
            <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span>
        </div>

    </div>
</div>

</div>
</div>
</li></div><div class="owl-item" style="width: 245px; margin-right: 10px;"><li> 


  <a class="thumb grid__item" href="soft-cotton-mattress.html">                                          
    <img alt="featured product" src="/images/products/p10_69ba0778-b027-4b92-8939-f8d7b16b8fb1a9d1.jpg?v=1550309620">                                              























</a>

<div class="products">
  <div class="top-products-detail product-detail grid__item"> 
      <div class="product_left">
        <a class="grid-link__title" href="soft-cotton-mattress.html"> Soft Cotton Mattress </a>
        <span class="shopify-product-reviews-badge" data-id="2204003237952"></span>

        <ul class="item-swatch color_swatch_Value">  





        </ul>



    </div>
    <div class="top-product-prices grid-link__meta">

      <div class="product_price">
        <div class="grid-link__org_price">
            <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span>
        </div>

        <del><span class="money" data-currency-usd="$400.00" data-currency="INR">₹ 400.00</span></del>

    </div>
</div>

<span class="sale">Sale</span>    

</div>
</div>
</li></div></div></div><div class="owl-dots disabled"></div></ul> 
<div class="product-sidebar-deals-nav"><div class="owl-prev disabled"> <a class="prev btn active"><i class="fa fa-angle-left"></i></a></div>   <div class="owl-next"> <a class="next btn active"><i class="fa fa-angle-right"></i></a></div></div>
</div>
</div>
<style>
    .sidebar-deal-products .lof-clock-timer-detail-single li {background:#000;color:#ffffff;}

</style>
</div>




<script type="text/javascript">
    $(document).ready(function(){
      var productSidedeals = $(".sidebar-deal-products");
      productSidedeals.owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        navContainer: ".product-sidebar-deals-nav",
        navText: [' <a class="prev btn active"><i class="fa fa-angle-left"></i></a>',' <a class="next btn active"><i class="fa fa-angle-right"></i></a>'],
        dots: false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });      
  });
</script>
</div>
<div id="shopify-section-product-sidebar-bestsellers" class="shopify-section">
    <div data-section-id="product-sidebar-bestsellers" data-section-type="product-sidebar-bestsellers" class="product-sidebar-bestsellers">  
        <div class="widget widget_top_rated_products">


          <h4><span>Best Sellers</span></h4>  


          <ul class="no-bullets top-products">

            <li class="products"> 
              <span class="top_product_count">01</span>
              <div class="top-products-detail product-detail">            
                <a class="grid-link__title" href="plain-crush-curtains.html"> Plain Crush Curtains </a>            
                <div class="top-product-prices grid-link__meta">

                  <div class="product_price">
                    <div class="grid-link__org_price">
                        <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span>
                    </div>

                </div>
            </div>

            <span class="shopify-product-reviews-badge" data-id="2204009070656"></span>

        </div>




        <a class="thumb grid__item post-large--one-half" href="plain-crush-curtains.html">                  
            <img alt="featured product" src="/images/products/p22_small5f29.jpg?v=1550301918">                                              
        </a>

    </li>


    <li class="products"> 
      <span class="top_product_count">02</span>
      <div class="top-products-detail product-detail">            
        <a class="grid-link__title" href="comfort-cushion.html"> Comfort Cushion </a>            
        <div class="top-product-prices grid-link__meta">

          <div class="product_price">
            <div class="grid-link__org_price">
                <span class="money" data-currency-usd="$229.00" data-currency="INR">₹ 229.00</span>
            </div>

            <del><span class="money" data-currency-usd="$300.00" data-currency="INR">₹ 300.00</span></del>

        </div>
    </div>

    <span class="shopify-product-reviews-badge" data-id="2204006842432"></span>

</div>




<a class="thumb grid__item post-large--one-half" href="comfort-cushion.html">                  
    <img alt="featured product" src="/images/products/p24_small588d.jpg?v=1550291740">                                              
</a>

</li>


<li class="products"> 
  <span class="top_product_count">03</span>
  <div class="top-products-detail product-detail">            
    <a class="grid-link__title" href="umang-pillow.html"> Umang Pillow </a>            
    <div class="top-product-prices grid-link__meta">

      <div class="product_price">
        <div class="grid-link__org_price">
            <span class="money" data-currency-usd="$225.00" data-currency="INR">₹ 225.00</span>
        </div>

    </div>
</div>

<span class="shopify-product-reviews-badge" data-id="2204002222144"></span>

</div>




<a class="thumb grid__item post-large--one-half" href="umang-pillow.html">                  
    <img alt="featured product" src="/images/products/p5_small3619.jpg?v=1550291435">                                              
</a>

</li>


<li class="products"> 
  <span class="top_product_count">04</span>
  <div class="top-products-detail product-detail">            
    <a class="grid-link__title" href="on-ear-headphones-black.html"> Spring Queen Mattress </a>            
    <div class="top-product-prices grid-link__meta">

      <div class="product_price">
        <div class="grid-link__org_price">
            <span class="money" data-currency-usd="$539.00" data-currency="INR">₹ 539.00</span>
        </div>

    </div>
</div>

<span class="shopify-product-reviews-badge" data-id="2203999436864"></span>

</div>




<a class="thumb grid__item post-large--one-half" href="on-ear-headphones-black.html">                  
    <img alt="featured product" src="/images/products/p7_smallf069.jpg?v=1550308200">                                              
</a>

</li>


</ul> 
</div>
</div>


</div>    

<div id="shopify-section-custom-text-type-1" class="shopify-section index-section"><div data-section-id="custom-text-type-1" data-section-type="custom-text-type-1" class="custom-text-type-1">  

  <div class="grid-uniform">
     <ul class="support_block" style="background:#fff">         


        <li class="grid__item wow fadeInUp" data-wow-delay="ms">
          <div class="custom-text_section">                      

            <div class="support_section">
              <div class="support_icon">                      
                <a href="#" class="" style="background:#69e0ff;color:#11423b;"><i class="fas fa-gift"></i></a>
            </div>

            <div class="support_text"> 

              <h5 style="color:#11423b;">Special offer 1 + 1 = 3</h5>




              <span style="color:#333333;" class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>

          </div>     
      </div>
  </div>    
</li>  

<li class="grid__item wow fadeInUp" data-wow-delay="ms">
  <div class="custom-text_section">                      

    <div class="support_section">
      <div class="support_icon">                      
        <a href="#" class="" style="background:#69e0ff;color:#11423b;"><i class="fas fa-money-check"></i></a>
    </div>

    <div class="support_text"> 

      <h5 style="color:#11423b;">Free reward card</h5>




      <span style="color:#333333;" class="desc">Ut enim ad minim veniam, quis nostrud exercitation ullamco.</span>

  </div>     
</div>
</div>    
</li>  

<li class="grid__item wow fadeInUp" data-wow-delay="ms">
  <div class="custom-text_section">                      

    <div class="support_section">
      <div class="support_icon">                      
        <a href="#" class="" style="background:#69e0ff;color:#11423b;"><i class="fas fa-truck"></i></a>
    </div>

    <div class="support_text"> 

      <h5 style="color:#11423b;">Free shipping</h5>




      <span style="color:#333333;" class="desc">Fugiat nulla pariatur. Excepteur sint occaecat cupidatat.</span>

  </div>     
</div>
</div>    
</li>  

<li class="grid__item wow fadeInUp" data-wow-delay="ms">
  <div class="custom-text_section">                      

    <div class="support_section">
      <div class="support_icon">                      
        <a href="#" class="" style="background:#69e0ff;color:#11423b;"><i class="fas fa-undo"></i></a>
    </div>

    <div class="support_text"> 

      <h5 style="color:#11423b;">Order return</h5>




      <span style="color:#333333;" class="desc">nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</span>

  </div>     
</div>
</div>    
</li>  


</ul>
</div>

</div>

</div>

<div id="shopify-section-sidebar-promoimage" class="shopify-section">  
    <div class="widget widget_promo_img">
      <ul id="promo-carousel" class="owl-carousel owl-theme owl-loaded owl-drag">





          <div class="owl-stage-outer"><div class="owl-stage"></div></div><div class="owl-nav disabled"><div class="owl-prev">prev</div><div class="owl-next">next</div></div><div class="owl-dots disabled"></div></ul>
      </div>






      <script type="text/javascript">
        $(document).ready(function(){
           $("#promo-carousel").owlCarousel({ 
            loop:false,
       // margin:10,
       nav:false,       
       dots: true,
       responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }

});
       });

   </script>
</div>
</div>

</div>

<div class="second">
    <div id="shopify-section-product-template" class="shopify-section">





        <div class="grid__item wide--four-fifths post-large--three-quarters">
          <div itemscope="" itemtype="http://schema.org/Product" class="single-product-layout-type-1"> 
            <meta itemprop="url" content="https://softie-demo.myshopify.com/products/cotton-striped-pillows">
            <meta itemprop="image" content="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p14_grande.jpg?v=1550229549">
            <div class="product-single">

              <div class="grid__item wide--one-half post-large--one-half large--one-half product-img-box">

                <div class="product-photo-container">

                  <a href="/images/products/p14b36b.jpg?v=1550229549">
                    <img id="product-featured-image" src="/images/products/p14_grandeb36b.jpg?v=1550229549" alt="Cotton Striped Pillows" data-zoom-image="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p14.jpg?v=1550229549">
                </a>            
            </div>



            <div class="  more-view-wrapper   more-view-wrapper-owlslider ">
              <ul id="ProductThumbs" class="product-photo-thumbs  owl-carousel owl-theme owl-loaded owl-drag">





                  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 275px;"><div class="owl-item active" style="width: 127.5px; margin-right: 10px;"><li class="grid-item">
                      <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p14_grande.jpg?v=1550229549" data-zoom-image="/images/products/p14b36b.jpg?v=1550229549">
                        <img src="/images/products/p14_mediumb36b.jpg?v=1550229549" alt="Cotton Striped Pillows">
                    </a>
                </li></div><div class="owl-item" style="width: 127.5px; margin-right: 10px;"><li class="grid-item">
                  <a href="javascript:void(0)" data-image="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_grande.jpg?v=1550229550" data-zoom-image="/images/products/p12670.jpg?v=1550229550">
                    <img src="/images/products/p1_medium2670.jpg?v=1550229550" alt="Cotton Striped Pillows">
                </a>
            </li></div></div></div><div class="owl-dots disabled"></div></ul>
            <div class="single-page-owl-carousel disabled"><div class="owl-prev disabled"><a class="prev"><i class="icon-arrow-left icons"></i></a></div><div class="owl-next disabled"><a class="next"><i class="icon-arrow-right icons"></i></a></div></div>
        </div>

        

    </div>

    <div class="product_single_detail_section grid__item wide--one-half post-large--one-half large--one-half">
        <h2 itemprop="name" class="product-single__title">Cotton Striped Pillows</h2>
        
        <div class="product_single_price">



            <div class="product_price">          
              <div class="grid-link__org_price" id="ProductPrice"><span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></div>
          </div>



          
      </div>
      <span class="shopify-product-reviews-badge" data-id="2203996028992"></span>
      <div class="grid__item product_desc_section" style="position:relative;border-top:1px solid $colorBorder;">

      </div>

      <div class="product-description rte" itemprop="description">
          Nam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo pharetras loremos.Donec pretium egestas sapien et mollis. Sample Unordered List Comodous in tempor ullamcorper miaculis Pellentesque vitae neque mollis urna mattis... 
      </div>

      <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">

          <meta itemprop="priceCurrency" content="INR">
          <link itemprop="availability" href="http://schema.org/InStock">

          
          
          



















          <form method="post" action="https://softie-demo.myshopify.com/cart/add" id="AddToCartForm" accept-charset="UTF-8" class="product-form" enctype="multipart/form-data"><input type="hidden" name="form_type" value="product"><input type="hidden" name="utf8" value="✓">



              <div class="selector-wrapper-secton">           
















                <style>
                  label[for="product-select-option-0"] { display: none; }
                  #productSelect-option-0 { display: none; }
                  #productSelect-option-0 + .custom-style-select-box { display: none !important; }
              </style>
              <script>$(window).load(function() { $('.product_single_detail_section .selector-wrapper:eq(0)').hide(); });</script>










              <div class="swatch clearfix" data-option-index="0">
                  <div class="header">Inch :</div>
                  <div class="swatch-section">







                      <div data-value="4" class="swatch-element 4 available">

                        <input id="swatch-0-4" type="radio" name="option-0" value="4">

                        <label for="swatch-0-4">
                          4
                          <img class="crossed-out" src="../../cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/soldout0c13.png?v=6625312704354037208">
                      </label>

                  </div>


                  <script>
                      jQuery('.swatch[data-option-index="0"] .4').removeClass('soldout').addClass('available').find(':radio').removeAttr('disabled');
                  </script>







                  <div data-value="6" class="swatch-element 6 available">

                    <input id="swatch-0-6" type="radio" name="option-0" value="6">

                    <label for="swatch-0-6">
                      6
                      <img class="crossed-out" src="../../cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/soldout0c13.png?v=6625312704354037208">
                  </label>

              </div>


              <script>
                  jQuery('.swatch[data-option-index="0"] .6').removeClass('soldout').addClass('available').find(':radio').removeAttr('disabled');
              </script>


          </div>
      </div>




















      <style>
          label[for="product-select-option-1"] { display: none; }
          #productSelect-option-1 { display: none; }
          #productSelect-option-1 + .custom-style-select-box { display: none !important; }
      </style>
      <script>$(window).load(function() { $('.product_single_detail_section .selector-wrapper:eq(1)').hide(); });</script>








      <div class="swatch clearfix" data-option-index="1">
          <div class="header">Material :</div>
          <div class="swatch-section">







              <div data-value="Cotton" class="swatch-element cotton available">

                <input id="swatch-1-cotton" type="radio" name="option-1" value="Cotton">

                <label for="swatch-1-cotton">
                  Cotton
                  <img class="crossed-out" src="../../cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/soldout0c13.png?v=6625312704354037208">
              </label>

          </div>


          <script>
              jQuery('.swatch[data-option-index="1"] .cotton').removeClass('soldout').addClass('available').find(':radio').removeAttr('disabled');
          </script>





          <script>
              jQuery('.swatch[data-option-index="1"] .cotton').removeClass('soldout').addClass('available').find(':radio').removeAttr('disabled');
          </script>


      </div>
  </div>





  <div class="selector-wrapper" style="display: none;"><label for="productSelect-option-0">Inch</label><div class="selector-arrow"><select class="single-option-selector" data-option="option1" id="productSelect-option-0"><option value="4">4</option><option value="6">6</option></select></div></div><div class="selector-wrapper" style="display: none;"><label for="productSelect-option-1">Material</label><div class="selector-arrow"><select class="single-option-selector" data-option="option2" id="productSelect-option-1"><option value="Cotton">Cotton</option></select></div></div><select name="id" id="productSelect" class="" style="display: none;">

      <option selected="selected" value="25391903211584">4 / Cotton</option>

      <option value="25392885170240">6 / Cotton</option>

  </select>
</div>          




<div class="product-single__quantity">
  <div class="quantity-box-section ">
    <label>Quantity :</label>

    <div class="quantity_width">


      <div class="dec button">-</div>
      
      <input type="number" id="quantity" name="quantity" value="1" min="1">
      
      <div class="inc button">+</div>
      

      <p class="min-qty-alert" style="display:none">Minimum quantity should be 1</p>
  </div>
</div>

<div class="total-price">
    <label>Subtotal : </label><span><span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
</div>

</div>


<script>
  jQuery(".button").on("click", function() {
    var oldValue = jQuery("#quantity").val(),
    newVal = 1;

    if (jQuery(this).text() == "+") {
      newVal = parseInt(oldValue) + 1;
  } else if (oldValue > 1) {
      newVal = parseInt(oldValue) - 1;
  }

  jQuery(".product-single #quantity").val(newVal);

  updatePricing();

});
  
  
  

  //update price when changing quantity
  function updatePricing() {


    //try pattern one before pattern 2
    var regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;
    var unitPriceTextMatch = jQuery('.product-single #ProductPrice').text().match(regex);

    if (!unitPriceTextMatch) {
      regex = /([0-9]+[.|,][0-9]+)/g;
      unitPriceTextMatch = jQuery('.product-single #ProductPrice').text().match(regex);        
  }

  if (unitPriceTextMatch) {
      var unitPriceText = unitPriceTextMatch[0];     
      var unitPrice = unitPriceText.replace(/[.|,]/g,'');
      var quantity = parseInt(jQuery('.product-single  #quantity').val());
      var totalPrice = unitPrice * quantity;

      var totalPriceText = Shopify.formatMoney(totalPrice, window.money_format);
      regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;     
      if (!totalPriceText.match(regex)) {
        regex = /([0-9]+[.|,][0-9]+)/g;
    } 
    totalPriceText = totalPriceText.match(regex)[0];

    var regInput = new RegExp(unitPriceText, "g"); 
    var totalPriceHtml = jQuery('.product-single #ProductPrice').html().replace(regInput ,totalPriceText);

    jQuery('.product-single .total-price span').html(totalPriceHtml);     
}
}

jQuery('.product-single #quantity').on('change', updatePricing);




var t = false

jQuery('input').focus(function () {
    var $this = jQuery(this)
    
    t = setInterval(

        function () {
            if (($this.val() < 1 ) && $this.val().length != 0) {
                if ($this.val() < 1) {
                    $this.val(1)
                }


                jQuery('.min-qty-alert').fadeIn(1000, function () {
                    jQuery(this).fadeOut(500)
                })
            }
        }, 50)
})

jQuery('input').blur(function () {
    if (t != false) {
        window.clearInterval(t)
        t = false;
    }
})





</script>


<div class="product-infor">


    <p class="product-vendor">
      <label>Brand :</label>
      <span>Recron</span>
  </p>


  <p class="product-type">
      <label>Product Type : </label>  
      <span>Pillow</span>
  </p>


  <p class="product-inventory" id="product-inventory">
      <label>Availability :  </label>              
      <span class="many-in-stock">Many In Stock</span>
  </p>










</div>

<button type="submit" name="add" id="AddToCart" class="btn">
    <i class="fa fa-cart-plus" aria-hidden="true"></i><span id="AddToCartText">Add to Cart</span>
</button>


<div data-shopify="payment-button" class="shopify-payment-button"><button class="shopify-payment-button__button shopify-payment-button__button--unbranded shopify-payment-button__button--hidden" disabled="disabled" aria-hidden="true">&nbsp;</button><button class="shopify-payment-button__more-options shopify-payment-button__button--hidden" disabled="disabled" aria-hidden="true">&nbsp;</button></div>



<div class="add-to-wishlist">     
    <div class="show">
      <div class="default-wishbutton-cotton-striped-pillows loading"><a title="Add to wishlist" class="add-in-wishlist-js btn" href="cotton-striped-pillows.html"><i class="far fa-heart"></i><span class="tooltip-label">Add to wishlist</span></a></div>
      <div class="loadding-wishbutton-cotton-striped-pillows loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="cotton-striped-pillows.html"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
      <div class="added-wishbutton-cotton-striped-pillows loading" style="display: none;"><a title="View Wishlist" class="added-wishlist btn add_to_wishlist" href="../pages/wishlist.html"><i class="fas fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
  </div>
</div>



</form>
</div>




<div class="share_this_btn">
  <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
    <a class="addthis_button_preferred_1"></a>
    <a class="addthis_button_preferred_2"></a>
    <a class="addthis_button_preferred_3"></a>
    <a class="addthis_button_preferred_4"></a>
    <a class="addthis_button_compact"></a>
    <a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">
    var addthis_product = 'sfy-2.0.2';
    var addthis_plugin_info = {"info_status":"enabled","cms_name":"Shopify","cms_version":null,"plugin_name":"AddThis Sharing Tool","plugin_version":"2.0.2","plugin_mode":"AddThis"};
var addthis_config     = {/*AddThisShopify_config_begins*/pubid:'xa-525fbbd6215b4f1a', button_style:'style3', services_compact:'', ui_delay:0, ui_click:false, ui_language:'', data_track_clickback:true, data_ga_tracker:'', custom_services:'', custom_services_size:true/*AddThisShopify_config_ends*/};
</script>
<script type="text/javascript" src="../../s7.addthis.com/js/300/addthis_widget.js#pubid=xa-525fbbd6215b4f1a"></script>
</div>

</div>
</div>
<div class="dt-sc-hr-invisible-large"></div>
<div class="dt-sc-tabs-container">
    <ul class="dt-sc-tabs">
      <li><a class="current" href="#"> Product Description </a></li> 
      <li><a class="" href="#"> Reviews  </a></li>
      <li><a class="" href="#"> Shipping Details  </a></li>
  </ul>

  <div class="dt-sc-tabs-content" id="desc_pro" style="display: block;">
      <p></p><p>Nam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo pharetras loremos.Donec pretium egestas sapien et mollis.</p>
      <h4>Sample Unordered List</h4>
      <ul>
        <li>Comodous in tempor ullamcorper miaculis</li>
        <li>Pellentesque vitae neque mollis urna mattis laoreet.</li>
        <li>Divamus sit amet purus justo.</li>
        <li>Proin molestie egestas orci ac suscipit risus posuere loremous</li>
    </ul>
    <h4>Sample Ordered Lista</h4>
    <ol>
        <li>Comodous in tempor ullamcorper miaculis.</li>
        <li>Pellentesque vitae neque mollis urna mattis laoreet.</li>
        <li>Divamus sit amet purus justo.</li>
        <li>Proin molestie egestas orci ac suscipit risus posuere loremous.</li>
    </ol>
    <h4>Sample Paragraph Text</h4>
    <blockquote>Praesent vestibulum congue tellus at fringilla. Curabitur vitae semper sem, eu convallis est. Cras felis nunc commodo eu convallis vitae interdum non nisl. Maecenas ac est sit amet augue pharetra convallis nec danos dui. Cras suscipit quam et turpis eleifend vitae malesuada magna congue. Damus id ullamcorper neque. Sed vitae mi a mi pretium aliquet ac sed elit. Pellentesque nulla eros accumsan quis justo at tincidunt lobortis denimes loremous. Suspendisse vestibulum lectus in lectus volutpat, ut dapibus purus pulvinar. Vestibulum sit amet auctor ipsum.</blockquote><p></p>
</div>


<div class="dt-sc-tabs-content" style="display: none;">
  <div class="commentlist">
    <div class="comment-text">
      <div class="rating-review">
        <div id="shopify-product-reviews" data-id="2203996028992"></div>
    </div>
</div>
</div>
</div>


<div class="dt-sc-tabs-content" style="display: none;">
  <p></p><h5>Returns Policy</h5>
  <p>You may return most new, unopened items within 30 days of delivery for a full refund. We'll also pay the return shipping costs if the return is a result of our error (you received an incorrect or defective item, etc.).</p>
  <p>You should expect to receive your refund within four weeks of giving your package to the return shipper, however, in many<span>&nbsp;</span>cases<span>&nbsp;</span>you will receive a refund more quickly. This time period includes the transit time for us to receive your return from the shipper (5 to 10 business days), the time it takes us to process your return once we receive it (3 to 5 business days), and the time it takes your bank to process our refund request (5 to 10 business days).</p>
  <p>If you need to return an item, simply<span>&nbsp;</span>login<span>&nbsp;</span>to your account, view the order using the 'Complete Orders' link under the My Account menu and click the Return Item(s) button. We'll notify you via e-mail of your refund once we've received and processed the returned item.</p>
  <h5>Shipping</h5>
  <p>We can ship to virtually any address in the world. Note that there are restrictions on some products, and some products cannot be shipped to international destinations.</p>
  <p>When you place an order, we will estimate shipping and delivery dates for you based on the availability of your items and the shipping options you choose. Depending on the shipping provider you choose, shipping date estimates may appear on the shipping quotes page.</p>
  <p>Please also note that the shipping rates for many items we sell are weight-based. The weight of any such item can be found on its detail page. To reflect the policies of the shipping companies we use, all weights will be rounded up to the next full pound.</p><p></p>
</div>

</div>
<div class="dt-sc-hr-invisible-small"></div>




<div class="related-products-container">

  <div class="section-header section-header--small">
    <div class="border-title">



      <h3 class="section-header__title">    
        Related Products
    </h3>

</div>
</div>
<div class="related_products_container">
  <ul class="grid-uniform grid-link__container related-products owl-carousel owl-theme owl-loaded owl-drag">









































































































































































































































































































































































































































      <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-3135px, 0px, 0px); transition: all 0s ease 0s; width: 12540px;"><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204007694400">

         <div class="products product-hover-15">
          <div class="product-container ">  





















            <a href="../collections/new-arrival/products/dual-comfort-mattress.html" class="grid-link">            


              <div class="featured-tag">
                <span class="badge badge--sale">          
                  <span class="gift-tag badge__text">Sale</span>
              </span>
          </div>



          <img src="/images/products/p3_7f396e1f-7041-4830-aaed-4a7e044fc998_large4834.jpg?v=1550300430" alt="Dual Comfort Mattress" class="featured-image">
      </a>


      <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

          <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
            <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p3_7f396e1f-7041-4830-aaed-4a7e044fc998.jpg?v=1550300430">
              <img src="/images/products/p3_7f396e1f-7041-4830-aaed-4a7e044fc9984834.jpg?v=1550300430" alt="Dual Comfort Mattress">
          </a>
      </li>
      
      <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p20.jpg?v=1550300430">
          <img src="/images/products/p204834.jpg?v=1550300430" alt="Dual Comfort Mattress">
      </a>
  </li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="dual-comfort-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204007694400">                                    
        <input type="hidden" name="id" value="25427683475520">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/dual-comfort-mattress.html" class="grid-link__title">Dual Comfort Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$228.00" data-currency="INR">₹ 228.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice" style="display: none;">
  <span><span class="money" data-currency-usd="$400.00" data-currency="INR">₹ 400.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204007694400"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204008546368">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/memory-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p2_large0e88.jpg?v=1550299516" alt="Memory Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p2.jpg?v=1550299516">
          <img src="/images/products/p20e88.jpg?v=1550299516" alt="Memory Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p3.jpg?v=1550299517">
      <img src="/images/products/p3112f.jpg?v=1550299517" alt="Memory Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="memory-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204008546368">                                    
        <input type="hidden" name="id" value="25427444760640">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/memory-foam-mattress.html" class="grid-link__title">Memory Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$534.00" data-currency="INR">₹ 534.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204008546368"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2203998322752">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/hand-made-cushion.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8_largee3cd.jpg?v=1550298962" alt="Hand Made Cushion" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8.jpg?v=1550298962">
      <img src="/images/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8e3cd.jpg?v=1550298962" alt="Hand Made Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6_04d3f889-b5a1-4910-95fd-b0d401368dae.jpg?v=1550298963">
      <img src="/images/products/p6_04d3f889-b5a1-4910-95fd-b0d401368dae3c41.jpg?v=1550298963" alt="Hand Made Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="hand-made-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203998322752">                                    
        <input type="hidden" name="id" value="25427285639232">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/hand-made-cushion.html" class="grid-link__title">Hand Made Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$500.00" data-currency="INR">₹ 500.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2203998322752"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2203997732928">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/throw-pillow-cushion.html" class="grid-link">            




      <img src="/images/products/p27_026f58da-16a0-40b3-8266-4dd698b5ef08_large7fdb.jpg?v=1550298406" alt="Throw Pillow Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27_026f58da-16a0-40b3-8266-4dd698b5ef08.jpg?v=1550298406">
          <img src="/images/products/p27_026f58da-16a0-40b3-8266-4dd698b5ef087fdb.jpg?v=1550298406" alt="Throw Pillow Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25.jpg?v=1550298406">
      <img src="/images/products/p257fdb.jpg?v=1550298406" alt="Throw Pillow Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="throw-pillow-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203997732928">                                    
        <input type="hidden" name="id" value="25427127107648">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/throw-pillow-cushion.html" class="grid-link__title">Throw Pillow Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$769.00" data-currency="INR">₹ 769.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2203997732928"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2203997175872">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/h151-headphones.html" class="grid-link">            




      <img src="/images/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94_large7817.jpg?v=1550297939" alt="Cotton Canvas Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94.jpg?v=1550297939">
          <img src="/images/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc947817.jpg?v=1550297939" alt="Cotton Canvas Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27.jpg?v=1550297939">
      <img src="/images/products/p277817.jpg?v=1550297939" alt="Cotton Canvas Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="h151-headphones" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203997175872">                                    
        <input type="hidden" name="id" value="25426986664000">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/h151-headphones.html" class="grid-link__title">Cotton Canvas Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2203997175872"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204001599552">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/backrest-cushion.html" class="grid-link">            




      <img src="/images/products/p26_large164d.jpg?v=1550292849" alt="Backrest Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p26.jpg?v=1550292849">
          <img src="/images/products/p26164d.jpg?v=1550292849" alt="Backrest Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17.jpg?v=1550292850">
      <img src="/images/products/p17830c.jpg?v=1550292850" alt="Backrest Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="backrest-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001599552">                                    
        <input type="hidden" name="id" value="25424764534848">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/backrest-cushion.html" class="grid-link__title">Backrest Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$499.00" data-currency="INR">₹ 499.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204001599552"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204005007424">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/micro-fibre-cushion.html" class="grid-link">            




      <img src="/images/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993_large561a.jpg?v=1550292544" alt="Micro Fibre Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993.jpg?v=1550292544">
          <img src="/images/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993561a.jpg?v=1550292544" alt="Micro Fibre Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_ef4f28be-6c20-4c97-b1ca-4b7387ff3ceb.jpg?v=1550292544">
      <img src="/images/products/p24_ef4f28be-6c20-4c97-b1ca-4b7387ff3ceb561a.jpg?v=1550292544" alt="Micro Fibre Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="micro-fibre-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204005007424">                                    
        <input type="hidden" name="id" value="25424443768896">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/micro-fibre-cushion.html" class="grid-link__title">Micro Fibre Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204005007424"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204007137344">

 <div class="products product-hover-15">
  <div class="product-container ">  









    <label class="deal-lable">Hurry, Only few item(s) left!</label>


    <div class="deal-clock lof-clock-2204007137344-detail"><ul class="list-inline"><li class="day"><b>199</b><span>Days</span></li><li class="hours"><b>19</b><span>Hours</span></li><li class="mins"><b>18</b><span>Min</span></li><li class="seconds"><b>20</b><span>Sec</span></li></ul></div>

    <style></style>



    
    <a href="../collections/new-arrival/products/cotton-plan-cushion.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p16_38a12388-a625-4040-a226-5f7a0132c079_large1508.jpg?v=1557811918" alt="Cotton Plan Cushion" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 360px; transform: translate3d(0px, 0px, 0px);">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p16_38a12388-a625-4040-a226-5f7a0132c079.jpg?v=1557811918">
      <img src="/images/products/p16_38a12388-a625-4040-a226-5f7a0132c0791508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27_grande_3a9009ff-6e78-477d-bfd2-0166db5f801d.jpg?v=1557811918">
      <img src="/images/products/p27_grande_3a9009ff-6e78-477d-bfd2-0166db5f801d1508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_grande_c00837cd-31d1-4603-87ab-02e38fe176e2.jpg?v=1557811918">
      <img src="/images/products/p17_grande_c00837cd-31d1-4603-87ab-02e38fe176e21508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_grande_1bebf316-7241-49fc-b00f-85b067d30812.jpg?v=1557811918">
      <img src="/images/products/p24_grande_1bebf316-7241-49fc-b00f-85b067d308121508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_grande_2011e86e-1567-4d8f-8969-7adde35fa695.jpg?v=1557811918">
      <img src="/images/products/p25_grande_2011e86e-1567-4d8f-8969-7adde35fa6951508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15.jpg?v=1557811918">
      <img src="/images/products/p151508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>


</ul><div class="lSAction"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="cotton-plan-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204007137344">                                    
        <input type="hidden" name="id" value="25424256434240">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/cotton-plan-cushion.html" class="grid-link__title">Cotton Plan Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$123.00" data-currency="INR">₹ 123.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$1,000.00" data-currency="INR">₹ 1,000.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204007137344"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204006842432">

 <div class="products product-hover-15">
  <div class="product-container ">  






    <label class="deal-lable">Hurry, Only few item(s) left!</label>


    <div class="deal-clock lof-clock-2204006842432-detail"><ul class="list-inline"><li class="day"><b>199</b><span>Days</span></li><li class="hours"><b>19</b><span>Hours</span></li><li class="mins"><b>18</b><span>Min</span></li><li class="seconds"><b>20</b><span>Sec</span></li></ul></div>

    <style></style>



    
    <a href="../collections/new-arrival/products/comfort-cushion.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p24_large588d.jpg?v=1550291740" alt="Comfort Cushion" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24.jpg?v=1550291740">
      <img src="/images/products/p24588d.jpg?v=1550291740" alt="Comfort Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p16.jpg?v=1550291741">
      <img src="/images/products/p162c71.jpg?v=1550291741" alt="Comfort Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="comfort-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006842432">                                    
        <input type="hidden" name="id" value="25424129228864">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/comfort-cushion.html" class="grid-link__title">Comfort Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$229.00" data-currency="INR">₹ 229.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$300.00" data-currency="INR">₹ 300.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204006842432"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204002222144">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/umang-pillow.html" class="grid-link">            




      <img src="/images/products/p5_large3619.jpg?v=1550291435" alt="Umang Pillow" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5.jpg?v=1550291435">
          <img src="/images/products/p53619.jpg?v=1550291435" alt="Umang Pillow">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p8.jpg?v=1550291436">
      <img src="/images/products/p8228a.jpg?v=1550291436" alt="Umang Pillow">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="umang-pillow" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204002222144">                                    
        <input type="hidden" name="id" value="25423989801024">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/umang-pillow.html" class="grid-link__title">Umang Pillow</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$225.00" data-currency="INR">₹ 225.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204002222144"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204001075264">

 <div class="products product-hover-15">
  <div class="product-container ">  












    <label class="deal-lable">Hurry, Only few item(s) left!</label>


    <div class="deal-clock lof-clock-2204001075264-detail"><ul class="list-inline"><li class="day"><b>199</b><span>Days</span></li><li class="hours"><b>19</b><span>Hours</span></li><li class="mins"><b>18</b><span>Min</span></li><li class="seconds"><b>20</b><span>Sec</span></li></ul></div>

    <style></style>



    
    <a href="../collections/new-arrival/products/standard-microfibre-pillow.html" class="grid-link">            




      <img src="/images/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447_large11c7.jpg?v=1550231484" alt="Standard Microfibre Pillow" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447.jpg?v=1550231484">
          <img src="/images/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e444711c7.jpg?v=1550231484" alt="Standard Microfibre Pillow">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6.jpg?v=1550231484">
      <img src="/images/products/p611c7.jpg?v=1550231484" alt="Standard Microfibre Pillow">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="standard-microfibre-pillow" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001075264">                                    
        <input type="hidden" name="id" value="25393193484352">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/standard-microfibre-pillow.html" class="grid-link__title">Standard Microfibre Pillow</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204001075264"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item active" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204000845888">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/polyester-window-curtain.html" class="grid-link">            




      <img src="/images/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075_large5cc5.jpg?v=1550304855" alt="Polyester Window Curtain" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075.jpg?v=1550304855">
          <img src="/images/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe90360755cc5.jpg?v=1550304855" alt="Polyester Window Curtain">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d.jpg?v=1550304855">
      <img src="/images/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d5cc5.jpg?v=1550304855" alt="Polyester Window Curtain">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="polyester-window-curtain" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000845888">                                    
        <input type="hidden" name="id" value="25429378760768">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/polyester-window-curtain.html" class="grid-link__title">Polyester Window Curtain</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$328.00" data-currency="INR">₹ 328.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204000845888"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2203998945344">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/memory-foam-mattress-1.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d_large64bc.jpg?v=1550312267" alt="Memory Foam Mattress" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d.jpg?v=1550312267">
      <img src="/images/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d64bc.jpg?v=1550312267" alt="Memory Foam Mattress">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e.jpg?v=1550312268">
      <img src="/images/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e9d04.jpg?v=1550312268" alt="Memory Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="memory-foam-mattress-1" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203998945344">                                    
        <input type="hidden" name="id" value="25433818038336">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/memory-foam-mattress-1.html" class="grid-link__title">Memory Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$400.00" data-currency="INR">₹ 400.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2203998945344"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item active" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204003729472">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/coir-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p13_largec1a5.jpg?v=1550311707" alt="Coir Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p13.jpg?v=1550311707">
          <img src="/images/products/p13c1a5.jpg?v=1550311707" alt="Coir Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90.jpg?v=1550311708">
      <img src="/images/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90b8da.jpg?v=1550311708" alt="Coir Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="coir-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204003729472">                                    
        <input type="hidden" name="id" value="25433112674368">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/coir-foam-mattress.html" class="grid-link__title">Coir Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$549.00" data-currency="INR">₹ 549.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204003729472"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204004417600">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/single-foam-mattress.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758_largea56e.jpg?v=1550310807" alt="Single Foam Mattress" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758.jpg?v=1550310807">
      <img src="/images/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758a56e.jpg?v=1550310807" alt="Single Foam Mattress">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11.jpg?v=1550310807">
      <img src="/images/products/p11a56e.jpg?v=1550310807" alt="Single Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="single-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204004417600">                                    
        <input type="hidden" name="id" value="25432331812928">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/single-foam-mattress.html" class="grid-link__title">Single Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$139.00" data-currency="INR">₹ 139.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$500.00" data-currency="INR">₹ 500.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204004417600"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item active" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204002680896">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/cooling-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37f_largefc4e.jpg?v=1550309434" alt="Cooling Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37f.jpg?v=1550309434">
          <img src="/images/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37ffc4e.jpg?v=1550309434" alt="Cooling Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p10.jpg?v=1550309434">
      <img src="/images/products/p10fc4e.jpg?v=1550309434" alt="Cooling Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="cooling-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204002680896">                                    
        <input type="hidden" name="id" value="25431018897472">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/cooling-foam-mattress.html" class="grid-link__title">Cooling Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$738.00" data-currency="INR">₹ 738.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204002680896"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2203999436864">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/on-ear-headphones-black.html" class="grid-link">            




      <img src="/images/products/p7_largef069.jpg?v=1550308200" alt="Spring Queen Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7.jpg?v=1550308200">
          <img src="/images/products/p7f069.jpg?v=1550308200" alt="Spring Queen Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12.jpg?v=1550308200">
      <img src="/images/products/p12f069.jpg?v=1550308200" alt="Spring Queen Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="on-ear-headphones-black" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203999436864">                                    
        <input type="hidden" name="id" value="25430662250560">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/on-ear-headphones-black.html" class="grid-link__title">Spring Queen Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$539.00" data-currency="INR">₹ 539.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2203999436864"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item active" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204006449216">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/plain-blanket.html" class="grid-link">            




      <img src="/images/products/p4_large28ef.jpg?v=1550307794" alt="Plain Blanket" class="featured-image">
  </a>



  <div class="product_right_tag  ">


  </div>
  <div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="plain-blanket" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006449216">                                    
        <input type="hidden" name="id" value="25430470262848">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/plain-blanket.html" class="grid-link__title">Plain Blanket</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$386.00" data-currency="INR">₹ 386.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204006449216"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204005466176">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/eyelet-polyster-curtain.html" class="grid-link">            




      <img src="/images/products/p19_large6ac6.jpg?v=1550303828" alt="Eyelet Polyster Curtain" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p19.jpg?v=1550303828">
          <img src="/images/products/p196ac6.jpg?v=1550303828" alt="Eyelet Polyster Curtain">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21.jpg?v=1550303829">
      <img src="/images/products/p21b542.jpg?v=1550303829" alt="Eyelet Polyster Curtain">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="eyelet-polyster-curtain" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204005466176">                                    
        <input type="hidden" name="id" value="25428997734464">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/eyelet-polyster-curtain.html" class="grid-link__title">Eyelet Polyster Curtain</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$225.00" data-currency="INR">₹ 225.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204005466176"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204000419904">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/door-curtain-set.html" class="grid-link">            




      <img src="/images/products/p23_09773fe5-d553-4e82-af31-0a00621daa63_large1612.jpg?v=1550302498" alt="Door Curtain Set" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498">
          <img src="/images/products/p23_09773fe5-d553-4e82-af31-0a00621daa631612.jpg?v=1550302498" alt="Door Curtain Set">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18.jpg?v=1550302500">
      <img src="/images/products/p18757d.jpg?v=1550302500" alt="Door Curtain Set">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="door-curtain-set" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000419904">                                    
        <input type="hidden" name="id" value="25428605665344">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/door-curtain-set.html" class="grid-link__title">Door Curtain Set</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$219.00" data-currency="INR">₹ 219.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204000419904"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204009070656">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/plain-crush-curtains.html" class="grid-link">            




      <img src="/images/products/p22_large5f29.jpg?v=1550301918" alt="Plain Crush Curtains" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22.jpg?v=1550301918">
          <img src="/images/products/p225f29.jpg?v=1550301918" alt="Plain Crush Curtains">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23.jpg?v=1550301919">
      <img src="/images/products/p23567e.jpg?v=1550301919" alt="Plain Crush Curtains">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="plain-crush-curtains" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204009070656">                                    
        <input type="hidden" name="id" value="25428327071808">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/plain-crush-curtains.html" class="grid-link__title">Plain Crush Curtains</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204009070656"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204006023232">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/orthopaedic-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p20_70ac1869-484b-4ec7-815c-ef5ddb38635e_largeb993.jpg?v=1550301055" alt="Orthopaedic Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p20_70ac1869-484b-4ec7-815c-ef5ddb38635e.jpg?v=1550301055">
          <img src="/images/products/p20_70ac1869-484b-4ec7-815c-ef5ddb38635eb993.jpg?v=1550301055" alt="Orthopaedic Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p2_15c4e4b2-6f79-4b18-8cfe-ed358892315a.jpg?v=1550301055">
      <img src="/images/products/p2_15c4e4b2-6f79-4b18-8cfe-ed358892315ab993.jpg?v=1550301055" alt="Orthopaedic Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="orthopaedic-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006023232">                                    
        <input type="hidden" name="id" value="25427980976192">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/orthopaedic-foam-mattress.html" class="grid-link__title">Orthopaedic Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$743.00" data-currency="INR">₹ 743.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204006023232"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204007694400">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/dual-comfort-mattress.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p3_7f396e1f-7041-4830-aaed-4a7e044fc998_large4834.jpg?v=1550300430" alt="Dual Comfort Mattress" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p3_7f396e1f-7041-4830-aaed-4a7e044fc998.jpg?v=1550300430">
      <img src="/images/products/p3_7f396e1f-7041-4830-aaed-4a7e044fc9984834.jpg?v=1550300430" alt="Dual Comfort Mattress">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p20.jpg?v=1550300430">
      <img src="/images/products/p204834.jpg?v=1550300430" alt="Dual Comfort Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="dual-comfort-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204007694400">                                    
        <input type="hidden" name="id" value="25427683475520">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/dual-comfort-mattress.html" class="grid-link__title">Dual Comfort Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$228.00" data-currency="INR">₹ 228.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$400.00" data-currency="INR">₹ 400.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204007694400"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204008546368">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/memory-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p2_large0e88.jpg?v=1550299516" alt="Memory Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p2.jpg?v=1550299516">
          <img src="/images/products/p20e88.jpg?v=1550299516" alt="Memory Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p3.jpg?v=1550299517">
      <img src="/images/products/p3112f.jpg?v=1550299517" alt="Memory Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="memory-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204008546368">                                    
        <input type="hidden" name="id" value="25427444760640">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/memory-foam-mattress.html" class="grid-link__title">Memory Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$534.00" data-currency="INR">₹ 534.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204008546368"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2203998322752">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/hand-made-cushion.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8_largee3cd.jpg?v=1550298962" alt="Hand Made Cushion" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8.jpg?v=1550298962">
      <img src="/images/products/p25_ff2db170-9797-467a-96bd-7bd92c5d28c8e3cd.jpg?v=1550298962" alt="Hand Made Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6_04d3f889-b5a1-4910-95fd-b0d401368dae.jpg?v=1550298963">
      <img src="/images/products/p6_04d3f889-b5a1-4910-95fd-b0d401368dae3c41.jpg?v=1550298963" alt="Hand Made Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="hand-made-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203998322752">                                    
        <input type="hidden" name="id" value="25427285639232">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/hand-made-cushion.html" class="grid-link__title">Hand Made Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$500.00" data-currency="INR">₹ 500.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2203998322752"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2203997732928">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/throw-pillow-cushion.html" class="grid-link">            




      <img src="/images/products/p27_026f58da-16a0-40b3-8266-4dd698b5ef08_large7fdb.jpg?v=1550298406" alt="Throw Pillow Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27_026f58da-16a0-40b3-8266-4dd698b5ef08.jpg?v=1550298406">
          <img src="/images/products/p27_026f58da-16a0-40b3-8266-4dd698b5ef087fdb.jpg?v=1550298406" alt="Throw Pillow Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25.jpg?v=1550298406">
      <img src="/images/products/p257fdb.jpg?v=1550298406" alt="Throw Pillow Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="throw-pillow-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203997732928">                                    
        <input type="hidden" name="id" value="25427127107648">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/throw-pillow-cushion.html" class="grid-link__title">Throw Pillow Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$769.00" data-currency="INR">₹ 769.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2203997732928"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2203997175872">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/h151-headphones.html" class="grid-link">            




      <img src="/images/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94_large7817.jpg?v=1550297939" alt="Cotton Canvas Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc94.jpg?v=1550297939">
          <img src="/images/products/p17_290efa2d-7584-4494-b663-e4d0df5fdc947817.jpg?v=1550297939" alt="Cotton Canvas Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27.jpg?v=1550297939">
      <img src="/images/products/p277817.jpg?v=1550297939" alt="Cotton Canvas Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="h151-headphones" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203997175872">                                    
        <input type="hidden" name="id" value="25426986664000">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/h151-headphones.html" class="grid-link__title">Cotton Canvas Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2203997175872"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204001599552">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/backrest-cushion.html" class="grid-link">            




      <img src="/images/products/p26_large164d.jpg?v=1550292849" alt="Backrest Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p26.jpg?v=1550292849">
          <img src="/images/products/p26164d.jpg?v=1550292849" alt="Backrest Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17.jpg?v=1550292850">
      <img src="/images/products/p17830c.jpg?v=1550292850" alt="Backrest Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="backrest-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001599552">                                    
        <input type="hidden" name="id" value="25424764534848">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/backrest-cushion.html" class="grid-link__title">Backrest Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$499.00" data-currency="INR">₹ 499.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204001599552"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204005007424">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/micro-fibre-cushion.html" class="grid-link">            




      <img src="/images/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993_large561a.jpg?v=1550292544" alt="Micro Fibre Cushion" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993.jpg?v=1550292544">
          <img src="/images/products/p15_82612dd0-9ca0-4d88-91f9-ed09a1aeb993561a.jpg?v=1550292544" alt="Micro Fibre Cushion">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_ef4f28be-6c20-4c97-b1ca-4b7387ff3ceb.jpg?v=1550292544">
      <img src="/images/products/p24_ef4f28be-6c20-4c97-b1ca-4b7387ff3ceb561a.jpg?v=1550292544" alt="Micro Fibre Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="micro-fibre-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204005007424">                                    
        <input type="hidden" name="id" value="25424443768896">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/micro-fibre-cushion.html" class="grid-link__title">Micro Fibre Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204005007424"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204007137344">

 <div class="products product-hover-15">
  <div class="product-container ">  









    <label class="deal-lable">Hurry, Only few item(s) left!</label>


    <div class="deal-clock lof-clock-2204007137344-detail"><ul class="list-inline"><li class="day"><b>199</b><span>Days</span></li><li class="hours"><b>19</b><span>Hours</span></li><li class="mins"><b>18</b><span>Min</span></li><li class="seconds"><b>21</b><span>Sec</span></li></ul></div>

    <style></style>



    
    <a href="../collections/new-arrival/products/cotton-plan-cushion.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p16_38a12388-a625-4040-a226-5f7a0132c079_large1508.jpg?v=1557811918" alt="Cotton Plan Cushion" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 360px; transform: translate3d(0px, 0px, 0px);">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p16_38a12388-a625-4040-a226-5f7a0132c079.jpg?v=1557811918">
      <img src="/images/products/p16_38a12388-a625-4040-a226-5f7a0132c0791508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p27_grande_3a9009ff-6e78-477d-bfd2-0166db5f801d.jpg?v=1557811918">
      <img src="/images/products/p27_grande_3a9009ff-6e78-477d-bfd2-0166db5f801d1508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p17_grande_c00837cd-31d1-4603-87ab-02e38fe176e2.jpg?v=1557811918">
      <img src="/images/products/p17_grande_c00837cd-31d1-4603-87ab-02e38fe176e21508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24_grande_1bebf316-7241-49fc-b00f-85b067d30812.jpg?v=1557811918">
      <img src="/images/products/p24_grande_1bebf316-7241-49fc-b00f-85b067d308121508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p25_grande_2011e86e-1567-4d8f-8969-7adde35fa695.jpg?v=1557811918">
      <img src="/images/products/p25_grande_2011e86e-1567-4d8f-8969-7adde35fa6951508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p15.jpg?v=1557811918">
      <img src="/images/products/p151508.jpg?v=1557811918" alt="Cotton Plan Cushion">
  </a>
</li>


</ul><div class="lSAction"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="cotton-plan-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204007137344">                                    
        <input type="hidden" name="id" value="25424256434240">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/cotton-plan-cushion.html" class="grid-link__title">Cotton Plan Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$123.00" data-currency="INR">₹ 123.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$1,000.00" data-currency="INR">₹ 1,000.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204007137344"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204006842432">

 <div class="products product-hover-15">
  <div class="product-container ">  






    <label class="deal-lable">Hurry, Only few item(s) left!</label>


    <div class="deal-clock lof-clock-2204006842432-detail"><ul class="list-inline"><li class="day"><b>199</b><span>Days</span></li><li class="hours"><b>19</b><span>Hours</span></li><li class="mins"><b>18</b><span>Min</span></li><li class="seconds"><b>21</b><span>Sec</span></li></ul></div>

    <style></style>



    
    <a href="../collections/new-arrival/products/comfort-cushion.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p24_large588d.jpg?v=1550291740" alt="Comfort Cushion" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p24.jpg?v=1550291740">
      <img src="/images/products/p24588d.jpg?v=1550291740" alt="Comfort Cushion">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p16.jpg?v=1550291741">
      <img src="/images/products/p162c71.jpg?v=1550291741" alt="Comfort Cushion">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="comfort-cushion" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006842432">                                    
        <input type="hidden" name="id" value="25424129228864">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/comfort-cushion.html" class="grid-link__title">Comfort Cushion</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$229.00" data-currency="INR">₹ 229.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$300.00" data-currency="INR">₹ 300.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204006842432"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204002222144">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/umang-pillow.html" class="grid-link">            




      <img src="/images/products/p5_large3619.jpg?v=1550291435" alt="Umang Pillow" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p5.jpg?v=1550291435">
          <img src="/images/products/p53619.jpg?v=1550291435" alt="Umang Pillow">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p8.jpg?v=1550291436">
      <img src="/images/products/p8228a.jpg?v=1550291436" alt="Umang Pillow">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="umang-pillow" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204002222144">                                    
        <input type="hidden" name="id" value="25423989801024">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/umang-pillow.html" class="grid-link__title">Umang Pillow</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$225.00" data-currency="INR">₹ 225.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204002222144"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204001075264">

 <div class="products product-hover-15">
  <div class="product-container ">  












    <label class="deal-lable">Hurry, Only few item(s) left!</label>


    <div class="deal-clock lof-clock-2204001075264-detail"><ul class="list-inline"><li class="day"><b>199</b><span>Days</span></li><li class="hours"><b>19</b><span>Hours</span></li><li class="mins"><b>18</b><span>Min</span></li><li class="seconds"><b>20</b><span>Sec</span></li></ul></div>

    <style></style>



    
    <a href="../collections/new-arrival/products/standard-microfibre-pillow.html" class="grid-link">            




      <img src="/images/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447_large11c7.jpg?v=1550231484" alt="Standard Microfibre Pillow" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e4447.jpg?v=1550231484">
          <img src="/images/products/p1_c7924cf9-cdf5-4c3a-a55a-848f9e8e444711c7.jpg?v=1550231484" alt="Standard Microfibre Pillow">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p6.jpg?v=1550231484">
      <img src="/images/products/p611c7.jpg?v=1550231484" alt="Standard Microfibre Pillow">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="standard-microfibre-pillow" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204001075264">                                    
        <input type="hidden" name="id" value="25393193484352">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/standard-microfibre-pillow.html" class="grid-link__title">Standard Microfibre Pillow</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204001075264"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204000845888">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/polyester-window-curtain.html" class="grid-link">            




      <img src="/images/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075_large5cc5.jpg?v=1550304855" alt="Polyester Window Curtain" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe9036075.jpg?v=1550304855">
          <img src="/images/products/p21_05bfbbb8-9e4e-4357-9109-ff4fe90360755cc5.jpg?v=1550304855" alt="Polyester Window Curtain">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d.jpg?v=1550304855">
      <img src="/images/products/p22_f1353b00-992f-4b56-bf2a-d5c3db2b670d5cc5.jpg?v=1550304855" alt="Polyester Window Curtain">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="polyester-window-curtain" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000845888">                                    
        <input type="hidden" name="id" value="25429378760768">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/polyester-window-curtain.html" class="grid-link__title">Polyester Window Curtain</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$328.00" data-currency="INR">₹ 328.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204000845888"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2203998945344">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/memory-foam-mattress-1.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d_large64bc.jpg?v=1550312267" alt="Memory Foam Mattress" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d.jpg?v=1550312267">
      <img src="/images/products/p11_b3448f3e-fe55-4dfb-835c-5869447aa03d64bc.jpg?v=1550312267" alt="Memory Foam Mattress">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e.jpg?v=1550312268">
      <img src="/images/products/p7_49de7977-c6f4-404e-8a7a-87500ba4cb4e9d04.jpg?v=1550312268" alt="Memory Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="memory-foam-mattress-1" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203998945344">                                    
        <input type="hidden" name="id" value="25433818038336">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/memory-foam-mattress-1.html" class="grid-link__title">Memory Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$400.00" data-currency="INR">₹ 400.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2203998945344"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204003729472">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/coir-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p13_largec1a5.jpg?v=1550311707" alt="Coir Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p13.jpg?v=1550311707">
          <img src="/images/products/p13c1a5.jpg?v=1550311707" alt="Coir Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90.jpg?v=1550311708">
      <img src="/images/products/p11_eb2b6799-6c9a-4f5b-9b29-2fb9b736fa90b8da.jpg?v=1550311708" alt="Coir Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="coir-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204003729472">                                    
        <input type="hidden" name="id" value="25433112674368">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/coir-foam-mattress.html" class="grid-link__title">Coir Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$549.00" data-currency="INR">₹ 549.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204003729472"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row       on-sale" id="product-2204004417600">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/single-foam-mattress.html" class="grid-link">            


      <div class="featured-tag">
        <span class="badge badge--sale">          
          <span class="gift-tag badge__text">Sale</span>
      </span>
  </div>



  <img src="/images/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758_largea56e.jpg?v=1550310807" alt="Single Foam Mattress" class="featured-image">
</a>


<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

  <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758.jpg?v=1550310807">
      <img src="/images/products/p9_89dbfc98-54d9-4307-bb95-5cc968ab3758a56e.jpg?v=1550310807" alt="Single Foam Mattress">
  </a>
</li>

<li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p11.jpg?v=1550310807">
      <img src="/images/products/p11a56e.jpg?v=1550310807" alt="Single Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag   offer_exist ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="single-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204004417600">                                    
        <input type="hidden" name="id" value="25432331812928">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/single-foam-mattress.html" class="grid-link__title">Single Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$139.00" data-currency="INR">₹ 139.00</span></span>
    </div>
</div>

<del class="grid-link__sale_price" id="ComparePrice">
  <span><span class="money" data-currency-usd="$500.00" data-currency="INR">₹ 500.00</span></span></del>




  <span class="shopify-product-reviews-badge" data-id="2204004417600"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204002680896">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/cooling-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37f_largefc4e.jpg?v=1550309434" alt="Cooling Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37f.jpg?v=1550309434">
          <img src="/images/products/p12_dc66e8c8-2230-416f-8a39-3fffa67fe37ffc4e.jpg?v=1550309434" alt="Cooling Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p10.jpg?v=1550309434">
      <img src="/images/products/p10fc4e.jpg?v=1550309434" alt="Cooling Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="cooling-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204002680896">                                    
        <input type="hidden" name="id" value="25431018897472">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/cooling-foam-mattress.html" class="grid-link__title">Cooling Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$738.00" data-currency="INR">₹ 738.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204002680896"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2203999436864">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/on-ear-headphones-black.html" class="grid-link">            




      <img src="/images/products/p7_largef069.jpg?v=1550308200" alt="Spring Queen Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p7.jpg?v=1550308200">
          <img src="/images/products/p7f069.jpg?v=1550308200" alt="Spring Queen Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p12.jpg?v=1550308200">
      <img src="/images/products/p12f069.jpg?v=1550308200" alt="Spring Queen Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="on-ear-headphones-black" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2203999436864">                                    
        <input type="hidden" name="id" value="25430662250560">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/on-ear-headphones-black.html" class="grid-link__title">Spring Queen Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$539.00" data-currency="INR">₹ 539.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2203999436864"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204006449216">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/plain-blanket.html" class="grid-link">            




      <img src="/images/products/p4_large28ef.jpg?v=1550307794" alt="Plain Blanket" class="featured-image">
  </a>



  <div class="product_right_tag  ">


  </div>
  <div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="plain-blanket" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006449216">                                    
        <input type="hidden" name="id" value="25430470262848">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/plain-blanket.html" class="grid-link__title">Plain Blanket</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$386.00" data-currency="INR">₹ 386.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204006449216"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204005466176">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/eyelet-polyster-curtain.html" class="grid-link">            




      <img src="/images/products/p19_large6ac6.jpg?v=1550303828" alt="Eyelet Polyster Curtain" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p19.jpg?v=1550303828">
          <img src="/images/products/p196ac6.jpg?v=1550303828" alt="Eyelet Polyster Curtain">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p21.jpg?v=1550303829">
      <img src="/images/products/p21b542.jpg?v=1550303829" alt="Eyelet Polyster Curtain">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="eyelet-polyster-curtain" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204005466176">                                    
        <input type="hidden" name="id" value="25428997734464">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/eyelet-polyster-curtain.html" class="grid-link__title">Eyelet Polyster Curtain</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$225.00" data-currency="INR">₹ 225.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204005466176"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204000419904">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/door-curtain-set.html" class="grid-link">            




      <img src="/images/products/p23_09773fe5-d553-4e82-af31-0a00621daa63_large1612.jpg?v=1550302498" alt="Door Curtain Set" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498">
          <img src="/images/products/p23_09773fe5-d553-4e82-af31-0a00621daa631612.jpg?v=1550302498" alt="Door Curtain Set">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p18.jpg?v=1550302500">
      <img src="/images/products/p18757d.jpg?v=1550302500" alt="Door Curtain Set">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="door-curtain-set" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204000419904">                                    
        <input type="hidden" name="id" value="25428605665344">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/door-curtain-set.html" class="grid-link__title">Door Curtain Set</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$219.00" data-currency="INR">₹ 219.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204000419904"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204009070656">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/plain-crush-curtains.html" class="grid-link">            




      <img src="/images/products/p22_large5f29.jpg?v=1550301918" alt="Plain Crush Curtains" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p22.jpg?v=1550301918">
          <img src="/images/products/p225f29.jpg?v=1550301918" alt="Plain Crush Curtains">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23.jpg?v=1550301919">
      <img src="/images/products/p23567e.jpg?v=1550301919" alt="Plain Crush Curtains">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="plain-crush-curtains" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204009070656">                                    
        <input type="hidden" name="id" value="25428327071808">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/plain-crush-curtains.html" class="grid-link__title">Plain Crush Curtains</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$329.00" data-currency="INR">₹ 329.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204009070656"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div><div class="owl-item cloned" style="width: 285px;"><li class="grid__item swiper-slide item-row      " id="product-2204006023232">

 <div class="products product-hover-15">
  <div class="product-container ">  





















    <a href="../collections/new-arrival/products/orthopaedic-foam-mattress.html" class="grid-link">            




      <img src="/images/products/p20_70ac1869-484b-4ec7-815c-ef5ddb38635e_largeb993.jpg?v=1550301055" alt="Orthopaedic Foam Mattress" class="featured-image">
  </a>


  <div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">

      <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
        <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p20_70ac1869-484b-4ec7-815c-ef5ddb38635e.jpg?v=1550301055">
          <img src="/images/products/p20_70ac1869-484b-4ec7-815c-ef5ddb38635eb993.jpg?v=1550301055" alt="Orthopaedic Foam Mattress">
      </a>
  </li>

  <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
    <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p2_15c4e4b2-6f79-4b18-8cfe-ed358892315a.jpg?v=1550301055">
      <img src="/images/products/p2_15c4e4b2-6f79-4b18-8cfe-ed358892315ab993.jpg?v=1550301055" alt="Orthopaedic Foam Mattress">
  </a>
</li>


</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>


<div class="product_right_tag  ">


</div>
<div class="ImageWrapper">
    <div class="product-button"> 

      <a href="javascript:void(0)" id="orthopaedic-foam-mattress" class="quick-view-text" title="Quick View">                      
        <b class="far fa-eye"></b>
        <i>   Quick View</i>
    </a>       




    <form action="https://softie-demo.myshopify.com/cart/add" method="post" class="gom variants clearfix" id="cart-form-2204006023232">                                    
        <input type="hidden" name="id" value="25427980976192">  
        <a class="add-cart-btn" title="Add to Cart">
          <b class="fas fa-shopping-cart"></b>
          <i>  Add to Cart</i>
      </a>
  </form>  


</div>
</div>

</div>
<div class="product-detail">
  <a href="../collections/new-arrival/products/orthopaedic-foam-mattress.html" class="grid-link__title">Orthopaedic Foam Mattress</a> 
  <div class="grid-link__meta">



    <div class="product_price">          
      <div class="grid-link__org_price" id="ProductPrice">
        <span> <span class="money" data-currency-usd="$743.00" data-currency="INR">₹ 743.00</span></span>
    </div>
</div>




<span class="shopify-product-reviews-badge" data-id="2204006023232"></span> 
</div>

<ul class="item-swatch color_swatch_Value">  





</ul>



</div>


</div>

<style>
  .product-container.product-hover-15 {position: relative;}
  .lSSlideOuter.vertical {
    float: right;
    width: 20%;
    position: absolute;
    top: 20px;
    right: -100%;
}

.thumbs_items a {display:block;}
</style>


</li></div></div></div><div class="owl-dots disabled"></div></ul>
<div class="nav_featured"><div class="owl-prev"><a class="prev"><i class="btn fas fa-angle-left"></i></a></div><div class="owl-next"><a class="next"><i class="btn fas fa-angle-right"></i></a></div></div>
</div>
</div>


<style>


</style>

<script type="text/javascript">
  $(document).ready(function(){
      var related = $(".related-products");
      related.owlCarousel({
        loop:true,       
        nav:true,
        navContainer: ".nav_featured",
        navText: ['<a class="prev"><i class="btn fas fa-angle-left"></i></a>','<a class="next"><i class="btn fas fa-angle-right"></i></a>'],
        dots: false,
        responsive:{
            0:{
              items:2
          },
          968:{
            items:3
        },
        1000:{
          items: 4
      }
  }
});
  });

</script>


<script src="../../ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
<div class="dt-sc-hr-invisible-small"></div>

<section class="recently-viewed-products">

  <div class="section-header section-header--small">
    <div class="border-title">

        <h3>Recently Viewed Products</h3>    

    </div>
</div>

<div class="products-grid" id="recently-viewed-products-grid">
</div>
</section>



<script id="recently-viewed-product-grid-template" type="text/x-jquery-tmpl">

    {{-- <div class="grid__item wide--one-quarter post-large--one-quarter large--one-third medium--one-half">
      <div id="product-${id}" class="inner product-item if !available}} sold-out {{/if}}{{if compare_at_price_min > price_min}} on-sale {{/if}}">
        <div class="inner-top products">
          <div class="product-top">
            <div class="product-image">
              <a href="${url}" class="product-grid-image">
                <img src="${Shopify.Products.resizeImage(featured_image, "master")}" alt="${featured_image.alt}"/>

            </a>
        </div>
        
        {{if compare_at_price_min > price_min || !available}}
        <div class="product-label">
          {{if compare_at_price_min > price_min}} 
          <strong class="label"> Sale</strong>
          {{/if}}
          {{if !available}}
          <strong class="sold-out-label"> >Sold Out</strong>
          {{/if}}
      </div>
      {{/if}}
  </div>
  
  <div class="product-bottom product-detail">


      <a class="product-title grid-link__title" href="${url}">${title} </a>





      <div class="price-box grid-link__meta">   
        {{if compare_at_price_min > price_min}} 
        <p class="sale">
            <span class="special-price grid-link__org_price">{{if price_varies}}{{/if}}{{html Shopify.formatMoney(price_min, window.money_format)}}</span>
            <del class="old-price grid-link__sale_price"> {{html Shopify.formatMoney(compare_at_price_min, window.money_format)}}</del>


        </p>
        {{else}}
        <p class="regular-product">
            <span class="special-price grid-link__org_price">{{if price_varies}}{{/if}}{{html Shopify.formatMoney(price_min, window.money_format)}}</span>
        </p>
        {{/if}}
    </div>
    <div class="action">
      <form action="/cart/add" method="post" class="variants" id="product-actions-${id}" enctype="multipart/form-data" style="padding:0px;">    
        {{if !available}} 
        <input class="btn add-to-cart-btn" type="submit"  value="Unavailable" disabled="disabled"/>
        {{else variants.length > 1 }}
        <input class="btn" type="button" onclick="window.location.href='${url}'"  value="Select options"/>
        {{else}}
        <input type="hidden" name="id" value="${variants[0].id}" />      
        <input class="btn add-to-cart-btn" type="submit"  value="Add to Cart"/>
        {{/if}}
    </form>
</div>    
</div>
</div>
</div>
</div> --}}
</script>


<script>

/**
 * Module to show Recently Viewed Products
 *
 * Copyright (c) 2014 Caroline Schnapp (11heavens.com)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
 Shopify.Products=function(){var e={howManyToShow:3,howManyToStoreInMemory:10,wrapperId:"recently-viewed-products",templateId:"recently-viewed-product-template",onComplete:null};var t=[];var n=null;var r=null;var i=0;var s={configuration:{expires:90,path:"/",domain:window.location.hostname},name:"shopify_recently_viewed",write:function(e){jQuery.cookie(this.name,e.join(" "),this.configuration)},read:function(){var e=[];var t=jQuery.cookie(this.name);if(t!==null&&t!=undefined){e=t.split(" ")}return e},destroy:function(){jQuery.cookie(this.name,null,this.configuration)},remove:function(e){var t=this.read();var n=jQuery.inArray(e,t);if(n!==-1){t.splice(n,1);this.write(t)}}};var o=function(){n.show();if(e.onComplete){try{e.onComplete()}catch(t){}}};var u=function(){if(t.length&&i<e.howManyToShow){jQuery.ajax({dataType:"json",url:"/products/"+t[0]+".js",cache:false,success:function(e){r.tmpl(e).appendTo(n);t.shift();i++;u()},error:function(){s.remove(t[0]);t.shift();u()}})}else{o()}};return{resizeImage:function(e,t){if(t==null){return e}if(t=="master"){return e.replace(/http(s)?:/,"")}var n=e.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?/i);if(n!=null&&n!=undefined){var r=e.split(n[0]);var i=n[0];return(r[0]+"_"+t+i).replace(/http(s)?:/,"")}else{return null}},showRecentlyViewed:function(i){var i=i||{};jQuery.extend(e,i);t=s.read();r=jQuery("#"+e.templateId);n=jQuery("#"+e.wrapperId);e.howManyToShow=Math.min(t.length,e.howManyToShow);if(e.howManyToShow&&r.length&&n.length){u()}},getConfig:function(){return e},clearList:function(){s.destroy()},recordRecentlyViewed:function(t){var t=t||{};jQuery.extend(e,t);var n=s.read();if(window.location.pathname.indexOf("index.html")!==-1){var r=window.location.pathname.match(/\/products\/([a-z0-9\-]+)/)[1];var i=jQuery.inArray(r,n);if(i===-1){n.unshift(r);n=n.splice(0,e.howManyToStoreInMemory)}else{n.splice(i,1);n.unshift(r)}s.write(n)}}}}()


 Shopify.Products.showRecentlyViewed({ 
  howManyToShow: 8, 
  wrapperId: 'recently-viewed-products-grid', 
  templateId: 'recently-viewed-product-grid-template',
  onComplete: function() {
      if (jQuery("#recently-viewed-products-grid").children().length > 0) {
          jQuery(".recently-viewed-products").show();  
      }
  }
});
</script>



</div>
</div>

<script src="../../cdn.shopify.com/s/assets/themes_support/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js" type="text/javascript"></script>
<script src="../../cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/jquery.fancybox9be8.js?v=4103147835157344137" type="text/javascript"></script>

<script>
  var selectCallback = function(variant, selector) {
    timber.productPage({
      money_format: "<span class=money>${{10}}</span>",
      variant: variant,
      selector: selector,
      translations: {
          add_to_cart : "Add to Cart",
          sold_out : "Sold Out",
          unavailable : "Unavailable"
      }
  });
};

jQuery(function($) {
    new Shopify.OptionSelectors('productSelect', {
      product: {"id":2203996028992,"title":"Cotton Striped Pillows","handle":"cotton-striped-pillows","description":"\u003cp\u003eNam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo pharetras loremos.Donec pretium egestas sapien et mollis.\u003c\/p\u003e\n\u003ch4\u003eSample Unordered List\u003c\/h4\u003e\n\u003cul\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous\u003c\/li\u003e\n\u003c\/ul\u003e\n\u003ch4\u003eSample Ordered Lista\u003c\/h4\u003e\n\u003col\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis.\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous.\u003c\/li\u003e\n\u003c\/ol\u003e\n\u003ch4\u003eSample Paragraph Text\u003c\/h4\u003e\n\u003cblockquote\u003ePraesent vestibulum congue tellus at fringilla. Curabitur vitae semper sem, eu convallis est. Cras felis nunc commodo eu convallis vitae interdum non nisl. Maecenas ac est sit amet augue pharetra convallis nec danos dui. Cras suscipit quam et turpis eleifend vitae malesuada magna congue. Damus id ullamcorper neque. Sed vitae mi a mi pretium aliquet ac sed elit. Pellentesque nulla eros accumsan quis justo at tincidunt lobortis denimes loremous. Suspendisse vestibulum lectus in lectus volutpat, ut dapibus purus pulvinar. Vestibulum sit amet auctor ipsum.\u003c\/blockquote\u003e","published_at":"2019-01-06T18:02:40-12:00","created_at":"2019-01-06T18:02:42-12:00","vendor":"Recron","type":"Pillow","tags":["$300 - $500","4","Cotton","Pillow","Recron"],"price":32900,"price_min":32900,"price_max":47900,"available":true,"price_varies":true,"compare_at_price":null,"compare_at_price_min":0,"compare_at_price_max":0,"compare_at_price_varies":false,"variants":[{"id":25391903211584,"title":"4 \/ Cotton","option1":"4","option2":"Cotton","option3":null,"sku":"","requires_shipping":true,"taxable":true,"featured_image":{"id":9383691583552,"product_id":2203996028992,"position":1,"created_at":"2019-02-14T23:19:09-12:00","updated_at":"2019-02-14T23:19:09-12:00","alt":null,"width":1000,"height":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1550229549","variant_ids":[25391903211584]},"available":true,"name":"Cotton Striped Pillows - 4 \/ Cotton","public_title":"4 \/ Cotton","options":["4","Cotton"],"price":32900,"weight":0,"compare_at_price":null,"inventory_management":null,"barcode":"","featured_media":{"alt":null,"id":4221264363584,"position":1,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1570462330"}}},{"id":25392885170240,"title":"6 \/ Cotton","option1":"6","option2":"Cotton","option3":null,"sku":"","requires_shipping":true,"taxable":true,"featured_image":{"id":9383691845696,"product_id":2203996028992,"position":2,"created_at":"2019-02-14T23:19:10-12:00","updated_at":"2019-02-14T23:19:10-12:00","alt":null,"width":1000,"height":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1550229550","variant_ids":[25392885170240]},"available":true,"name":"Cotton Striped Pillows - 6 \/ Cotton","public_title":"6 \/ Cotton","options":["6","Cotton"],"price":47900,"weight":0,"compare_at_price":null,"inventory_management":null,"barcode":"","featured_media":{"alt":null,"id":4221264396352,"position":2,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1570462330"}}}],"images":["\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1550229549","\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1550229550"],"featured_image":"\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1550229549","options":["Inch","Material"],"media":[{"alt":null,"id":4221264363584,"position":1,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1570462330"},"aspect_ratio":1.0,"height":1000,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1570462330","width":1000},{"alt":null,"id":4221264396352,"position":2,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1570462330"},"aspect_ratio":1.0,"height":1000,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1570462330","width":1000}],"content":"\u003cp\u003eNam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo pharetras loremos.Donec pretium egestas sapien et mollis.\u003c\/p\u003e\n\u003ch4\u003eSample Unordered List\u003c\/h4\u003e\n\u003cul\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous\u003c\/li\u003e\n\u003c\/ul\u003e\n\u003ch4\u003eSample Ordered Lista\u003c\/h4\u003e\n\u003col\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis.\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous.\u003c\/li\u003e\n\u003c\/ol\u003e\n\u003ch4\u003eSample Paragraph Text\u003c\/h4\u003e\n\u003cblockquote\u003ePraesent vestibulum congue tellus at fringilla. Curabitur vitae semper sem, eu convallis est. Cras felis nunc commodo eu convallis vitae interdum non nisl. Maecenas ac est sit amet augue pharetra convallis nec danos dui. Cras suscipit quam et turpis eleifend vitae malesuada magna congue. Damus id ullamcorper neque. Sed vitae mi a mi pretium aliquet ac sed elit. Pellentesque nulla eros accumsan quis justo at tincidunt lobortis denimes loremous. Suspendisse vestibulum lectus in lectus volutpat, ut dapibus purus pulvinar. Vestibulum sit amet auctor ipsum.\u003c\/blockquote\u003e"},
      onVariantSelected: selectCallback,
      enableHistoryState: true
  });




Shopify.linkOptionSelectors({"id":2203996028992,"title":"Cotton Striped Pillows","handle":"cotton-striped-pillows","description":"\u003cp\u003eNam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo pharetras loremos.Donec pretium egestas sapien et mollis.\u003c\/p\u003e\n\u003ch4\u003eSample Unordered List\u003c\/h4\u003e\n\u003cul\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous\u003c\/li\u003e\n\u003c\/ul\u003e\n\u003ch4\u003eSample Ordered Lista\u003c\/h4\u003e\n\u003col\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis.\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous.\u003c\/li\u003e\n\u003c\/ol\u003e\n\u003ch4\u003eSample Paragraph Text\u003c\/h4\u003e\n\u003cblockquote\u003ePraesent vestibulum congue tellus at fringilla. Curabitur vitae semper sem, eu convallis est. Cras felis nunc commodo eu convallis vitae interdum non nisl. Maecenas ac est sit amet augue pharetra convallis nec danos dui. Cras suscipit quam et turpis eleifend vitae malesuada magna congue. Damus id ullamcorper neque. Sed vitae mi a mi pretium aliquet ac sed elit. Pellentesque nulla eros accumsan quis justo at tincidunt lobortis denimes loremous. Suspendisse vestibulum lectus in lectus volutpat, ut dapibus purus pulvinar. Vestibulum sit amet auctor ipsum.\u003c\/blockquote\u003e","published_at":"2019-01-06T18:02:40-12:00","created_at":"2019-01-06T18:02:42-12:00","vendor":"Recron","type":"Pillow","tags":["$300 - $500","4","Cotton","Pillow","Recron"],"price":32900,"price_min":32900,"price_max":47900,"available":true,"price_varies":true,"compare_at_price":null,"compare_at_price_min":0,"compare_at_price_max":0,"compare_at_price_varies":false,"variants":[{"id":25391903211584,"title":"4 \/ Cotton","option1":"4","option2":"Cotton","option3":null,"sku":"","requires_shipping":true,"taxable":true,"featured_image":{"id":9383691583552,"product_id":2203996028992,"position":1,"created_at":"2019-02-14T23:19:09-12:00","updated_at":"2019-02-14T23:19:09-12:00","alt":null,"width":1000,"height":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1550229549","variant_ids":[25391903211584]},"available":true,"name":"Cotton Striped Pillows - 4 \/ Cotton","public_title":"4 \/ Cotton","options":["4","Cotton"],"price":32900,"weight":0,"compare_at_price":null,"inventory_management":null,"barcode":"","featured_media":{"alt":null,"id":4221264363584,"position":1,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1570462330"}}},{"id":25392885170240,"title":"6 \/ Cotton","option1":"6","option2":"Cotton","option3":null,"sku":"","requires_shipping":true,"taxable":true,"featured_image":{"id":9383691845696,"product_id":2203996028992,"position":2,"created_at":"2019-02-14T23:19:10-12:00","updated_at":"2019-02-14T23:19:10-12:00","alt":null,"width":1000,"height":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1550229550","variant_ids":[25392885170240]},"available":true,"name":"Cotton Striped Pillows - 6 \/ Cotton","public_title":"6 \/ Cotton","options":["6","Cotton"],"price":47900,"weight":0,"compare_at_price":null,"inventory_management":null,"barcode":"","featured_media":{"alt":null,"id":4221264396352,"position":2,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1570462330"}}}],"images":["\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1550229549","\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1550229550"],"featured_image":"\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1550229549","options":["Inch","Material"],"media":[{"alt":null,"id":4221264363584,"position":1,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1570462330"},"aspect_ratio":1.0,"height":1000,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p14.jpg?v=1570462330","width":1000},{"alt":null,"id":4221264396352,"position":2,"preview_image":{"aspect_ratio":1.0,"height":1000,"width":1000,"src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1570462330"},"aspect_ratio":1.0,"height":1000,"media_type":"image","src":"https:\/\/cdn.shopify.com\/s\/files\/1\/0167\/7249\/7472\/products\/p1.jpg?v=1570462330","width":1000}],"content":"\u003cp\u003eNam tempus turpis at metus scelerisque placerat nulla deumantos solicitud felis. Pellentesque diam dolor, elementum etos lobortis des mollis ut risus. Sedcus faucibus an sullamcorper mattis drostique des commodo pharetras loremos.Donec pretium egestas sapien et mollis.\u003c\/p\u003e\n\u003ch4\u003eSample Unordered List\u003c\/h4\u003e\n\u003cul\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous\u003c\/li\u003e\n\u003c\/ul\u003e\n\u003ch4\u003eSample Ordered Lista\u003c\/h4\u003e\n\u003col\u003e\n\u003cli\u003eComodous in tempor ullamcorper miaculis.\u003c\/li\u003e\n\u003cli\u003ePellentesque vitae neque mollis urna mattis laoreet.\u003c\/li\u003e\n\u003cli\u003eDivamus sit amet purus justo.\u003c\/li\u003e\n\u003cli\u003eProin molestie egestas orci ac suscipit risus posuere loremous.\u003c\/li\u003e\n\u003c\/ol\u003e\n\u003ch4\u003eSample Paragraph Text\u003c\/h4\u003e\n\u003cblockquote\u003ePraesent vestibulum congue tellus at fringilla. Curabitur vitae semper sem, eu convallis est. Cras felis nunc commodo eu convallis vitae interdum non nisl. Maecenas ac est sit amet augue pharetra convallis nec danos dui. Cras suscipit quam et turpis eleifend vitae malesuada magna congue. Damus id ullamcorper neque. Sed vitae mi a mi pretium aliquet ac sed elit. Pellentesque nulla eros accumsan quis justo at tincidunt lobortis denimes loremous. Suspendisse vestibulum lectus in lectus volutpat, ut dapibus purus pulvinar. Vestibulum sit amet auctor ipsum.\u003c\/blockquote\u003e"});



    // Add label if only one product option and it isn't 'Title'. Could be 'Size'.
    
     // Hide selectors if we only have 1 variant and its title contains 'Default'.
     

    // Auto-select first available variant on page load. Otherwise the product looks sold out.
    
    
    
    
    
    $('.single-option-selector:eq(0)').val("4").trigger('change');
    
    $('.single-option-selector:eq(1)').val("Cotton").trigger('change');
    
    
    
    
    
    
    
    $('.product-single .single-option-selector').wrap('<div class="selector-arrow">');
    
    

});  


</script>
<style>
  .swatch .tooltip{  display:block; }

  
</style>




</div>
</div>


</div>


</div>  
</div>
</div>


</div>


<div class="dt-sc-hr-invisible-large"></div>

</main>
@endsection