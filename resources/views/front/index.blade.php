@extends('layouts.front.app')
@section('og')
<meta property="og:type" content="home"/>
<meta property="og:title" content="{{ config('app.name') }}"/>
<meta property="og:description" content="{{ config('app.name') }}"/>
@endsection
@section('content')
<main class="main-content">
	<div class="grid-uniform">
		<div class="grid__item">
			<!-- BEGIN content_for_index -->

			@if(count($sliders)>0)
			<!-- <div id="shopify-section" class="shopify-section index-section index-section--flush"> -->

				<div id="owl-demo" class="owl-carousel owl-theme">
					@foreach($sliders as $slider)
					<div class="item">
						<img src="{{asset('storage/'.$slider->cover)}}" alt="The Last of us">
					</div>
					@endforeach
					
				</div>

				<!-- </div> -->
				@endif



				<div id="shopify-section-1546669459199" class="shopify-section index-section">
					<div data-section-id="1546669459199" data-section-type="content-block-type-1" class="content-block-type-1">
						<div class="container">
							<div class="section-header section-header--small">
								<div class="border-title">
									<h2 style="color:#11423b;">WHAT'S NEW</h2>
									<div class="short-desc">
										<p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
									</div>
								</div>
							</div>
							<div class="dt-sc-hr-invisible-small"></div>
							<div class="grid__item" style="position:relative;">

								<ul class="content_block grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="background:rgba(0,0,0,0)">
									@foreach($new_products as $key => $value)
									@if($key<2)
									<li class="grid__item wow fadeInUp animated" data-wow-delay="ms" style="visibility: visible;-webkit-animation-delay: ms; -moz-animation-delay: ms; animation-delay: ms;">
										<div class="content_section">
											<div class="support_icon">                      
												<a href="{{ url('product-detail/'.$value->slug ?? '') }}" class="support_section">
													<img src="{{ asset('storage/'.$value->cover) }}" alt="{{$value->name ?? ''}}" class="new-image">
												</a>
											</div>
											<div class="support_text">
												<h5 style="color:#11423b;">{{$value->name ?? ''}}</h5>
												<p style="color:#676e6d;" class="desc"><?php   echo substr($value->description ?? '',0,100)?>...</p>
											</div>
										</div>
									</li>
									@endif
									@endforeach
								</ul>
								@foreach($new_products as $key => $value)
								@if($key==2)
								<div class="v_column_img grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item">
									<a href="{{ url('product-detail/'.$value->slug ?? '') }}" class="support_section">
										<img src="{{ asset('storage/'.$value->cover) }}" alt="">
									</a>
								</div>
								@endif
								@endforeach
								<ul class="content_block content-block-2 grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="background:rgba(0,0,0,0)">
									@foreach($new_products as $key => $value)
									@if($key>2)
									<li class="grid__item  wow fadeInUp animated" data-wow-delay="ms" style="visibility: visible;-webkit-animation-delay: ms; -moz-animation-delay: ms; animation-delay: ms;">
										<div class="content_section">
											<div class="support_icon">                      
												<a href="{{ url('product-detail/'.$value->slug ?? '') }}" class="support_section">
													<img src="{{ asset('storage/'.$value->cover) }}" alt="{{$value->name ?? ''}}">
												</a>
											</div>
											<div class="support_text">
												<h5 style="color:#11423b;">{{$value->name ?? ''}}</h5>
												<p style="color:#676e6d;" class="desc"><?php   echo substr($value->description ?? '',0,100)?>...</p>
											</div>
										</div>
									</li>
									@endif
									@endforeach
								</ul>
							</div>
						</div>
						<div class="dt-sc-hr-invisible-large"></div>
					</div>
				</div>


				@if(count($categoryProducts)>0)
				<div id="shopify-section-1546669586143" class="shopify-section index-section">
					<div data-section-id="1546669586143" data-section-type="product-tab-type-1" class="product-tab-type-1" style="float:left;width:100%;">
						<div class="container">
							<div class="full_width_tab">
								<div class="grid-uniform">
									<div class="grid__item">
										<div class="dt-sc-tabs-container-section">
											<div class="border-title text-center wow fadeInDown animated animated" style="visibility: visible;">
												<h2 style="color:#11423b;">YOU MAY BE LOOKING FOR</h2>
												<div class="short-desc">
													<p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
												</div>
												<div class="dt-sc-hr-invisible-small"></div>
											</div>
											@php $i=0 @endphp
											@php $cnt=0 @endphp
											<div class="dt-sc-tabs-container">
												<ul class="dt-sc-tabs wow fadeInUp animated animated" style="visibility: visible;">
													@foreach($categoryProducts as $getCategory)
													<?php
													$category = $getCategory['category'];
													?>
													<li onclick="showTab({{$cnt++}})"><a href="#" class="btn tabs-1546669586143-0 current">{{ ucfirst($category->name ?? '') }} </a></li>
													@endforeach

												</ul>
												@foreach($categoryProducts as $getProducts)
												<?php
												$products = $getProducts['products'];
												?>
												<div class="dt-sc-tabs-content" id="tabContent{{$i}}">
													<ul class="tab2 12345-{{$i}} owl-carousel owl-theme owl-loaded owl-drag" id="12345-{{$i}}">

														@php $j=0 @endphp
														@foreach($products as $product)
														<div class="owl-item ">
															<li class="grid__item swiper-slide item-row" id="product-{{$product->id}}">
																<div class="products product-hover-15">
																	<div class="product-container ">
																		<a href="{{ url('product-detail/'.$product->slug ?? '')}}" class="grid-link">            
																			<img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="Coir Foam Mattress" class="featured-image" id="featured-image">
																		</a>
																		<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><div class="lSSlideOuter  vertical lightSlider lsGrab lSSlide" style="height: 58px;">
																			<div class="lSSlideWrapper usingCss lslide active" style="height: 54px; margin-bottom: 4px;">
																				<div class="lSSlideOuter  vertical"><div class="lSSlideWrapper usingCss" style="height: 170px;"><ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 116px;">
																					<li class="thumb_items lslide active" style="height: 54px; margin-bottom: 4px;">
																						<a data-thumb="{{ asset('storage/'.$product->cover ?? '') }}">
																							<img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="Coir Foam Mattress">
																						</a>
																					</li>
																					<li class="thumb_items lslide" style="height: 54px; margin-bottom: 4px;">
																						<a data-thumb="{{ asset('storage/'.$product->cover ?? '') }}">
																							<img src="{{ asset('storage/'.$product->cover ?? '') }}">
																						</a>
																					</li>
																				</ul><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>
																				<div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
																			</div>
																		</div><div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div></div></div>
																		<div class="product_right_tag  ">
																		</div>
																		<div class="ImageWrapper">
																			<div class="product-button">
																				
																				<a   class="quick-view-text" title="Wishlist" onclick="addToWishlist({{$product->id}})">                      
																					<b class="far fa-heart"></b>
																					<i>   Wishlist</i>
																				</a> 
																				 <a  id="add-cart-btn " class=" add-cart-btn" title="Add To Cart" onclick="addToCart({{$product->id}})">                      
																					<b class="fas fa-shopping-cart"></b>
																					<i>   Add To Cart</i>
																				</a>       
																				<!-- <form method="post" class="gom variants clearfix" id="cart-form-{{$product->id}}">                                    
																					<input type="hidden" name="id" value="25433112674368">  
																					<a class="add-cart-btn" title="Add to Cart">
																						<b class="fas fa-shopping-cart"></b>
																						<i>  Add to Cart</i>
																					</a>
																				</form> -->
																				<input type="hidden" id="quantityPd{{$product->id}}" value="1">
																				<input type="hidden" id="stock{{$product->id}}" value="@if($product->quantity>0){{ 'yes' }}@else{{ 'no' }}@endif">
																			</div>
																		</div>
																	</div>
																	<div class="product-detail">
																		<a href="{{ url('product-detail/'.$product->slug ?? '')}}" class="grid-link__title">{{$product->name ?? ''}}</a> 
																		<div class="grid-link__meta">
																			<div class="product_price">
																				<div class="grid-link__org_price" id="ProductPrice">
																					<span> <span class="money" data-currency-usd="RS. 1200.00" data-currency-inr="RS. 1200.00">{{$product->price ?? ''}}.00</span></span>
																				</div>
																			</div>
																			<span class="spr-badge" id="spr_badge_2204003729472" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i></span><span class="spr-badge-caption">1 review</span>
																		</span>
																	</div>
																	<ul class="item-swatch color_swatch_Value">  
																	</ul>
																</div>
															</div>
															<style>
																.product-container.product-hover-15 {position: relative;}
																.lSSlideOuter.vertical {
																	float: right;
																	width: 20%;
																	position: absolute;
																	top: 20px;
																	right: -100%;
																}
																.thumbs_items a {display:block;}
															</style>
														</li>
													</div>
													@php $j++ @endphp
													@endforeach


													
													<div class="owl-dots disabled"></div>
												</ul>
												<div class="tab_2_nav 12345-{{$i}} carousel-arrow " >
													<div class="owl-prev disabled"><a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>
													<div class="owl-next"><a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
												</div>
											</div>
											
											<script type="text/javascript">       
												$(document).ready(function(){
													var i = <?php echo $i; ?>;
													var tab2 = $('.tab2#12345-'+i);
													tab2.owlCarousel({
														loop:true,                      
														nav:true,
														navContainer: '.tab_2_nav.12345-'+i,
														navText: ['<a class="prev active btn"><i class="fa fa-angle-left" aria-hidden="true"></i></a>','<a class="next btn"><i class="fa fa-angle-right" aria-hidden="true"></i></a>'],
														dots: false,                  
														autoplayTimeout: 3000,
														responsive:{
															0:{
																items: 2
															},
															767:{
																items: 2
															},
															768:{
																items:3
															},
															1000:{
																items: 3
															},
															1200:{
																items: 3
															},
															1600:{
																items: 4
															}
														}
													});
												});

											</script>

											@php $i++ @endphp
											@endforeach

										</div>
										<!--End tabs container-->
									</div>
								</div>
							</div>
							<style>
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a { color:#11423b;background:none; }
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a.tabs-1546669586143-0:hover,.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current { color:#69e0ff; }
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current::after {background:#69e0ff;}
							</style>
							<style>
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a { color:#11423b;background:none; }
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a.tabs-1546670799435:hover,.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current { color:#69e0ff; }
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current::after {background:#69e0ff;}
							</style>
							<style>
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a { color:#11423b;background:none; }
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs  li a.tabs-1546670815318:hover,.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current { color:#69e0ff; }
								.product-tab-type-1 .full_width_tab ul.dt-sc-tabs li a.current::after {background:#69e0ff;}
							</style>
						</div>
					</div>
				</div>
				<div class="dt-sc-hr-invisible-large"></div>
			</div>
			@endif














			<div id="shopify-section-1546671026627" class="shopify-section index-section">
				<div data-section-id="1546671026627" data-section-type="video-content-block-1" class="video-content-block-1">
					<div class="grid-uniform" style="background-image:url(//cdn.shopify.com/s/files/1/0167/7249/7472/files/bg_3bd2010f-5200-42e8-8cb0-eacc4804654f.jpg?v=1549431984);clear: both;background-size: cover;background-repeat: no-repeat;padding:100px 0;">
						<div class="container">
							<div class="video-banner-type-1-block">
								<div class="intro-video grid__item wide--two-fifths post-large--two-fifths large--two-fifths">
									<div class="intro-video-text p-video">    
										<a href="https://www.youtube.com/watch?v=O_IJ5-YUli0" class="jas-popup-url video-player" data-effect="mfp-move-from-top">
											<img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/img-6_64cfe8d9-e4f6-4df2-8262-ab881f1b7f24.jpg?v=1550578667" alt="Feel the softness...">  
										</a>   
									</div>
								</div>
								<div class="video-banner-type-1-content  grid__item wide--three-fifths post-large--three-fifths large--three-fifths">
									<span style="color:#ffffff">Feel the softness...</span>
									<h2 style="color:#69e0ff">what type of mattress to buy?</h2>
									<p style="color:#ffffff">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia con sequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quis quam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
									<a class="btn" href="/products/all">Shop Now</a>    
								</div>
							</div>
						</div>
						<style>
							.video-banner-type-1-content a.btn { color:#11423b;background-color:#ffffff; }
							.video-banner-type-1-content a.btn:hover { color:#11423b;background-color:#69e0ff; }
						</style>
					</div>
					<div class="dt-sc-hr-invisible-large"></div>
				</div>
			</div>
			<div id="shopify-section-1546669814766" class="shopify-section index-section">
				<div data-section-id="1546669814766" data-section-type="product-tab-type-9-sorting" class="product-tab-type-9-sorting">
					<div class="full_width_tab">
						<div class="grid-uniform">
							<div class="grid__item">
								<div class="dt-sc-tabs-container-section filter-grid-type-3">
									<div class="container">
										<div class="section-header section-header--small">
											<div class="border-title">
												<h2 style="color:#11423b;">Brand Collection</h2>
												<div class="short-desc">
													<p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
												</div>
											</div>
										</div>
										<div class="dt-sc-hr-invisible-small"></div>
									</div>
									<div class="dt-sc-tabs-container">
										<ul class="dt-sc-tabs wow fadeInUp animated" style="visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;">
											@if(count($brands))
											@php $i=0 @endphp
											@foreach($brands as $brand)
											<li><a href="#" class="btn tabs-{{$brand->id}} @if($i==0){{'current'}}@endif">{{ ucfirst($brand->name ?? '') }} </a></li>
											@php $i++ @endphp
											@endforeach
											@endif

										</ul>
										@if(count($brandProducts)>0)
										@foreach($brandProducts as $bpd)
										@php $products = $bpd['products'] @endphp
										<div class="dt-sc-tabs-content portfolio-container current" style="padding: 0px; background: rgb(255, 255, 255); display: block;" >
											<div class="grid-uniform list-collection-products portfolio-container">
												@foreach($products as $product)
												<div class="grid__item grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item column gallery  text-center pickgradient-products">
													<img src="{{ asset('storage/'.$product->cover) }}" alt="Sold Product">        
													<div class="image-overlay">
														<div class="links">
															
															<a  class="link"  onclick="addToCart({{$product->id}})"><i class="fas fa-shopping-cart"></i></a>
															<a href="{{ asset('storage/'.$product->cover) }}" class="portfolio_zoom" rel="prettyPhoto[gallery4]"><i class="fas fa-search icons"></i></a>
														</div>
													</div>
													<div class="image-overlay-details">
														<h4><a href="{{ url('product-detail/'.$product->slug) }}">{{ strtoupper($product->name ?? '') }}</a></h4>
														<input type="hidden" id="quantityPd{{$product->id}}" value="1">
															<input type="hidden" id="stock{{$product->id}}" value="@if($product->quantity>0){{ 'yes' }}@else{{ 'no' }}@endif">
													</div>
												</div>
												@endforeach


											</div>
										</div>
										@endforeach

										@endif




									</div>
									<!--End tabs container-->
								</div>
							</div>
						</div>
						<style>
							.product-tab-type-9-sorting .dt-sc-tabs-frame-content, .dt-sc-tabs-content {border:none; }
							.filter-grid-type-3 .portfolio-container .grid__item.gallery:hover .image-overlay {    opacity: 1;}
							.filter-grid-type-3 .grid__item .image-overlay .links a { border:1px solid #ffffff;color:#ffffff; }
							.filter-grid-type-3 .grid__item .image-overlay .links a:hover { background:#ffffff;color:#11423b; }

							.filter-grid-type-3 .grid__item .image-overlay .links .fa-shopping-cart{
								color: #fff;
							} 
							.filter-grid-type-3 .grid__item .image-overlay .links a:hover .fa-shopping-cart{
								color:#11423b;
							}

							.filter-grid-type-3 .portfolio-container .image-overlay-details h4 a { color:#11423b; }
							.filter-grid-type-3 .portfolio-container .image-overlay-details h4 a:hover { color:#000; }
							.filter-grid-type-3 .portfolio-container .image-overlay-details p { color:#ffffff; }
							.filter-grid-type-3 .grid__item .image-overlay {  background: rgba(105, 224, 255, 0.8);  }
							.filter-grid-type-3 .portfolio-container .gallery:hover .image-overlay-details { }
						</style>
					</div>
				</div>
				<div class="dt-sc-hr-invisible-large"></div>
			</div>

			@if(count($latest_products)>0)
			<div id="shopify-section-1546671143172" class="shopify-section index-section">
				<div data-section-id="1546671143172" data-section-type="grid-banner-type-8" class="grid-banner-type-8">
					<div class="container">
						<div class="section-header section-header--small">
							<div class="border-title">
								<h2 style="color:#11423b;">Latest Collection</h2>
								<div class="short-desc">
									<p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
								</div>
							</div>
						</div>
						<div class="dt-sc-hr-invisible-small"></div>
						<div class="grid-uniform">
							@php $i=1 @endphp
							@foreach($latest_products as $product)
							<div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item loop-{{$i++}}">
								<div class="img-hover-effect">
									<div class="overlay">
										<img src="{{ asset('storage/'.$product->cover) }}" alt="Stylish">
										<a class="overlay-link" href="/"></a>
									</div>
									<div class="block-content">
										<h6 style="color:#11423b">{{ ucfirst($product->name ?? '') }}</h6>
										<h4 style="color:#11423b">Bedspread</h4>
										<p style="color:#11423b;"><?php echo substr($product->description ?? '',0,100) ?>...</p>
										<a href="{{ url('products/all') }}" class="btn">View all collection <i class="fas fa-arrow-right"></i></a>
									</div>
								</div>
							</div>
							@endforeach

						</div>
						<style>
							.grid-banner-type-8 .block-content { background:#f4f9f9; }
							.grid-banner-type-8 .loop-2 .block-content { background:#69e0ff; }
							.grid-banner-type-8 .loop-2 .block-content .btn { background:#69e0ff;color:#11423b;  }
							.grid-banner-type-8 .loop-2 .block-content .btn:hover { background:#ffffff; color:#11423b; }
							.grid-banner-type-8 .block-content .btn { background:#e7eeef;color:#11423b; }
							.grid-banner-type-8 .block-content .btn:hover { background:#69e0ff;color:#11423b; }
							Liquid error (sections/grid-banner-type-8.liquid line 68): Could not find asset snippets/hex2rgb.liquid
							.grid-banner-type-8 .img-hover-effect .overlay .overlay-link {  background: rgba(17, 66, 59, 0.8); }
						</style>
						<div class="dt-sc-hr-invisible-large"></div>
					</div>
				</div>
			</div>

			@endif



			@if(count($new_arrivals)>0)
			<div id="shopify-section-1546671336518" class="shopify-section index-section">
				<div data-section-id="1546671336518" data-section-type="vertical-product-grid-type-2" class="vertical-product-grid-type-2">
					<div class="vertical-product-grid" style="float:left;width:100%;background-color:#e7eeef;">
						<div class="dt-sc-hr-invisible-large"></div>
						<div class="container-bg">
							<div class="section-header section-header--small">
								<div class="border-title">
									<h2 class="section-header__title" style="color:#11423b;">    
										New arrivals
									</h2>
									<div class="short-desc">
										<p style="color:#676e6d;">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
									</div>
								</div>
							</div>
							<div class="dt-sc-hr-invisible-small"></div>
							<div class="grid-uniform">
								<ul class="v_grid-type-1__items">
									@foreach($new_arrivals as $product)
									<li class="grid__item item-row  wide--one-third post-large--one-third large--one-half medium--one-half small-grid__item" id="product-2204000845888">
										<div class="products">
											<div class="product-container">
												<a href="{{ url('product-detail/'.$product->slug) }}" class="grid-link">
													<div class="ImageOverlayCa"></div>
													<div class="reveal"> 
														<span class="product-additional">      
															<img src="{{ asset('storage/'.$product->cover) }}" class="featured-image" alt="{{ ucfirst($product->name ?? '') }}">
														</span>
														<img src="{{ asset('storage/'.$product->cover) }}" class="hidden-feature_img" alt="{{ ucfirst($product->name ?? '') }}">
													</div>
												</a>
											</div>
											<div class="product-detail">
												<div class="product_left">
													<a href="{{ url('product-detail/'.$product->slug) }}" class="grid-link__title">{{ ucfirst($product->name ?? '') }}</a>
													@if(!empty($product->brand_id))     
													<p class="product-vendor">
														<label>Brand :</label>
														<span>{{$product->brand_name}}</span>
													</p>
													@endif
													<div class="grid-link__meta">
														<div class="product_price">
															<div class="grid-link__org_price">
																<span class="money" data-currency-usd="RS. {{$product->price}}">RS. {{$product->price}}</span>
															</div>
														</div>
													</div>
													<form  method="post" class="variants clearfix" >                                    
													
														<a title="Add to Wishlist" class="add-cart-btn" id="cartNew" onclick="addToWishlist({{$product->id}})"> 
															<i class="fas fa-heart"></i>
														</a>
														<a title="Add to Cart" class="add-cart-btn" id="cartNew" onclick="addToCart({{$product->id}})"> 
															<i class="fas fa-shopping-cart"></i>
														</a>
													</form>
												</div>
											</div>
										</div>
									</li>

									@endforeach
								</ul>
								<div class="nav_v_grid-type-1__items">
									<a class="prev active"></a>
									<a class="next"></a>  
								</div>
							</div>
						</div>
						<style>
							.vertical-product-grid-type-2 ul li .products { background:#ffffff; }
						</style>
						<div class="dt-sc-hr-invisible-medium"></div>
					</div>
					<div class="dt-sc-hr-invisible-large"></div>
				</div>
			</div>

			@endif


			@if(count($blogs)>0)
			<div id="shopify-section-1546671399231" class="shopify-section">
				<div data-section-id="1546671399231" data-section-type="blog-post-type-3" class="blog-post-type-3">
					<div class="grid">
						<div class="blog-post">
							<div class="container">
								<div class="section-header section-header--small">
									<div class="border-title">
										<h2>From our blog</h2>
										<div class="short-desc">
											<p style="color:#676e6d;">    
												Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.
											</p>
										</div>
									</div>
								</div>
								<div class="dt-sc-hr-invisible-small"></div>
							</div>
							<div class="home-blog-type-3 blog-section">
								@foreach($blogs as $blog)
								<div class="article-item article-item-1 grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item" style="position:relative;background:url(storage/{{$blog->cover}});min-height:550px;background-size:cover;background-position:center;">
									<div class="blog-overlay"></div>
									<div class="article">
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">
												<p class="blog-date" style="color:#ffffff;">
													<time datetime="2019-02-14"><span class="date"><i style="color:#ffffff">{{ date('d / M',strtotime(explode(' ',$blog->created_at)[0])) }} </i> </span></time>         
												</p>
												<p class="author" style="color:#ffffff">
													<i class="zmdi zmdi-account"></i>
													<span> {{ ucfirst($blog->author ?? '') }}</span>
												</p>
											</div>
											<div class="home-blog-content blog-detail">
												<h4><a href="{{ url('blog/'.$blog->slug) }}" style="">{{ ucfirst($blog->title ?? '') }}</a></h4>
												<span style="color:#fff"><?php echo ucfirst($blog->description ?? '') ?></span>
												<div class="blog-btn">
													<a href="{{ url('blog/'.$blog->slug) }}">Read more <i class="fas fa-arrow-right"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endforeach

							</div>
						</div>
						<style>
							.blog-post-type-3 .border-title h2 {  color:#11423b; }
							.blog-post-type-3 .home-blog-content.blog-detail h4 a { color:#69e0ff; }
							.blog-post-type-3 .home-blog-content.blog-detail h4 a:hover { color:#ffffff; }
							.blog-post-type-3 .article-item .blog-overlay { background:#000; }
							.blog-post-type-3 .article-item.article-item-2 .blog-overlay { background:#000; }
							.blog-post-type-3 .article-item.article-item-2:hover .blog-overlay { background:#000; }
							.blog-post-type-3 .article-item:hover .blog-overlay { background:#000; }
							.blog-post-type-3 .article-item .blog-btn a { color:#ffffff; } 
							.blog-post-type-3 .article-item .blog-btn a:hover { color:#69e0ff; }
						</style>
					</div>
					<div class="dt-sc-hr-invisible-large"></div>
				</div>
			</div>
			@endif


			@if(count($testimonials)>0)

			<div id="shopify-section-1546672563385" class="shopify-section">
				<div data-section-id="1546672563385" data-section-type="testimonial-type-1" class="testimonial-type-1">
					<div class="grid-uniform">
						<div class="container-bg">
							<div class="section-header section-header--small">
								<div class="border-title">
									<h2 class="section-header__title" style="color:#11423b">What our customers say</h2>
									<div class="short-desc">
										<!-- <p style="color:#676e6d">Lorem is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p> -->
									</div>
								</div>
							</div>
							<div class="dt-sc-hr-invisible-small"></div>
							<div class="grid__item">
								@foreach($testimonials as $test)
								<div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
									<div class="testimonial-block" style="background:#e7eeef">
										<div class="grid__item wide--one-fifth post-large--one-fifth large--one-fifth">
											<div class="authorimg">
												<img src="{{ asset('storage/'.$test->cover) }}">
											</div>
										</div>
										<div class="grid__item wide--four-fifths post-large--four-fifths large--four-fifths">
											<div class="testimonial-detail">
												<blockquote>
													<q style="color:#676e6d">{{$test->description ?? ''}}</q>
												</blockquote>
												<h6 style="color:#11423b">{{ $test->name ?? '' }} <span style="color:#69e0ff">{{ $test->profession ?? '' }}</span></h6>
											</div>
										</div>
									</div>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
				<style>
					.testimonial-type-1 {background:#e7eeef;padding: 30px 0;}
					blockquote p {
						font-size: 12px!important;
					}
				</style>
			</div>

			@endif
			<!-- END content_for_index -->
		</div>
	</div>
	@endsection
