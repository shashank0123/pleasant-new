
@extends('layouts.front.app')

@section('og')
<meta property="og:type" content="home"/>
<meta property="og:title" content="{{ config('app.name') }}"/>
<meta property="og:description" content="{{ config('app.name') }}"/>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
@endsection

@section('content')
<main class="main-content">
   <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>{{$product['name'] ?? ''}}</h1>
      <a href="/" title="Back to the frontpage">Home</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <a href="{{ url('products/'.$category['slug'] ?? '') }}" title="">{{$category['name'] ?? ''}}</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>{{$product['name'] ?? ''}}</span>
   </nav>
   <div class="dt-sc-hr-invisible-large"></div>
   <div class="wrapper">
      <div class="grid-uniform">
         <div class="grid__item">
            <div class="container-bg">
               <div class="position-change">
                  <div class="grid__item wide--one-fifth post-large--one-quarter">
                     <div class="product_sidebar">
                       @if(count($categories)>0)
                       <div id="shopify-section-sidebar-category" class="shopify-section">
                        <div class="widget widget_product_categories">
                           <h4>
                              Category
                           </h4>
                           <ul class="product-categories dt-sc-toggle-frame-set">
                              @foreach($categories as $category)
                              <li class="cat-item cat-item-39 cat-parent first">
                                 <i></i>
                                 <a class="" href="{{ url('products/'.$category[0]->slug ?? '') }}">{{$category[0]->name ?? ''}}</a> <span class="dt-sc-toggle"></span>
                                 <?php $cats = $category;?>
                                 <ul class="children dt-sc-toggle-content ">
                                  @foreach($cats as $key=>$value)
                                  @if($key>0)
                                  <li class="second">
                                    <i></i>             
                                    <a class="" href="{{ url('products/'.$value->slug ?? '') }}">{{$value->name ?? ''}}</a>
                                 </li>
                                 @endif
                                 @endforeach

                              </ul>
                           </li>
                           @endforeach

                        </ul>
                     </div>
                  </div>
                  @endif

                  @if(count($hot_deals)>0)
                  <div id="shopify-section-product-sidebar-deals" class="shopify-section">
                     <div data-section-id="product-sidebar-deals" data-section-type="product-sidebar-deals" class="product-sidebar-deals">
                        <div class="widget widget_top_rated_products">
                           <h4><span>Hot Deals</span></h4>
                           <div class="widget_top_rated_products-section" style="height: 350px">
                              <ul class="no-bullets sidebar-deal-products owl-carousel owl-theme owl-loaded owl-drag">

                                 @foreach($hot_deals as $deal)
                                 <div class="owl-item active">
                                    <li>
                                       <a class="thumb grid__item" href="{{ url('product-detail/'.$deal->slug ?? '') }}">                                          
                                          <img alt="featured product" src="{{ asset('storage/'.$deal->cover ?? '') }}" style="width: auto; height: 150px;">                                              
                                       </a>
                                       <div class="products">
                                          <div class="top-products-detail product-detail grid__item">
                                             <div class="product_left">
                                                <a class="grid-link__title" href="{{ url('product-detail/'.$deal->slug ?? '') }}"> {{ $deal->name ?? '' }} </a>
                                                <span class="spr-badge" id="spr_badge_2204006449216" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                             </span>
                                             <ul class="item-swatch color_swatch_Value">  
                                             </ul>
                                          </div>
                                          <div class="top-product-prices grid-link__meta">
                                             <div class="product_price">
                                                <div class="grid-link__org_price">
                                                   <span class="money" data-currency-usd="RS. {{ $deal->price ?? '' }}" data-currency="INR">RS. {{ $deal->price ?? '' }}</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </div>
                              @endforeach




                           </ul>
                           <div class="product-sidebar-deals-nav">
                              <div class="owl-prev disabled"> <a class="prev btn active"><i class="fa fa-angle-left"></i></a></div>
                              <div class="owl-next"> <a class="next btn active"><i class="fa fa-angle-right"></i></a></div>
                           </div>
                        </div>
                     </div>
                     <style>
                        .sidebar-deal-products .lof-clock-timer-detail-single li {background:#000;color:#ffffff;}
                     </style>
                  </div>

               </div>
               @endif

               @if(count($best_sellers)>0)
               <div id="shopify-section-product-sidebar-bestsellers" class="shopify-section">
                  <div data-section-id="product-sidebar-bestsellers" data-section-type="product-sidebar-bestsellers" class="product-sidebar-bestsellers">
                     <div class="widget widget_top_rated_products">
                        <h4><span>Best Sellers</span></h4>
                        <ul class="no-bullets top-products">
                           @foreach($best_sellers as $best)
                           <li class="products">
                              <span class="top_product_count">01</span>
                              <div class="top-products-detail product-detail">
                                 <a class="grid-link__title" href="{{ url('product-detail/'.$best->slug ?? '') }}"> {{ 
                                    $best->name ?? '' }} </a>            
                                    <div class="top-product-prices grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="RS. {{ $best->price ?? '0' }}" data-currency="INR">RS. {{ $best->price ?? '0' }}</span>
                                          </div>
                                       </div>
                                    </div>
                                    <span class="spr-badge" id="spr_badge_2204009070656" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                 </span>
                              </div>
                              <a class="thumb grid__item post-large--one-half" href="{{ url('product-detail/'.$best->slug ?? '') }}">                  
                                 <img alt="featured product" src="{{ url('storage/'.$best->cover ?? '') }}" style="height: 60px">                                              
                              </a>
                           </li>
                           @endforeach

                        </ul>
                     </div>
                  </div>
               </div>
               @endif

            </div>
         </div>
         <div class="second">
            <div id="shopify-section-product-template" class="shopify-section">
               <div class="grid__item wide--four-fifths post-large--three-quarters">
                  <div itemscope="" itemtype="" class="single-product-layout-type-1">
                     <meta itemprop="url" content="{{ url('product-detail/'.$product['slug']) }}">
                     <meta itemprop="image" content="{{ asset('storage/'.$product['cover']) }}">
                     <div class="product-single">
                        <div class="grid__item wide--one-half post-large--one-half large--one-half product-img-box">
                           <div class="product-photo-container">
                              <a href="{{ asset('storage/'.$product['cover']) }}">

                                 <img id="product-featured-image" src="{{ asset('storage/'.$product['cover']) }}" alt="{{ $product['name'] ?? '' }}" data-zoom-image="{{ asset('storage/'.$product['cover']) }}"  style="height: 350px;" >
                                 
                              </a>
                           </div>
                           <div class="  more-view-wrapper   more-view-wrapper-owlslider ">
                              <ul id="ProductThumbs" class="product-photo-thumbs  owl-carousel owl-theme owl-loaded owl-drag">

                                 <div class="owl-item active" style="width: 115.667px; margin-right: 10px;">
                                    <li class="grid-item">
                                       <a href="javascript:void(0)" data-image="{{ asset('storage/'.$product['cover']) }}" data-zoom-image="{{ asset('storage/'.$product['cover']) }}">
                                          <img src="{{ asset('storage/'.$product['cover']) }}" alt="{{ $product['name'] ?? '' }}">
                                       </a>
                                    </li>
                                 </div>
                                 @if(count($images)>0)
                                 @foreach($images as $image)
                                 <div class="owl-item" style="width: 115.667px; margin-right: 10px;">
                                    <li class="grid-item">
                                       <a href="javascript:void(0)" data-image="{{ asset('storage/'.$image['src']) }}" data-zoom-image="{{ asset('storage/'.$image['src']) }}">
                                          <img src="{{ asset('storage/'.$image['src']) }}" alt="{{ $product['name'] ?? '' }}">
                                       </a>
                                    </li>
                                 </div>
                                 @endforeach
                                 @endif

                              </ul>
                              <div class="single-page-owl-carousel disabled">
                                 <div class="owl-prev disabled"><a class="prev"><i class="icon-arrow-left icons"></i></a></div>
                                 <div class="owl-next disabled"><a class="next"><i class="icon-arrow-right icons"></i></a></div>
                              </div>
                           </div>
                        </div>
                        <div class="product_single_detail_section grid__item wide--one-half post-large--one-half large--one-half">
                           <h2 itemprop="name" class="product-single__title">{{ $product['name'] ?? '' }}</h2>
                           <p class="product-arrows">
                              <span class="left">
                                 <a href="/collections/best-seller/products/coir-foam-mattress#content"> <i class="fas fa-chevron-left"></i></a>
                              </span>
                              <span class="right">
                                 <a href="/collections/best-seller/products/standard-microfibre-pillow#content"> <i class="fas fa-chevron-right"></i></a>
                              </span>
                           </p>
                           <div class="product_single_price">
                              <div class="product_price">
                                 <div class="grid-link__org_price" id="ProductPrice"><span class="money" data-currency-usd="RS. {{ $product['price'] ?? '0' }}" data-currency="INR">RS. {{ $product['price'] ?? '0' }}</span></div>
                              </div>
                           </div>
                           <span class="spr-badge" id="spr_badge_2204001599552" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                        </span>
                        <div class="grid__item product_desc_section" style="position:relative;border-top:1px solid #eee;">
                        </div>
                        <div class="product-description rte" itemprop="description">
                           <?php echo $product['description'] ?? '' ?> 
                        </div>
                        <div itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                           <meta itemprop="priceCurrency" content="INR">
                           <link itemprop="availability" href="http://schema.org/InStock">
                           <form method="post" action="/cart/add" id="AddToCartForm" accept-charset="UTF-8" class="product-form" enctype="multipart/form-data">
                              <input type="hidden" name="form_type" value="product"><input type="hidden" name="utf8" value="✓">
                              <div class="selector-wrapper-secton">
                                 <style>
                                    label[for="product-select-option-0"] { display: none; }
                                    #productSelect-option-0 { display: none; }
                                    #productSelect-option-0 + .custom-style-select-box { display: none !important; }
                                 </style>
                                       <input type="radio" name="product_size" value="" id="productSize{{$product['id']}}">
                                       <input type="radio" name="product_size" value="" id="productPrice{{$product['id']}}">
                                 @if(!empty($product_sizes) && count($product_sizes)>0)
                                 <div class="swatch clearfix" data-option-index="0">
                                    <div class="header">Size :</div>
                                    <div class="swatch-section">
                                       @foreach($product_sizes as $size)
                                       <div data-value="{{$size->product_size ?? ''}}" class="swatch-element {{$size->product_size ?? ''}} available">
                                          <input id="swatch-0-{{$size->product_size ?? ''}}" type="radio" name="product_size"  value="{{$size->product_size ?? ''}}">
                                          <label for="swatch-0-{{$size->product_size}}" onclick="showSizewisePrice({{ $product['id'] ?? '' }},{{ $size->product_price ?? '' }},'{{$size->product_size ?? ''}}')">
                                             {{$size->product_size ?? ''}}

                                          </label>
                                       </div>
                                       @endforeach

                                    </div>
                                 </div>
                                 @endif
                                 <style>
                                    label[for="product-select-option-1"] { display: none; }
                                    #productSelect-option-1 { display: none; }
                                    #productSelect-option-1 + .custom-style-select-box { display: none !important; }
                                 </style>

                                 <div class="swatch clearfix" data-option-index="1">
                                    <div class="header">Material :</div>
                                    <div class="swatch-section">
                                       <div data-value="Silk" class="swatch-element silk available">
                                          <input id="swatch-1-silk" type="radio" name="option-1" value="Silk">
                                          <label for="swatch-1-silk">
                                             Silk
                                             <img class="crossed-out" src="//cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/soldout.png?v=6625312704354037208">
                                          </label>
                                       </div>

                                    </div>
                                 </div>
                                 @if(count($sizes)>0)
                                 <div class="selector-wrapper" style="display: none;">
                                    <label for="productSelect-option-0">Size</label>
                                    <div class="selector-arrow">
                                       <select class="single-option-selector" data-option="option1" id="productSelect-option-0">
                                          @foreach($sizes as $size)
                                          <option value="{{$size}}">{{$size}}</option>
                                          @endforeach
                                       </select>
                                    </div>
                                 </div>
                                 @endif
                                 @if(!empty($product['material']))
                                 <div class="selector-wrapper" style="display: none;">
                                    <label for="productSelect-option-1">Material</label>
                                    <div class="selector-arrow">
                                       <select class="single-option-selector" data-option="option2" id="productSelect-option-1">
                                          <option value="Silk">{{ $product['material'] ?? '' }}</option>
                                       </select>
                                    </div>
                                 </div>
                                 @endif
                              </div>
                              <div class="product-single__quantity">
                                 <div class="quantity-box-section ">
                                    <label>Quantity :</label>
                                    <div class="quantity_width">
                                       <div class="dec button"  onclick="minusProductQty({{$product['id']}})">-</div>
                                       <input type="number" id="quantityPd{{ $product['id'] }}" name="quantityId" value="1" min="1" >
                                       <div class="inc button" onclick="plusProductQty({{$product['id']}})">+</div>
                                       <p class="min-qty-alert" style="display:none">Minimum quantity should be 1</p>
                                    </div>
                                 </div>
                                 <div class="total-price">
                                    <label>Subtotal : </label><span><span class="money" data-currency-usd="RS. {{ $product['price'] ?? '' }}" data-currency="INR" id="ProductPrice1">RS. {{ $product['price'] ?? '' }}</span></span>
                                 </div>
                              </div>

                              <div class="product-infor">
                                 @if(!empty($brand))
                                 <p class="product-vendor">
                                    <label>Brand :</label>
                                    <span>{{ $brand->name ?? '' }}</span>
                                 </p>
                                 @endif
                                 @if(!empty($product['product_type']))
                                 <p class="product-type">
                                    <label>Product Type : </label>  
                                    <span>{{ $product['product_type'] ?? '' }}</span>
                                 </p>
                                 @endif
                                 <p class="product-inventory" id="product-inventory">
                                    <label>Availability :  </label>              
                                    <span class="many-in-stock">@if($product['quantity']>0){{'In Stock'}}@else{{'Out Of Stock'}}@endif</span>
                                 </p>
                                 <input type="hidden" id="stock{{$product->id}}" value="@if($product['quantity']){{ 'yes' }}@else{{ 'no' }}@endif">
                              </div>
                              <button type="button" name="add" id="AddToCart" class="btn" style="padding: 12px"  onclick="addToCart({{$product->id}})">
                                 <i class="fa fa-cart-plus" aria-hidden="true"></i><span id="AddToCartText">Add to Cart</span>
                              </button>
                              <div data-shopify="payment-button" class="shopify-payment-button">
                                 <div>
                                    <div>
                                       <div>

                                          <button type="button" class="shopify-payment-button__button shopify-payment-button__button--unbranded " onclick="buyNow({{ $product['id'] }})">Buy it now</button>
                                          
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="add-to-wishlist">
                                 <div class="show">

                                    <button type="button" name="add" id="AddToCart" class="btn" style="padding: 12px" onclick="addToWishlist({{$product->id}})">
                                       <i class="far fa-heart" aria-hidden="true"></i><span id="AddToCartText">Add to Wishlist</span>
                                    </button>

                                 </div>
                              </div>
                           </form>
                        </div>
                        <div class="share_this_btn">
                           <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
                              <a class="addthis_button_facebook addthis_button_preferred_1 at300b" title="Facebook" href="#">
                                 <span class="at-icon-wrapper" style="background-color: rgb(59, 89, 152); line-height: 32px; height: 32px; width: 32px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-2" title="Facebook" alt="Facebook" class="at-icon at-icon-facebook" style="width: 32px; height: 32px;">
                                       <title id="at-svg-facebook-2">Facebook</title>
                                       <g>
                                          <path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>
                                       </g>
                                    </svg>
                                 </span>
                              </a>
                              <a class="addthis_button_twitter addthis_button_preferred_2 at300b" title="Twitter" href="#">
                                 <span class="at-icon-wrapper" style="background-color: rgb(29, 161, 242); line-height: 32px; height: 32px; width: 32px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-3" title="Twitter" alt="Twitter" class="at-icon at-icon-twitter" style="width: 32px; height: 32px;">
                                       <title id="at-svg-twitter-3">Twitter</title>
                                       <g>
                                          <path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path>
                                       </g>
                                    </svg>
                                 </span>
                              </a>

                              <div class="atclear"></div>
                           </div>

                        </div>
                     </div>
                  </div>
                  <div class="dt-sc-hr-invisible-large"></div>
                  <div class="dt-sc-tabs-container">
                     <ul class="dt-sc-tabs">
                        <li><a class="current" href="#"> Product Description </a></li>
                        <li><a class="" href="#"> Reviews  </a></li>
                        <!-- <li><a class="" href="#"> Shipping Details  </a></li> -->
                     </ul>
                     <div class="dt-sc-tabs-content" id="desc_pro" style="display: block;">
                        <?php echo $product['description'] ?? '' ?>
                     </div>
                     <div class="dt-sc-tabs-content" style="display: none;">
                        <div class="commentlist">
                           <div class="comment-text">
                              <div class="rating-review">
                                 <div id="shopify-product-reviews" data-id="2204001599552">
                                    <style scoped="">.spr-container {
                                       padding: 24px;
                                       border-color: #ECECEC;}
                                       .spr-review, .spr-form {
                                          border-color: #ECECEC;
                                       }
                                    </style>
                                    <div class="spr-container" >
                                       <div class="spr-header">
                                         
                                          <div class="spr-summary">
                                             @if(count($reviews)==0)
                                             <span class="spr-summary-caption">No reviews yet</span>
                                             @endif<span class="spr-summary-actions">
                                                <a style="cursor: pointer" class="spr-summary-actions-newreview" onclick="showReviewForm()">Write a review</a>
                                             </span>
                                          </div>
                                       </div>
                                       <div class="spr-content">
                                          <div class="spr-form" >
                                             <div class="row">
                                                <div class="col-sm-7">
                                                   <form method="post" action="{{ url('submit-product-review') }} " id="productReviewForm" class="new-review-form">
                                                     @csrf
                                                     <input type="hidden" name="product_id" value="{{ $product['id'] }}">
                                                      <h3 class="spr-form-title">Write a review</h3>
                                                      <fieldset class="spr-form-contact">
                                                         <div class="spr-form-contact-name">
                                                            <label class="spr-form-label" for="review_name">Name</label><br/>
                                                            <input class="spr-form-input spr-form-input-text form-control" type="text" name="name" id="review_name" value="{{ auth()->user()->name ?? '' }}" placeholder="Enter your name">
                                                         </div>
                                                         <div class="spr-form-contact-email">
                                                            <label class="spr-form-label" for="email">Email</label><br/>
                                                            <input class="spr-form-input spr-form-input-email " id="review_email" type="email" name="email" value="{{ auth()->user()->email ?? '' }}" placeholder="john.smith@example.com">
                                                         </div>
                                                      </fieldset>
                                                      <fieldset class="spr-form-review">
                                                         <div class="spr-form-review-rating">
                                                            <label class="spr-form-label" for="product_rating">Rating</label>
                                                            <div class="spr-form-input spr-starrating ">

                                                               <style type="text/css">
                                                                  .ratingStar {

                                                                     list-style-type: none;
                                                                  }
                                                                  .ratingStar li { 
                                                                    padding-right: 10px }

                                                                    .ratingStar .fas {
                                                                     color: #d89522;
                                                                  }
                                                               </style>
                                                               <ul class="ratingStar">
                                                                <li>
                                                                  <input type="radio" name="product_rating" id="product_rating" value="1" checked="checked">&nbsp;&nbsp;
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="far fa-star"></i>
                                                                  <i class="far fa-star"></i>
                                                                  <i class="far fa-star"></i>
                                                                  <i class="far fa-star"></i><br>
                                                               </li>

                                                               <li>
                                                                  <input type="radio" name="product_rating" id="product_rating" value="2">&nbsp;&nbsp;
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="far fa-star"></i>
                                                                  <i class="far fa-star"></i>
                                                                  <i class="far fa-star"></i><br>
                                                               </li>

                                                               <li>
                                                                  <input type="radio" name="product_rating" id="product_rating" value="3">&nbsp;&nbsp;
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="far fa-star"></i>
                                                                  <i class="far fa-star"></i><br>
                                                               </li>

                                                               <li>
                                                                  <input type="radio" name="product_rating" id="product_rating" value="4">&nbsp;&nbsp;
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="far fa-star"></i><br>
                                                               </li>

                                                               <li>
                                                                  <input type="radio" name="product_rating" id="product_rating" value="5">&nbsp;&nbsp;
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i>
                                                                  <i class="fas fa-star"></i><br>
                                                               </li>

                                                            </ul>
                                                            <br>                      </div>
                                                         </div>
                                                         <!-- <div class="spr-form-review-title">
                                                            <label class="spr-form-label" for="review_title_2204001599552">Review Title</label>
                                                            <input class="spr-form-input spr-form-input-text " id="review_title_2204001599552" type="text" name="review[title]" value="" placeholder="Give your review a title">
                                                         </div> -->
                                                         <div class="spr-form-review-body">
                                                            <label class="spr-form-label" for="product_review">
                                                               Body of Review
                                                               <span role="status" aria-live="polite" aria-atomic="true">
                                                                  <span class="spr-form-review-body-charactersremaining">(1500)</span>
                                                                  <span class="visuallyhidden">characters remaining</span>
                                                               </span>
                                                            </label>
                                                            <div class="spr-form-input">
                                                               <textarea class="spr-form-input spr-form-input-textarea " id="product_review"  name="product_review" rows="10" placeholder="Write your comments here"></textarea>

                                                            </div>
                                                         </div>
                                                      </fieldset>
                                                      <fieldset class="spr-form-actions">
                                                         <input type="submit" class="spr-button spr-button-primary button button-primary btn btn-primary" value="Submit Review">
                                                      </fieldset>
                                                   </form>
                                                </div>
                                                <div class="col-sm-5"style="max-height: 500px; overflow: auto; text-align: left !important;">
                                                    <h2 class="spr-header-title">Customer Reviews</h2>
                                                   @if(count($reviews)>0)

                                                   <div class="row">
                                                      <table class="table">
                                                   @foreach($reviews as $key => $review)
                                                         <tr>
                                                            <th>{{ ++$key }}
                                                            </th>
                                                            <td colspan="5">
                                                               <h5>{{ $review->name ?? '' }}</h5>
                                                               <h6 style="color: #d89522; font-size: 10px;">{{ $review->email ?? '' }}</h6>
                                                               <p>{{ $review->product_review ?? '' }}</p>
                                                            </td>
                                                         </tr>
                                                   @endforeach
                                                      </table>
                                                   </div>
                                                   @else
                                                   No reviews yet.
                                                   @endif
                                                </div>
                                             </div>
                                          </div>
                                          <div class="spr-reviews" id="reviews_2204001599552" style="display: none"></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="dt-sc-tabs-content" style="display: none;">
                        
                     </div> -->
                  </div>
                  <div class="dt-sc-hr-invisible-small"></div>
                  @if(count($related_products)>0)
                  <div class="related-products-container">
                     <div class="section-header section-header--small">
                        <div class="border-title">
                         <h3 class="section-header__title"> Related Products</h3>
                      </div>
                   </div>
                   <div class="related_products_container">
                     <ul class="grid-uniform grid-link__container related-products owl-carousel owl-theme owl-loaded owl-drag">

                        @foreach($related_products as $product)
                        <div class="owl-item">
                           <li class="grid__item swiper-slide item-row      " id="product-2204000419904">
                              <div class="products product-hover-15">
                                 <div class="product-container ">
                                    <a href="{{ url('product-detail/'.$product->slug ?? '') }}" class="grid-link">            
                                       <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="{{ $product->name ?? '' }}" class="featured-image"style="height: 200px">
                                    </a>
                                    <div class="lSSlideOuter  vertical">
                                       <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                          <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                             <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                <a data-thumb="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498">
                                                   <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/products/p23_09773fe5-d553-4e82-af31-0a00621daa63.jpg?v=1550302498" alt="Door Curtain Set">
                                                </a>
                                             </li>
                                             <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                <a data-thumb="{{ asset('storage/'.$product->cover ?? '') }}">
                                                   <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="{{ $product->name  ?? '' }}">
                                                </a>
                                             </li>
                                          </ul>
                                          <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                       </div>
                                    </div>
                                    <div class="product_right_tag  ">
                                    </div>
                                    <div class="ImageWrapper">
                                       <div class="product-button">
                                          <a href="javascript:void(0)" id="door-curtain-set" class="quick-view-text" title="Quick View" onclick="addToWishlist({{$product->id}})">                      
                                             <b class="far fa-heart"></b>
                                             <i>   Add To wishlist</i>
                                          </a>       
                                          <form action="" method="post" class="gom variants clearfix" id="cart-form-2204000419904">                                    
                                             <input type="hidden" id="quantityPd{{$product->id}}" value="1">
                                             <input type="hidden" id="stock{{$product->id}}" value="@if($product['quantity']){{ 'yes' }}@else{{ 'no' }}@endif">

                                             <a class="add-cart-btn" title="Add to Cart" onclick="addToCart({{$product->id}})" id="add-cart-btn ">
                                                <b class="fas fa-shopping-cart"></b>
                                                <i>  Add to Cart</i>
                                             </a>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="product-detail">
                                    <a href="{{ url('product-detail/'.$product->slug ?? '') }}" class="grid-link__title">{{ $product->name ?? '' }}</a> 
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price" id="ProductPrice">
                                             <span> <span class="money" data-currency-usd="RS. {{ $product->price ?? '0' }}" data-currency="INR">RS. {{ $product->price ?? '0' }}</span></span>
                                          </div>
                                       </div>
                                       <span class="spr-badge" id="spr_badge_2204000419904" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                    </span>
                                 </div>
                                 <ul class="item-swatch color_swatch_Value">  
                                 </ul>
                              </div>
                           </div>
                           <style>
                              .product-container.product-hover-15 {position: relative;}
                              .lSSlideOuter.vertical {
                                 float: right;
                                 width: 20%;
                                 position: absolute;
                                 top: 20px;
                                 right: -100%;
                              }
                              .thumbs_items a {display:block;}
                           </style>
                        </li>
                     </div>
                     @endforeach


                  </ul>
                  <div class="nav_featured">
                     <div class="owl-prev"><a class="prev"><i class="btn fas fa-angle-left"></i></a></div>
                     <div class="owl-next"><a class="next"><i class="btn fas fa-angle-right"></i></a></div>
                  </div>
               </div>
            </div>
            @endif
            <style>
            </style>
            
            <script src="//ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js" type="text/javascript"></script>
            <div class="dt-sc-hr-invisible-small"></div>
            <section class="recently-viewed-products">
               <div class="section-header section-header--small">
                  <div class="border-title">
                     <h3>Recently Viewed Products</h3>
                  </div>
               </div>
               <div class="products-grid" id="recently-viewed-products-grid">
               </div>
            </section>


         </div>
      </div>

      <style>
         .swatch .tooltip{  display:block; }
      </style>
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="dt-sc-hr-invisible-large"></div>
</main>
@include('front.scripts.script')
@endsection