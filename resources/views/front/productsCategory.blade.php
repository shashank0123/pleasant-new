@extends('layouts.front.app')
@section('og')
<meta property="og:type" content="home"/>
<meta property="og:title" content="{{ config('app.name') }}"/>
<meta property="og:description" content="{{ config('app.name') }}"/>
@endsection
@section('content')

<main class="main-content">
   <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>Products</h1>
      <a href="/" title="Back to the frontpage">Home</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>Products</span>
   </nav>
   <div class="dt-sc-hr-invisible-large"></div>
   <div class="wrapper">
      <div class="grid-uniform">
         <div class="grid__item">
            <div class="container-bg">
               <div class="grid__item">
                  <div class="collection-products position-change">
                     <div class="grid__item wide--one-fifth post-large--one-fifth  left-sidebar ">
                        <!-- /snippets/collection-sidebar.liquid -->
                        <div class="collection_sidebar">
                         @if(count($categories)>0)
                         <div id="shopify-section-sidebar-category" class="shopify-section">
                           <div class="widget widget_product_categories">
                              <h4>
                                 Category
                              </h4>
                              <ul class="product-categories dt-sc-toggle-frame-set">
                                 @foreach($categories as $category)
                                 <li class="cat-item cat-item-39 cat-parent first">
                                    <i></i>
                                    <a class="" href="{{ url('products/'.$category[0]->slug ?? '') }}">{{$category[0]->name ?? ''}}</a> <span class="dt-sc-toggle"></span>
                                    <?php $cats = $category;?>
                                    <ul class="children dt-sc-toggle-content ">
                                     @foreach($cats as $key=>$value)
                                     @if($key>0)
                                     <li class="second">
                                       <i></i>             
                                       <a class="" href="{{ url('products/'.$value->slug ?? '') }}">{{$value->name ?? ''}}</a>
                                    </li>
                                    @endif
                                    @endforeach

                                 </ul>
                              </li>
                              @endforeach

                           </ul>
                        </div>
                     </div>
                     @endif
                     <div class="refined-widgets">
                        <a href="javascript:void(0)" class="clear-all" style="display:none">
                           Clear All
                        </a>
                     </div>
                     <div class="sidebar-block">
                        <div id="shopify-section-sidebar-colors" class="shopify-section">
                        </div>
                        <div id="shopify-section-sidebar-tag-filters" class="shopify-section">
                           <aside class="sidebar-tag filter tags shop by size ">
                              <div class="widget">
                                 <h4>
                                    <span>Shop By Size </span>
                                    <a href="javascript:void(0)" class="clear" style="display:none">
                                       <i class="fas fa-times"></i>
                                    </a>
                                 </h4>
                                 <div class="widget-content">
                                    <ul>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="4">
                                          <label>4</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="6">
                                          <label>6</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="8">
                                          <label>8</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="9">
                                          <label>9</label>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </aside>
                           <aside class="sidebar-t ag filter tags shop by price ">
                              <div class="widget">
                                 <h4>
                                    <span>Shop By Price </span>
                                    <a href="javascript:void(0)" class="clear" style="display:none">
                                       <i class="fas fa-times"></i>
                                    </a>
                                 </h4>
                                 <div class="widget-content">
                                    <ul>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="100-200">
                                          <label>$100 - $200</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="200-300">
                                          <label>$200 - $300</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="300-500">
                                          <label>$300 - $500</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="500-700">
                                          <label>$500 - $700</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="700-1000">
                                          <label>$700 - $1000</label>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </aside>
                           <aside class="sidebar-tag filter tags shop by materials ">
                              <div class="widget">
                                 <h4>
                                    <span>Shop By Materials </span>
                                    <a href="javascript:void(0)" class="clear" style="display:none">
                                       <i class="fas fa-times"></i>
                                    </a>
                                 </h4>
                                 <div class="widget-content">
                                    <ul>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="cotton">
                                          <label>Cotton</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="polyester">
                                          <label>Polyester</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="velvet">
                                          <label>Velvet</label>
                                       </li>
                                       <li>
                                          <i></i>
                                          <input type="checkbox" value="microfibre">
                                          <label>Microfibre</label>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </aside>
                           @if(count($brands)>0)
                           <aside class="sidebar-tag filter tags shop by brand ">
                              <div class="widget">
                                 <h4>
                                    <span>Shop By Brand </span>
                                    <a href="javascript:void(0)" class="clear" style="display:none">
                                       <i class="fas fa-times"></i>
                                    </a>
                                 </h4>
                                 <div class="widget-content">
                                    <ul>
                                     @foreach($brands as $brand)
                                     <li onclick="getBrand({{$brand->id}})">
                                       <i></i>
                                       <input type="checkbox" value="captcha">
                                       <label>{{ucfirst($brand->name ?? '')}}</label>
                                    </li>
                                    @endforeach
                                 </ul>
                              </div>
                           </div>
                        </aside>
                        @endif
                     </div>
                  </div>
                  @if(count($best_sellers)>0)
                  <div id="shopify-section-sidebar-bestsellers" class="shopify-section">
                     <div class="widget widget_top_rated_products">
                        <h4>Best Sellers</h4>
                        <ul class="no-bullets top-products sidebar-bestsellers owl-carousel owl-theme owl-loaded owl-drag">

                           @foreach($best_sellers as $product)
                           <div class="owl-item" >
                              <li class="products">
                                 <div class="product-container"> 
                                    <a class="thumb grid__item" href="{{ url('product-detail/'.$product->slug ?? '') }}">                                          
                                       <img alt="featured product" src="{{ asset('storage/'.$product->cover ?? '') }}">                                              
                                    </a>
                                 </div>
                                 <div class="top-products-detail product-detail grid__item">
                                    <a class="grid-link__title" href="{{ url('product-detail/'.$product->slug ?? '') }}"> {{ $product->name ?? '' }} </a>            
                                    <div class="top-product-prices grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price">
                                             <span class="money" data-currency-usd="RS. {{ $product->price ?? '' }}">RS. {{ $product->price ?? '' }}</span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </div>
                           @endforeach  

                        </ul>
                        <div class="top_products_nav sidebar-bestsellers">
                           <div class="owl-prev disabled"><a class="prev btn active"><i class="fa fa-chevron-left"></i></a></div>
                           <div class="owl-next"><a class="next btn"><i class="fa fa-chevron-right"></i></a></div>
                        </div>
                     </div>
                     <script type="text/javascript">
                        $(document).ready(function(){
                         $(".top-products.sidebar-bestsellers").owlCarousel({ 
                            loop:true,      
                            nav:true,       
                            dots: false,
                            navContainer: ".top_products_nav.sidebar-bestsellers",
                            navText: ['<a class="prev btn active"><i class="fa fa-chevron-left"></i></a>','<a class="next btn"><i class="fa fa-chevron-right"></i></a>'],
                            responsive:{
                              0:{
                                items:1
                             },
                             600:{
                                items:1
                             },
                             1000:{
                                items:1
                             }
                          }

                       });
                      });

                   </script>
                </div>
                @endif

             </div>
          </div>
          <div class="collection_grid_template second">
            <div id="shopify-section-collection-template" class="shopify-section">
               <div class="grid__item sidebar-hidden">
                  <div class="collection-list">
                     <div class="grid-uniform grid-link__container col-main">
                        <header class="section-header section-header--large">
                           <div class="toolbar">

                            <input type="hidden" id="sortFilter" value="">
                            <input type="hidden" id="minPriceFilter" value="">
                            <input type="hidden" id="maxPriceFilter" value="">
                            <input type="hidden" id="brandFilter" value="">
                            <input type="hidden" id="materialFilter" value="">
                            <input type="hidden" id="colorFilter" value="">
                            <input type="hidden" id="categoryFilter" value="">

                              <div class="view-mode grid__item wide--one-third post-large--two-tenths large--two-tenths">
                                 <span class="btn active" onclick="showGridView()" title="Grid view" id="grid"><span class="fas fa-th-large"></span></span>  
                                 <span class="btn" onclick="showListView()" title="List view" id="list"><span class="fas fa-th-list"></span></span>  
                              </div>
                              <div class="grid__item wide--five-tenths post-large--eight-tenths large--eight-tenths right">
                                 <div class="filter-sortby grid__item ">
                                    <label for="sort-by">Sort by</label> 
                                    <input type="text" id="sort-by">
                                    <div class="sorting-section">
                                       <button class="btn dropdown-toggle" data-toggle="dropdown">
                                          <i class="fas fa-exchange-alt"></i>
                                          <span>Featured</span>
                                       </button>
                                       <ul class="dropdown-menu" role="menu">
                                          <li class="active"><a onclick="getSort('mannual')">Featured</a></li>
                                          <li><a onclick="getSort('p-low-high')">Price, low to high</a></li>
                                          <li><a onclick="getSort('p-high-low')">Price, high to low</a></li>
                                          <li><a onclick="getSort('ascending')"> A-Z</a></li>
                                          <li><a onclick="getSort('descending')">Z-A</a></li>
                                          <li><a onclick="getSort('created_at')">Latest</a></li>
                                          <!-- <li><a href="created-descending">Date, new to old</a></li> -->
                                          <!-- <li><a href="best-selling">Best Selling</a></li> -->
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </header>
                        <div class="products-grid-view showView" id="gridView" >
                           <ul>
                             @if(count($products)>0)
                             @foreach($products as $product)
                             <li class="grid__item item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item  medium--one-half small--one-half  " id="product-2204001599552">
                              <div class="products product-hover-15">
                                 <div class="product-container ">
                                    <a href="{{ url('product-detail/'.$product->slug ?? '') }}" class="grid-link">            
                                       <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="{{ $product->name ?? '' }}" class="productcat-image">
                                    </a>
                                    <div class="lSSlideOuter  vertical">
                                       <div class="lSSlideWrapper usingCss" style="height: 170px;">
                                          <ul class="vertical thumbs lightSlider lsGrab lSSlide" style="height: 120px;">
                                             <li class="thumb_items lslide active" style="height: 50px; margin-bottom: 10px;">
                                                <a data-thumb="{{ asset('storage/'.$product->cover ?? '') }}">
                                                   <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="{{ $product->name ?? '' }}" >
                                                </a>
                                             </li>
                                             <li class="thumb_items lslide" style="height: 50px; margin-bottom: 10px;">
                                                <a data-thumb="{{ asset('storage/'.$product->cover ?? '') }}">
                                                   <img src="{{ asset('storage/'.$product->cover ?? '') }}" alt="{{ $product->name ?? '' }}" >
                                                </a>
                                             </li>
                                          </ul>
                                          <div class="lSAction" style="display: none;"><a class="lSPrev"></a><a class="lSNext"></a></div>
                                       </div>
                                    </div>
                                    <div class="product_right_tag  ">
                                    </div>
                                    <div class="ImageWrapper">
                                       <div class="product-button">
                                          <a href="javascript:void(0)" id="backrest-cushion" class="quick-view-text" title="Quick View">                      
                                             <b class="far fa-eye"></b>
                                             <i>   Quick View</i>
                                          </a>       
                                          <form  method="post" class="gom variants clearfix" >                                    
                                             <input type="hidden" name="id" value="25424764534848">  
                                             <a class="add-cart-btn" title="Add to Cart" onclick="addToCart()">
                                                <b class="fas fa-shopping-cart"></b>
                                                <i>  Add to Cart</i>
                                             </a>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="product-detail">
                                    <a href="/collections/all/products/backrest-cushion" class="grid-link__title">{{ $product->name ?? '' }}</a> 
                                    <div class="grid-link__meta">
                                       <div class="product_price">
                                          <div class="grid-link__org_price" id="ProductPrice">
                                             <span> <span class="money" data-currency-usd="RS. {{$product->price}}">RS. {{$product->price}}</span></span>
                                          </div>
                                       </div>
                                       <span class="spr-badge" id="spr_badge_2204001599552" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
                                    </span>
                                 </div>
                                 <ul class="item-swatch color_swatch_Value">  
                                 </ul>
                              </div>
                           </div>
                           <style>
                              .product-container.product-hover-15 {position: relative;}
                              .lSSlideOuter.vertical {
                                 float: right;
                                 width: 20%;
                                 position: absolute;
                                 top: 20px;
                                 right: -100%;
                              }
                              .thumbs_items a {display:block;}
                           </style>
                        </li>
                        @endforeach
                        @endif

                     </ul>
                  </div>


                  <div class="product-list-view hideView" id="listView" >

                     <ul>


                        @if(count($products)>0)
                        @foreach($products as $product)



                        <li class="product-list products grid__item item-row " id="product-2204002222144">
                         <div class="grid__item wide--one-third  post-large--one-third  large--one-third medium--one-third small--one-third">
                          <div class="product-container">
                           <a href="{{ url('product-detail/'.$product->slug ?? '') }}" class="grid-link">
                            <div class="ImageOverlayCa"></div>
                            <div class="grid-link__image grid-link__image--product">

                              <div class="reveal"> 
                               <span class="product-additional">      
                                <img src="{{ asset('storage/'.$product->cover ?? '') }}" class="featured-image" alt="{{ $product->name ?? '' }}" style="height: 300px; width: 100%">
                             </span>
                             <img src="{{ asset('storage/'.$product->cover ?? '') }}" class="hidden-feature_img" alt="{{ $product->name ?? '' }}" style="height: 300px; width: 100%">
                          </div> 




                       </div>
                    </a>
                    <div class="ImageWrapper">
               <!--        <div class="product-button"> 

                       <a title="Quick View" href="javascript:void(0)" id="umang-pillow" class="quick-view-text">                      
                        <i class="far fa-eye" aria-hidden="true"></i>
                     </a>       



                     <a title="Product Link" href="/products/umang-pillow">                      
                        <i class="fas fa-link" aria-hidden="true"></i>
                     </a>       




                     <a href="#" class="compare action-home4" data-pid="umang-pillow" title="Compare Products" data-original-title="Compare product">  <i class="far fa-chart-bar"></i></a>

                  </div> -->
               </div>
            </div>
         </div>
         <div class="grid__item wide--two-thirds post-large--two-thirds  large--two-thirds  medium--two-thirds  small--two-thirds ">
           <div class="product-detail">
            <a href="/collections/sale-off/products/umang-pillow" class="grid-link__title">{{ $product->name ?? '' }}</a>     
            <div class="grid-link__meta">
             <div class="product_price">

              <div class="grid-link__org_price">
               <span class="money">RS. {{ $product->price ?? '' }}</span>
            </div>        

         </div>      
         <span class="spr-badge" id="spr_badge_2204002222144" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i><i class="spr-icon spr-icon-star-empty"></i></span><span class="spr-badge-caption"></span>
      </span>

   </div>
   <p>

      <?php echo $product->description ?? ''; ?>

   </p>

   <ul class="item-swatch color_swatch_Value">  

   </ul>
   <form action="/cart/add" method="post" class="variants clearfix" id="cart-form-2204002222144">                                    
    <input type="hidden" name="id" value="25423989801024">  
    <a class="add-cart-btn btn" title="Add to Cart">
     <i class="fas fa-cart-plus" aria-hidden="true"></i> Add to Cart
  </a>
</form>  


<div class="add-to-wishlist">     
   <div class="show">
    <div class="default-wishbutton-umang-pillow loading"><a title="Add to wishlist" class="add-in-wishlist-js btn" href="umang-pillow"><i class="far fa-heart"></i><span class="tooltip-label">Add to wishlist</span></a></div>
    <div class="loadding-wishbutton-umang-pillow loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="umang-pillow"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
    <div class="added-wishbutton-umang-pillow loading" style="display: none;"><a title="View Wishlist" class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fas fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
 </div>
</div>


</li>


@endforeach

@endif  





</ul></div>
</div>
</li>







</ul>



</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="dt-sc-hr-invisible-large"></div>
</main>
@endsection