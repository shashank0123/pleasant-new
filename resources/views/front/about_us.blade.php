@extends('layouts.front.app')
@section('content')
<main class="main-content">
	<nav class="breadcrumb" aria-label="breadcrumbs">
		<h1>{{ $content->title ?? 'About Us' }}</h1>
		<a href="/" title="Back to the frontpage">Home</a>
		<span aria-hidden="true" class="breadcrumb__sep">/</span>
		<span>{{ $content->title ?? 'About Us' }}</span>
	</nav>
	<div class="dt-sc-hr-invisible-large"></div>
	<div class="wrapper">
		<div class="grid-uniform">
			<div class="grid__item">
				<div class="container-bg">
					<div class="grid">
						<div class="grid__item">
							<div id="shopify-section-about" class="shopify-section">
								<div data-section-id="about" data-section-type="About section" class="about-us">


										<div class="grid-uniform block7">
											<?php
												echo $content->description ?? '';
											?>
										</div>


									<!-- Top Sections -->
									<!-- <div class="grid-uniform block7">
										<div class="container">
											<div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-whole small-grid__item wow fadeInUp">
												<div class="grid-data-box left-icon">

													<div class="row">
														<div class="col-sm-3">

															<div class="grid-data-icon">
																<img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/icon-1_65d497ac-2754-41e5-a4bd-ed8d40a3d639_300x300.png?v=1549436654" class="attachment-full" alt="01">
															</div>

														</div>
														<div class="col-sm-9">

															<div class="grid-data-details">
																<div class="number-icon">
																	<h2>01</h2>
																</div>
																<h5>Choose your Material</h5>
																<p>Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat.</p>
															</div>
														</div>
													</div>
												</div>
												<div class="grid-data-box left-icon">
													<div class="row">
														<div class="col-sm-3">
															<div class="grid-data-icon">
																<img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/icon-2_300x300.png?v=1549436807" class="attachment-full" alt="02">
															</div>

														</div>
														<div class="col-sm-9">
															<div class="grid-data-details">
																<div class="number-icon">
																	<h2>02</h2>
																</div>
																<h5>Easy to handel</h5>
																<p>Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat.</p>
															</div>

														</div>
													</div>
												</div>
											</div>
											<div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-whole small-grid__item wow fadeInUp">
												<div class="grid-data-box middle">
													<div class="mid-grid-data-details" style="padding: 45px 20px 15px;
													font-size: 14px;
													text-align: center;">
													<h3>Product We Use</h3>
													<p style="padding: 15px 20px 25px;
													font-size: 14px;
													text-align: center;">Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt.</p>
												</div>
											</div>
										</div>
										<div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-whole small-grid__item wow fadeInUp">
											<div class="grid-data-box right-icon">
												<div class="row">
													<div class="col-sm-9">
														<div class="grid-data-details ">
															<div class="number-icon">
																<h2>03</h2>
															</div>
															<h5>Light weight</h5>
															<p>Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat.</p>
														</div>

													</div>
													<div class="col-sm-3">
														<div class="grid-data-icon">
															<img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/icon-4_300x300.png?v=1549437063" class="attachment-full" alt="03">
														</div>

													</div>
												</div>
											</div>
											<div class="grid-data-box right-icon">
												<div class="row">
													<div class="col-sm-9">
														<div class="grid-data-details right-icon">
															<div class="number-icon">
																<h2>04</h2>
															</div>
															<h5>Excellent Support</h5>
															<p>Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat.</p>
														</div>

													</div>
													<div class="col-sm-3">
														<div class="grid-data-icon">
															<img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/icon-3_300x300.png?v=1549436993" class="attachment-full" alt="04">
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								
 -->

								<!-- Our Team -->
								
								<!-- <div class="grid-uniform client-section">
									<div class="container">
										<div class="main-title">
											<h2 style="color:#11423b">Our Clients</h2>
											<div class="title-sep" style="background:#11423b"></div>
										</div>
										<div class="grid__item">
											<div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item ">
												<div class="client-img">
													<a href="">  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/clien-1_4a3eef24-ac03-48d3-9058-dce917dbd6aa.jpg?v=1549436189" alt=""></a>
												</div>
											</div>
											<div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item ">
												<div class="client-img">
													<a href="">  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/clien-2_3e224c4e-b16f-4fa6-9019-d0a969b23f9d.jpg?v=1549436199" alt=""></a>
												</div>
											</div>
											<div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item ">
												<div class="client-img">
													<a href="">  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/clien-3_b31a62d1-5713-4f82-b1b4-50104853ee71.jpg?v=1549436205" alt=""></a>
												</div>
											</div>
											<div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item ">
												<div class="client-img">
													<a href="">  <img src="//cdn.shopify.com/s/files/1/0167/7249/7472/files/clien-4_524c0b0c-0ea9-4328-8da2-9cab41e3adaf.jpg?v=1549436211" alt=""></a>
												</div>
											</div>
										</div>
									</div>
								</div> -->
							</div>
							<style>
								.about-us .team h5 { text-transform:inherit; }
								.about-us  .block7 h2 {color: #676e6d;}
								.about-us  .block7 h5 {color: #11423b;}
								.about-us   .block7 p {color: #676e6d;}
								.about-us   .block7 .mid-grid-data-details h3, .about-us .block7 .mid-grid-data-details h5  {color: #11423b;}
								.about-us  .block7 .mid-grid-data-details p,.about-us  .block7 .mid-grid-data-details h6  {color: #676e6d;}
								.about-us .block7 .number-icon {border-bottom: 1px dashed #11423b;}
								.about-us  .grid-data-box.middle {background:#69e0ff;}
								.about-us  .section-six  .gallery-overlay{   background-color:rgba(105, 224, 255, 0);}
								.about-us .section-six .gallery-img:hover .gallery-overlay{background-color:rgba(105, 224, 255, 0.9);}
								.about-us  .section-six  .overlay-link h5{color:#11423b ;}
								.about-us  .section-six  .overlay-link h4{color:#ffffff ;}
								.about-us .team-detail {background-color:rgba(0,0,0,0.7);
									position: absolute;
									bottom: 0;
									left : 5%;
									width:90%;
									text-align: center;

								}
									.team-detail h4 {
										padding-top: 20px;
									}

									.team-detail ul {
										list-style-type: none; padding: 10%;
										float: left;
										width: 100%;
										text-align: center;
									}

									.team-detail ul li {
										list-style-type: none; padding: 5%;
										float: left;
									}

									.team-detail ul li .fab{
										padding: 7px;
									}

									.about-us .team-detail ul li a {
    border: 1px solid #fff !important;
    color: #fff !important;
    height: 35px;
    width: 35px;
}
								.about-us .team-detail a { border: 1px solid #11423b;color: #11423b;}
								.about-us .team-detail a:hover{ border: 1px solid #11423b; color: #ffffff;background:#11423b;}
								.about-us .team h4 , .about-us .team h5{color:#ffffff;}
								.about-us .team:hover .team-detail h5,.about-us .team:hover .team-detail h4{color:#11423b;}
								.about-us .team:hover .team-detail {background:#69e0ff;}
								.about-us .section-one .right-section{border-left: 1px solid rgba(0,0,0,0);}
								.about-us .icon-section .icon{background:#11423b;}
								.about-us .icon-section .icon{color:#ffffff;}
								.about-us .icon-section  .icon-heading h3{color:#11423b;}
								.about-us .icon-desc p{color:#333333;}
								.about-us .section-one::after { background: #69e0ff;}
								.about-us .about-btn:hover{background:#ffffff!important;color:#11423b!important;}

								.grid-data-details h5 { padding-top: 15px; font-size: 18px }
								.grid-data-details{
									padding: 20px 20px 10px;
								}

								.grid-data-details{
									font-size: 14px ;
								}
							</style>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</main>
@endsection