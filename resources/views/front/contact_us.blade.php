@extends('layouts.front.app')

@section('content')


<main class="main-content">  

    

<nav class="breadcrumb" aria-label="breadcrumbs">


  
  <h1>Contact us</h1>
  <a href="/" title="Back to the frontpage">Home</a>

  <span aria-hidden="true" class="breadcrumb__sep">/</span>
  <span>Contact us</span>

  
</nav>
<div class="dt-sc-hr-invisible-large"></div> 
  


    
      
      
      <div class="grid-uniform">
        <div class="grid__item">  
          <div class="container-bg"> 
            

<div id="map"><iframe width="960" height="578" style="border:0;overflow:hidden;" src="https://maps.google.co.in/?ie=UTF8&amp;t=m&amp;ll=-37.823006,144.977388&amp;spn=0.02034,0.042915&amp;z=14&amp;output=embed"></iframe> </div>


        
<div class="contact-address">
  <div class="wrapper">
    <div class="dt-sc-hr-invisible-large"></div>

    <div class="grid__item">
      
      <ul>
        <li class="grid__item wide--one-third post-large--one-third large--one-third">
          <div class="icon">
            <i class="fas fa-phone"></i>
          </div>
           
          <h4>Talk to us</h4>
          <p>  <b>Toll-Free: </b> 0803 - 080 - 3081 <br><b>Fax: </b> 0803 - 080 - 3082</p><mark></mark> 
          
        </li>
        <li class="grid__item wide--one-third post-large--one-third large--one-third">
          
          <div class="icon">
            <i class="far fa-envelope"></i>
          </div>
          <h4>Contact Us</h4>
          <p> 
            <a title="" href="">buddhathemes@somemail.com</a><br><a title="" href="">support@somemail.com</a>
          </p> 
          
        </li>
         
        <li class="address grid__item wide--one-third post-large--one-third large--one-third"> 
          <div class="icon">
            <i class="fas fa-location-arrow"></i>
          </div> 
          <h4>Location</h4>
          <p> No: 58 A, East Madison Street,<br> Baltimore, MD, USA
4508<br></p><mark></mark> 

        </li>
        
      </ul>



      
    </div>
    <div class="dt-sc-hr-invisible-large"></div>

    <div class="grid__item">
      <div class="contact-form-section">
        <form method="post" action="{{ url('contact-form-submit') }}" accept-charset="UTF-8" class="contact-form">

        	@csrf
        
        
        <label for="ContactFormName" class="label--hidden">Name</label>
        <input type="text" id="ContactFormName" name="name" value="{{ auth()->user()->name ?? '' }}" placeholder="Name" autocapitalize="words" required="required">

        <label for="ContactFormEmail" class="label--hidden">Email</label>
        <input type="email" id="ContactFormEmail" name="email" placeholder="Email *" autocorrect="off" autocapitalize="off" value="{{ auth()->user()->email ?? '' }}" required="required">
        
        <label for="ContactFormSub" class="label--hidden">translation missing: en.contact.form.phone</label>
        <input type="text" id="ContactFormSub" name="subject" placeholder="Subject" autocapitalize="words" value="" required="required">

        <label for="ContactFormMessage" class="label--hidden">Message</label>
        <textarea rows="7" id="ContactFormMessage" name="message" placeholder="Message" required="required"></textarea>

        <button type="submit" class="btn" style="box-shadow: 3px 0 0 0 #e7eeef, 3px 3px 0 0 #e7eeef, 0 3px 0 0 #e7eeef; margin-top: 20px; border: 1px solid #e7eeef">Send</button>

        </form>
      </div>

    </div>


  </div>
</div>

		          
          </div>  
        </div>
      </div>
      
      
    
    <div class="dt-sc-hr-invisible-large"></div>
    
  </main>


@endsection