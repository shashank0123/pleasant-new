@extends('layouts.front.app')
@section('content')
<main class="main-content">
   <nav class="breadcrumb" aria-label="breadcrumbs">
      <h1>Blogs</h1>
      <a href="/" title="Back to the frontpage">Home</a>
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span><a href="{{ url('blogs') }}" title="All Blogs">Blogs</a></span>
      @if(!empty($tag))
      <span aria-hidden="true" class="breadcrumb__sep">/</span>
      <span>{{$tag}}</span>
      @endif
   </nav>
   <div class="dt-sc-hr-invisible-large"></div>
   <div class="wrapper">
      <div class="grid-uniform">
         <div class="grid__item">
            <div class="container-bg">
               <div class="blog-design-4">
                  <div class="blog-section position-change">
                     <aside class="sidebar left-sidebar grid__item wide--one-fifth post-large--one-quarter">
                        <div id="shopify-section-blog-sidebar-article" class="shopify-section">
                           <div class="widget recent_article">
                              <h4>Recent Articles<span class="right"></span></h4>
                              <ul>
                                 @if(count($recent_articles))
                                 @foreach($recent_articles as $article)
                                 <li>
                                    <div class="artical-image">
                                       <a href="{{ url('blog/'.$article->slug ?? '') }}" title="{{ $article->title ?? '' }}"><img src="{{ asset('storage/'.$article->cover ?? '') }}" alt="{{ $article->title ?? '' }}" class="article__image" id="recentArticle"></a>
                                    </div>
                                    <div class="article-detail">
                                       <h5>
                                          <a href="{{ url('blog/'.$article->slug ?? '') }}">{{ $article->title ?? '' }}</a>
                                       </h5>
                                       <p>{{ substr($article->description ?? '',0,50) }}...</p>
                                    </div>
                                 </li>
                                 @endforeach
                                 @endif
                              </ul>
                           </div>
                        </div>
                        <!-- <div id="shopify-section-blog-sidebar-tags" class="shopify-section">
                           <div class="widget widget_categories">
                             
                             <h4>Categories<span class="right"></span></h4>
                             <ul>
                               
                               
                               <li><a href="/blogs/news/tagged/bed" title="Show articles tagged Bed">Bed</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/checking" title="Show articles tagged Checking">Checking</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/comfort" title="Show articles tagged Comfort">Comfort</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/family" title="Show articles tagged Family">Family</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/fashion" title="Show articles tagged Fashion">Fashion</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/love" title="Show articles tagged Love">Love</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/mattress" title="Show articles tagged Mattress">Mattress</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/pillow" title="Show articles tagged Pillow">Pillow</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/relax" title="Show articles tagged Relax">Relax</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/royal" title="Show articles tagged Royal">Royal</a></li>
                               
                               
                               
                               <li><a href="/blogs/news/tagged/softness" title="Show articles tagged Softness">Softness</a></li>
                               
                               
                             </ul>
                             
                           </div>
                           
                           
                            </div> -->
                       <!--  <div id="shopify-section-blog-sidebar-instagram" class="shopify-section instagram-block">
                           <div class="grid__item clearfix instagram widget" style="">
                              <h4 style="color:#11423b;"><span> Instagram Feed</span></h4>
                              <div id="insta-feed">
                                 <a href="#0-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22794110_445898755803873_8913170695370833920_n.jpg?_nc_cat=104&amp;_nc_sid=8ae9d6&amp;_nc_ohc=dX9-W_kCMWwAX_GYpD-&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=3eee876066bf6902624a2f39247c9716&amp;oe=5EFEBC5E" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22794110_445898755803873_8913170695370833920_n.jpg?_nc_cat=104&amp;_nc_sid=8ae9d6&amp;_nc_ohc=dX9-W_kCMWwAX_GYpD-&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=3eee876066bf6902624a2f39247c9716&amp;oe=5EFEBC5E" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="0-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22794110_445898755803873_8913170695370833920_n.jpg?_nc_cat=104&amp;_nc_sid=8ae9d6&amp;_nc_ohc=dX9-W_kCMWwAX_GYpD-&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=3eee876066bf6902624a2f39247c9716&amp;oe=5EFEBC5E" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#11-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#1-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal31z1Hh02/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#1-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22709299_2152828954944675_6033853057110900736_n.jpg?_nc_cat=104&amp;_nc_sid=8ae9d6&amp;_nc_ohc=uuqpzYXsu64AX--p6pG&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=393e6a617fa31e806302583f6b235df8&amp;oe=5EFF08C7" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22709299_2152828954944675_6033853057110900736_n.jpg?_nc_cat=104&amp;_nc_sid=8ae9d6&amp;_nc_ohc=uuqpzYXsu64AX--p6pG&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=393e6a617fa31e806302583f6b235df8&amp;oe=5EFF08C7" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="1-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22709299_2152828954944675_6033853057110900736_n.jpg?_nc_cat=104&amp;_nc_sid=8ae9d6&amp;_nc_ohc=uuqpzYXsu64AX--p6pG&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=393e6a617fa31e806302583f6b235df8&amp;oe=5EFF08C7" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#0-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#2-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3zD9HqRE/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#2-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22710422_221064848430906_5389012139077795840_n.jpg?_nc_cat=109&amp;_nc_sid=8ae9d6&amp;_nc_ohc=YUClNNNuOaEAX9NpeDU&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=91fa42d2d70f96fe1c064ee4a2dc6f71&amp;oe=5EFD39FB" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710422_221064848430906_5389012139077795840_n.jpg?_nc_cat=109&amp;_nc_sid=8ae9d6&amp;_nc_ohc=YUClNNNuOaEAX9NpeDU&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=91fa42d2d70f96fe1c064ee4a2dc6f71&amp;oe=5EFD39FB" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="2-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710422_221064848430906_5389012139077795840_n.jpg?_nc_cat=109&amp;_nc_sid=8ae9d6&amp;_nc_ohc=YUClNNNuOaEAX9NpeDU&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=91fa42d2d70f96fe1c064ee4a2dc6f71&amp;oe=5EFD39FB" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#1-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#3-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3ptgH7Dg/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#3-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22710515_441558126241416_6328376406459809792_n.jpg?_nc_cat=108&amp;_nc_sid=8ae9d6&amp;_nc_ohc=ZpBN1_2RbqEAX_p-qwo&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=ed96051aa17e57ca8ac2941294c6c929&amp;oe=5EFF306A" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710515_441558126241416_6328376406459809792_n.jpg?_nc_cat=108&amp;_nc_sid=8ae9d6&amp;_nc_ohc=ZpBN1_2RbqEAX_p-qwo&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=ed96051aa17e57ca8ac2941294c6c929&amp;oe=5EFF306A" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="3-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710515_441558126241416_6328376406459809792_n.jpg?_nc_cat=108&amp;_nc_sid=8ae9d6&amp;_nc_ohc=ZpBN1_2RbqEAX_p-qwo&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=ed96051aa17e57ca8ac2941294c6c929&amp;oe=5EFF306A" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#2-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#4-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3ip4n7lO/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#4-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22802064_1422246701224409_8705570247073071104_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KXxUsdJCzc4AX8BY_di&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=5761eff6fef6fcdf4e0f2fad40ac8d40&amp;oe=5EFF0C98" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22802064_1422246701224409_8705570247073071104_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KXxUsdJCzc4AX8BY_di&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=5761eff6fef6fcdf4e0f2fad40ac8d40&amp;oe=5EFF0C98" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="4-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22802064_1422246701224409_8705570247073071104_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=KXxUsdJCzc4AX8BY_di&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=5761eff6fef6fcdf4e0f2fad40ac8d40&amp;oe=5EFF0C98" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#3-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#5-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3ckrHh-J/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#5-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22801981_109350606449958_6270976148528693248_n.jpg?_nc_cat=103&amp;_nc_sid=8ae9d6&amp;_nc_ohc=YOH2WZPfxjYAX99Bst8&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=406b442ea871e4872e66cd60e0d383a5&amp;oe=5EFD7B34" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22801981_109350606449958_6270976148528693248_n.jpg?_nc_cat=103&amp;_nc_sid=8ae9d6&amp;_nc_ohc=YOH2WZPfxjYAX99Bst8&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=406b442ea871e4872e66cd60e0d383a5&amp;oe=5EFD7B34" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="5-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22801981_109350606449958_6270976148528693248_n.jpg?_nc_cat=103&amp;_nc_sid=8ae9d6&amp;_nc_ohc=YOH2WZPfxjYAX99Bst8&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=406b442ea871e4872e66cd60e0d383a5&amp;oe=5EFD7B34" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#4-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#6-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3Yz8HzPn/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#6-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22639401_137911830107246_6610967168429326336_n.jpg?_nc_cat=102&amp;_nc_sid=8ae9d6&amp;_nc_ohc=amOUpz792oMAX9J3Gqc&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=b6206650f9dfb28cfe9603b689d2c140&amp;oe=5EFE3441" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22639401_137911830107246_6610967168429326336_n.jpg?_nc_cat=102&amp;_nc_sid=8ae9d6&amp;_nc_ohc=amOUpz792oMAX9J3Gqc&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=b6206650f9dfb28cfe9603b689d2c140&amp;oe=5EFE3441" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="6-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22639401_137911830107246_6610967168429326336_n.jpg?_nc_cat=102&amp;_nc_sid=8ae9d6&amp;_nc_ohc=amOUpz792oMAX9J3Gqc&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=b6206650f9dfb28cfe9603b689d2c140&amp;oe=5EFE3441" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#5-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#7-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3VS5nuno/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#7-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22638796_438133363251071_5963685025344913408_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=rmw5DKjVQfgAX-7BazC&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=1287038b83e1c40fcb79348ca2a42c11&amp;oe=5EFB89CC" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22638796_438133363251071_5963685025344913408_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=rmw5DKjVQfgAX-7BazC&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=1287038b83e1c40fcb79348ca2a42c11&amp;oe=5EFB89CC" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="7-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22638796_438133363251071_5963685025344913408_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=rmw5DKjVQfgAX-7BazC&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=1287038b83e1c40fcb79348ca2a42c11&amp;oe=5EFB89CC" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#6-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#8-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3SFgH3R7/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#8-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22710861_1190900621011808_2905790828829999104_n.jpg?_nc_cat=111&amp;_nc_sid=8ae9d6&amp;_nc_ohc=07M_MxvlJLcAX-vpRn-&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=0dd951f4b0756551a7d1986d61380ec1&amp;oe=5EFC8171" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710861_1190900621011808_2905790828829999104_n.jpg?_nc_cat=111&amp;_nc_sid=8ae9d6&amp;_nc_ohc=07M_MxvlJLcAX-vpRn-&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=0dd951f4b0756551a7d1986d61380ec1&amp;oe=5EFC8171" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="8-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710861_1190900621011808_2905790828829999104_n.jpg?_nc_cat=111&amp;_nc_sid=8ae9d6&amp;_nc_ohc=07M_MxvlJLcAX-vpRn-&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=0dd951f4b0756551a7d1986d61380ec1&amp;oe=5EFC8171" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#7-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#9-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3OAcnjS5/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#9-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22639187_132294454096062_9035780934950977536_n.jpg?_nc_cat=100&amp;_nc_sid=8ae9d6&amp;_nc_ohc=hf9CYYjJixcAX9zQ-yX&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=9481884318593a300e62d407d3231e3a&amp;oe=5EFB7AA7" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22639187_132294454096062_9035780934950977536_n.jpg?_nc_cat=100&amp;_nc_sid=8ae9d6&amp;_nc_ohc=hf9CYYjJixcAX9zQ-yX&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=9481884318593a300e62d407d3231e3a&amp;oe=5EFB7AA7" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="9-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22639187_132294454096062_9035780934950977536_n.jpg?_nc_cat=100&amp;_nc_sid=8ae9d6&amp;_nc_ohc=hf9CYYjJixcAX9zQ-yX&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=9481884318593a300e62d407d3231e3a&amp;oe=5EFB7AA7" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#8-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#10-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3KGLncCd/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#10-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22710423_892693164220036_4749682437179572224_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=zqemE72-MDgAX90zV8R&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=1a67a1eda228ded7b9041d6a17bccc73&amp;oe=5EFEFF58" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710423_892693164220036_4749682437179572224_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=zqemE72-MDgAX90zV8R&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=1a67a1eda228ded7b9041d6a17bccc73&amp;oe=5EFEFF58" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="10-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22710423_892693164220036_4749682437179572224_n.jpg?_nc_cat=101&amp;_nc_sid=8ae9d6&amp;_nc_ohc=zqemE72-MDgAX90zV8R&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=1a67a1eda228ded7b9041d6a17bccc73&amp;oe=5EFEFF58" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#9-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#11-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal3GNeHaTF/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                                 <a href="#11-insta-feed">
                                    <div class="instafeed-container" style="width:25.000000%;padding-top:25.000000%;">
                                       <img class="js-lazy-image js-lazy-image--handled" style="width:100%;height:100%;" src="https://scontent.cdninstagram.com/v/t51.2885-15/22639198_127432138015117_626877539440132096_n.jpg?_nc_cat=103&amp;_nc_sid=8ae9d6&amp;_nc_ohc=HycbgzhzTq8AX_lM2or&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=ac73c5163fa6edefe1e04fffdcaa02fb&amp;oe=5EFC2132" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22639198_127432138015117_626877539440132096_n.jpg?_nc_cat=103&amp;_nc_sid=8ae9d6&amp;_nc_ohc=HycbgzhzTq8AX_lM2or&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=ac73c5163fa6edefe1e04fffdcaa02fb&amp;oe=5EFC2132" alt="instagram post image">
                                       <div style="width:100%;height:100%;" class="instafeed-overlay "></div>
                                    </div>
                                 </a>
                                 <a href="#_" class="instafeed-lightbox" id="11-insta-feed">
                                    <div class="lightbox-instagram">
                                       <div class="image"><img class="js-lazy-image" src="//instafeed.nfcube.com/assets/img/pixel.gif" data-src="https://scontent.cdninstagram.com/v/t51.2885-15/22639198_127432138015117_626877539440132096_n.jpg?_nc_cat=103&amp;_nc_sid=8ae9d6&amp;_nc_ohc=HycbgzhzTq8AX_lM2or&amp;_nc_ht=scontent.cdninstagram.com&amp;oh=ac73c5163fa6edefe1e04fffdcaa02fb&amp;oe=5EFC2132" alt="video thumbnail"></div>
                                       <div class="description">
                                          <div class="header"><object>
                                 <a href="https://www.instagram.com/iamdesigning6428" target="_blank" rel="noopener"><img src="https://instagram.fevn1-3.fna.fbcdn.net/v/t51.2885-19/44884218_345707102882519_2446069589734326272_n.jpg?_nc_ht=instagram.fevn1-3.fna.fbcdn.net&amp;_nc_ohc=-dngjj927I8AX-S1si8&amp;oh=34261929dd056cefeec26ed4c6c92279&amp;oe=5EFFE60F&amp;ig_cache_key=YW5vbnltb3VzX3Byb2ZpbGVfcGlj.2" class="profile-picture js-lazy-image" data-src="false" alt="instagram user picture"></a></object><object class="name-section"><a class="fullname" href="https://www.instagram.com/iamdesigning6428/" target="_blank" rel="noopener"><span class="username">iamdesigning6428</span></a></object></div><hr><div class="box-content"><div class="sub-header"><div class="post-engagement"></div><div class="arrows"><object><a href="#10-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="previous image"></a></object><object><a href="#0-insta-feed"><img src="//instafeed.nfcube.com/assets/img/pixel.gif" alt="next image"></a></object></div></div> <div class="instafeed-caption"></div><div class="post-date">OCTOBER 23 • <object><a href="https://www.instagram.com/p/Bal29GwHy9a/" target="_blank" rel="noopener" class="follow">View on Instagram</a></object></div></div></div></div></a>
                              </div>
                           </div>
                           <link href="//cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/slick.scss?v=14012654398117444897" rel="stylesheet" type="text/css" media="all">
                           <script src="//cdn.shopify.com/s/files/1/0167/7249/7472/t/4/assets/instagram.js?v=12631321077811964863" type="text/javascript"></script>
                        </div> -->

                        @if(count($bestDeals)>0)
                        <div id="shopify-section-blog-sidebar-deals" class="shopify-section">
                           <div data-section-id="blog-sidebar-deals" data-section-type="blog-sidebar-deals" class="home-sidebar-deals">
                              <div class="widget widget_top_rated_products" style="background:#fff">
                                 <h4><span>Best Deals</span></h4>
                                 <ul class="no-bullets sidebar-deal-products owl-carousel owl-theme owl-loaded owl-drag">
                                    @foreach($bestDeals as $product)
                                          <div class="owl-item active" style="width: 203px;">
                                             <li class="products">
                                                <div class="product-container"> 
                                                   <a class="thumb grid__item" href="{{ url('product-detail/'.$product->slug ?? '') }}">                                          
                                                   <img alt="{{ $product->name ?? '' }}" src="{{ asset('storage/'.$product->cover ?? '') }}">                                              
                                                   </a>
                                                </div>
                                                <div class="top-products-detail product-detail grid__item">
                                                   <a class="grid-link__title" href="{{ url('product-detail/'.$product->slug ?? '') }}"> {{ $product->name ?? '' }} </a>
                                                   <div class="top-product-prices grid-link__meta">
                                                      <div class="product_price">
                                                         <div class="grid-link__org_price">
                                                            <span class="money" data-currency-usd="RS. {{ $product->price ?? '' }}">RS. {{ $product->price ?? '' }}</span>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <span class="spr-badge" id="spr_badge_2204003729472" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i><i class="spr-icon spr-icon-star"></i></span><span class="spr-badge-caption">1 review</span>
                                                   </span>
                                                </div>
                                             </li>
                                          </div>

                                          @endforeach
                                       
                                    
                                 </ul>
                                 <div class="home_sidebar--deal-nav">
                                    <div class="owl-prev disabled"> <a class="prev btn active"><i class="fas fa-chevron-left icons"></i></a></div>
                                    <div class="owl-next"><a class="next btn"><i class="fas fa-chevron-right icons"></i></a></div>
                                 </div>
                              </div>
                              <style>
                                 .home-sidebar-deals .lof-clock-timer-detail-single li { background:#000;border:1px solid #000;color:#000;}
                              </style>
                           </div>
                           <script type="text/javascript">
                              $(document).ready(function(){
                                  var homeSidedeals = $(".sidebar-deal-products");
                                   homeSidedeals.owlCarousel({
                                   items:1,
                                      loop:true,              
                                      dots: false,
                                     nav:true,       
                                      navContainer: ".home_sidebar--deal-nav",
                                      navText: [' <a class="prev btn active"><i class="fas fa-chevron-left icons"></i></a>','<a class="next btn"><i class="fas fa-chevron-right icons"></i></a>'],
                                      responsive:{
                                        0:{
                                          items: 1
                                        },
                                        600:{
                                          items:1
                                        },
                                        700:{
                                          items:1
                                        },
                                        
                                        1000:{
                                          items: 1
                                        }
                                      }
                               			});
                                     });
                           </script>
                        </div>
                        @endif

                     </aside>
                     <div class="second">
                        <div id="shopify-section-blog" class="shopify-section">
                           <div class="grid-uniform">
                              <div class="grid__item second grid__item wide--four-fifths post-large--three-quarters">
                                 <div class="blog_grid_section">
                                 	@if(!empty($blogs))
                                 	@php $i = 0 @endphp
                                 	@foreach($blogs as $blog)
                                    <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
                                       <div class="article">
                                          <div class="article-img">
                                             <a href="{{ url('blog/'.$blog->slug ?? '') }}" title="{{ $blog->title ?? '' }}"><img src="{{ asset('storage/'.$blog->cover ?? '') }}" alt="{{ $blog->title ?? '' }}" class="article__image" id="article__image"></a>
                                          </div>
                                          <div class="blog-description">
                                             <div class="blogs-sub-title">
                                                <p class="blog-date">
                                                   <i class="far fa-calendar"></i> <span data-datetime="2019-02-14"><span class="date">{{ date('M d,Y', strtotime(explode(' ',$blog->created_at)[0])) }}</span></span>             
                                                </p>
                                                <p class="comments-count"><i class="far fa-comment"></i>{{$comments[$i]}} Comments</p>
                                                <p class="author">
                                                   <i class="far fa-user"></i>
                                                   <span> {{ $blog->author }}</span>
                                                </p>
                                             </div>
                                             <div class="home-blog-content blog-detail">
                                                <h4><a href="{{ url('blog/'.$blog->slug ?? '') }}">{{ $blog->title ?? '' }}</a></h4>
                                                <p>  {{ substr($blog->description ?? '',0,200) }}...</p>
                                                @php
                                                $tags = array();
                                                if(!empty($blog->tags))
                                                $tags = explode(',',$blog->tags) ;
                                                
                                                @endphp
                                                @if(count($tags)>0)
                                                <div class="blog-tag">
                                                   <i class="fas fa-tag"></i>
                                                   @foreach($tags as $tag)
                                                   <a class="blog-tags" href="{{ url('blogs/'.$tag) }}">{{$tag}}</a>/
                                                   @endforeach
                                                </div>
                                                @endif
                                             </div>
                                             <div class="blog-btn">
                                                <a class="btn" href="{{ url('blog/'.$blog->slug ?? '') }}">Read more<i class="fas fa-angle-double-right"></i></a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                    @endif
                                 
                                    <div class="text-center">
                                       {{ $blogs->links() }}
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <style>
                              .blog_grid_section h4 a { color:#11423b; }
                              .blog_grid_section .blog-description:hover  h4 a { color:#69e0ff; }
                              .blog_grid_section .blog-date { background:;color:#333333; }
                              .blog_grid_section .blog-description { background:; }
                              .blog_grid_section .blog-description:hover { background:; }
                              .blog_grid_section .blog-tag a:hover {color:#11423b; }
                              .blog_grid_section .blogs-sub-title .author { color:#333333; }
                              .blog_grid_section .blog-tag a,.blog_grid_section .blog-tag,.blog_grid_section .blog-tag i  {color:#69e0ff;} 
                              .blog_grid_section .blog-tag a:hover {color:#11423b; }
                              .blog_grid_section .comments-count { color:#333333; }
                              .blog_grid_section .blog-description:hover .blogs-sub-title .author,.blog_grid_section .blog-description:hover .comments-count,.blog_grid_section .blog-description:hover .blog-tag a,.blog_grid_section .blog-description:hover .home-blog-content.blog-detail p,.blog_grid_section .blog-description:hover .blog-tag i { color: !important; }
                              .blog_grid_section .blog-description:hover .comments-count:before { background: !important; }
                           </style>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-hr-invisible-large"></div>
</main>
@endsection