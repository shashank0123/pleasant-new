@extends('layouts.admin.app')

@section('content')
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Dashboard</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    








            <div class="container">
                <div class="row">
                    <div class="col-sm-3 ">
                        <div class="card1">
                            {{$totalCustomers ?? '0'}} Customers
                        </div>
                    </div>

                    <div class="col-sm-3 ">
                        <div class="card2">
                         {{$totalOrders ?? '0'}} Orders
                     </div>
                 </div>

                 <div class="col-sm-3 ">
                    <div class="card3">
                        {{$totalProducts ?? '0'}} Total Products
                    </div>
                </div>

                <div class="col-sm-3 ">
                    <div class="card4">
                        {{$totalBrands ?? '0'}} Brands
                    </div>
                </div>

            </div>

            <div class="row">

                <!-- Top 5 customers -->
                <div class="col-sm-6">
                    <h4>New Customers</h4>
                    <table class="table table-colored">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($customers)>0)
                            @php $c=1 @endphp
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{$c++}}</td>
                                <td>{{$customer->name ?? ''}}</td>
                                <td>{{$customer->email ?? ''}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <a href="" >View All ... </a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>

                <!-- Top 5 Products -->
                <div class="col-sm-6">
                    <h4>New Products</h4>
                    <table class="table table-colored">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($customers)>0)
                            @php $c=1 @endphp
                            @foreach($customers as $customer)
                            <tr>
                                <td>{{$c++}}</td>
                                <td>{{$customer->name ?? ''}}</td>
                                <td>{{$customer->email ?? ''}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">
                                    <a href="" >View All ... </a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>


            </div>







            <div class="row">

                <!-- Top 5 orders -->
                <div class="col-sm-6">
                    <h4>New Orders</h4>
                    <table class="table table-colored">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order ID</th>
                                <th>Customer</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($orders)>0)
                            @php $c=1 @endphp
                            @foreach($orders as $order)
                            <tr>
                                <td>{{$c++}}</td>
                                <td>{{$order->id ?? ''}}</td>
                                <td>{{$order->name ?? ''}}</td>
                                <td>{{$order->total_price ?? ''}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">
                                    <a href="" >View All ... </a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>

                <!-- Top 5 Brands -->
                <div class="col-sm-6">
                    <h4>New Brands</h4>
                    <table class="table table-colored">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Brand</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if(count($brands)>0)
                            @php $c=1 @endphp
                            @foreach($brands as $brand)
                            <tr>
                                <td>{{$c++}}</td>
                                <td>{{$brand->name ?? ''}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <a href="" >View All ... </a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>


            </div>
        </div>







                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

            <style type="text/css">
                .card
            </style>

        </section>
        <!-- /.content -->
        @endsection
